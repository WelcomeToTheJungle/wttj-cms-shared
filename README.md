# WTTJ-CMS components

## Develop

To use this package from local source on other applications:

```
cd dev/wttj-cms-shared
yarn link
```

```
cd dev/wttj-front
yarn link wttj-cms-shared
```

## HOW-TO

- [How to manage translations / locales with crowdin.com?](https://www.notion.so/wttj/Manage-translations-with-crowdin-com-aef149099e6742d9831bd93992a30956)
