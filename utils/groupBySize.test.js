import groupBySize from './groupBySize'

describe('groupBySize', () => {
  test('Group array by a defined number', () => {
    let splittedArray = groupBySize(new Array(3).fill(1), 3)
    expect(splittedArray.length).toBe(1)
    expect(splittedArray[0].length).toBe(3)

    splittedArray = groupBySize(new Array(4).fill(1), 3)
    expect(splittedArray.length).toBe(2)
    expect(splittedArray[0].length).toBe(3)
    expect(splittedArray[1].length).toBe(1)
  })
})
