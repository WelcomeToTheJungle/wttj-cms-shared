export default (array, split) => {
  let splittedArray = []
  if (!array) return splittedArray

  let currentSplit = []
  array.forEach((tag, i) => {
    currentSplit.push(tag)
    if (i !== 0 && (i + 1) % split === 0) {
      splittedArray.push(currentSplit)
      currentSplit = []
    }
  })

  if (currentSplit.length) {
    splittedArray.push(currentSplit)
  }

  return splittedArray
}
