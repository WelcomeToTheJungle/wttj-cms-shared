// from https://github.com/formatjs/formatjs/blob/ae89b596ada7011f84c12069ee219e8a68922527/packages/formatjs/packages/intl-utils/src/diff.ts
// Explanation on why we need it to fork it:
// https://github.com/formatjs/formatjs/blob/ae89b596ada7011f84c12069ee219e8a68922527/packages/formatjs/packages/intl-utils/README.md#caveats

const MS_PER_SECOND = 1e3
const SECS_PER_MIN = 60
const SECS_PER_HOUR = SECS_PER_MIN * 60
const SECS_PER_DAY = SECS_PER_HOUR * 24
const SECS_PER_WEEK = SECS_PER_DAY * 7

export const thresholds = {
  second: 45,
  minute: 45,
  hour: 22,
  day: 29,
}

export const selectDateUnit = (from, to = Date.now()) => {
  const secs = (+from - +to) / MS_PER_SECOND
  if (Math.abs(secs) < thresholds.second) {
    return {
      value: Math.round(secs),
      unit: 'second',
    }
  }

  const mins = secs / SECS_PER_MIN
  if (Math.abs(mins) < thresholds.minute) {
    return {
      value: Math.round(mins),
      unit: 'minute',
    }
  }

  const hours = secs / SECS_PER_HOUR
  if (Math.abs(hours) < thresholds.hour) {
    return {
      value: Math.round(hours),
      unit: 'hour',
    }
  }

  const days = secs / SECS_PER_DAY
  if (Math.abs(days) < thresholds.day) {
    return {
      value: Math.round(days),
      unit: 'day',
    }
  }

  const fromDate = new Date(from)
  const toDate = new Date(to)
  const years = fromDate.getFullYear() - toDate.getFullYear()
  const months = years * 12 + fromDate.getMonth() - toDate.getMonth()
  if (Math.round(Math.abs(months)) > 11) {
    return {
      value: Math.round(years),
      unit: 'year',
    }
  }

  if (Math.round(Math.abs(months)) > 0) {
    return {
      value: Math.round(months),
      unit: 'month',
    }
  }

  const weeks = secs / SECS_PER_WEEK
  return {
    value: Math.round(weeks),
    unit: 'week',
  }
}
