import hexToRGB from './hexToRGB'

test(`hexToRGB() accepts hex color with hash`, () => {
  expect(hexToRGB('#996633')).to.equal('153, 102, 51')
})

test(`hexToRGB() accepts hex color without hash`, () => {
  expect(hexToRGB('996633')).to.equal('153, 102, 51')
})

test(`hexToRGB() accepts shorthand hex color with hash`, () => {
  expect(hexToRGB('#963')).to.equal('153, 102, 51')
})

test(`hexToRGB() accepts shorthand hex color without hash`, () => {
  expect(hexToRGB('963')).to.equal('153, 102, 51')
})

test(`hexToRGB() accepts invalid hex`, () => {
  expect(hexToRGB('#963Z7')).to.equal(undefined)
})
