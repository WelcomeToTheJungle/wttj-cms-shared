import { createTheme } from '@welcome-ui/core'

import {
  bannerHeight,
  searchFormWidth,
  centeredContainerWidth,
  color,
  fontSize,
  gutter,
  imagesHeight,
  rgba,
} from './theme-helpers'

const theme = createTheme({
  color: {
    black: '#000000',
    primary: '#00c29a',
    green: {
      100: '#ebfff6',
      300: '#baf4d9',
    },
  },
  imagesHeight: {
    organizations: {
      show: {
        logo: '12.5rem',
      },
      thumb: {
        cover: '9.4rem',
      },
    },
  },
  searchFormWidth: {
    md: '58rem',
    lg: '78rem',
    test: {
      md: '65rem',
    },
  },
  centeredContainerWidth: {
    md: '56rem',
    lg: '78rem',
    movies: {
      md: '64rem',
    },
  },
  fontSize: {
    sm: '0.8rem',
    md: '1rem',
    lg: '1.2rem',
  },
  gutter: {
    sm: '0.3rem',
    md: '0.5rem',
    lg: '0.8rem',
  },
  bannerHeight: {
    movies_categories: '30rem',
    mobile: {
      home: '38rem',
      job: '28rem',
    },
  },
})

const props = { theme }

test(`color() handles valid params`, () => {
  expect(color('green', '300')(props)).toEqual('#baf4d9')
})

test(`color() handles short params`, () => {
  expect(color('green')(props)).toEqual(theme.color.green)
})

test(`color() handles invalid params`, () => {
  expect(color('invalid')(props)).toEqual(undefined)
})

test(`rgba() handles single color value`, () => {
  expect(rgba('dark', 900, 0.3)(props)).toEqual('rgba(0, 0, 0, 0.3)')
})

test(`rgba() handles longer color value`, () => {
  expect(rgba('primary', '500', 0.3)(props)).toEqual('rgba(59, 53, 220, 0.3)')
})

test(`imagesHeight() handles valid params`, () => {
  expect(imagesHeight('organizations.show.logo')(props)).toEqual('12.5rem')
})

test(`imagesHeight() handles short params`, () => {
  expect(imagesHeight('organizations.show')(props)).toEqual({ logo: '12.5rem' })
})

test(`imagesHeight() handles invalid params`, () => {
  expect(imagesHeight('organizations.invalid')(props)).toEqual(undefined)
})

test(`gutter() handles valid params`, () => {
  expect(gutter('sm')(props)).toEqual('0.3rem')
})

test(`gutter() handles invalid params`, () => {
  expect(gutter('invalid')(props)).toEqual(undefined)
})

test(`bannerHeight() handles valid params`, () => {
  expect(bannerHeight('mobile.home')(props)).toEqual('38rem')
})

test(`bannerHeight() handles short params`, () => {
  expect(bannerHeight('movies_categories')(props)).toEqual('30rem')
})

test(`bannerHeight() handles invalid params`, () => {
  expect(bannerHeight('invalid')(props)).toEqual(undefined)
})

test(`fontSize() handles valid params`, () => {
  expect(fontSize('sm')(props)).toEqual('0.8rem')
})

test(`fontSize() handles invalid params`, () => {
  expect(fontSize('invalid')(props)).toEqual(undefined)
})

// Skip these tests until we can mock `createHelpers`
test(`searchFormWidth() handles empty params`, () => {
  expect(searchFormWidth()(props)).toEqual(undefined)
})

test(`searchFormWidth() handles valid params`, () => {
  expect(searchFormWidth('md')(props)).toEqual('58rem')
})

test(`searchFormWidth() handles valid nested params`, () => {
  expect(searchFormWidth('test', 'md')(props)).toEqual('65rem')
})

test(`searchFormWidth() handles invalid params`, () => {
  expect(searchFormWidth('invalid')(props)).toEqual(undefined)
})

test(`centeredContainerWidth() handles valid params`, () => {
  expect(centeredContainerWidth('md')(props)).toEqual('56rem')
})

test(`centeredContainerWidth() handles valid params`, () => {
  expect(centeredContainerWidth('movies.md')(props)).toEqual('64rem')
})

test(`centeredContainerWidth() handles invalid params`, () => {
  expect(centeredContainerWidth('invalid')(props)).toEqual(undefined)
})
