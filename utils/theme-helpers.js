// Temporary file while we convert all helpers to welcome-ui.get

import { th, css } from '@xstyled/styled-components'
import hexToRGB from './hexToRGB'

export const avatarSize = (...path) => th(`avatarSize.${path.join('.')}`)
export const bannerHeight = (...path) => th(`bannerHeight.${path.join('.')}`)
export const borderWidth = (...path) => th(`borderWidth.${path.join('.')}`)
export const boxShadow = (...path) => th(`boxShadow.${path.join('.')}`)
export const breakpoint = (...path) => th(`breakpoint.${path.join('.')}`)
export const centeredContainerWidth = (...path) => th(`centeredContainerWidth.${path.join('.')}`)
export const checkboxSize = (...path) => th(`checkboxSize.${path.join('.')}`)
export const color = (...path) => th(`color.${path.join('.')}`)
export const coverHeight = (...path) => th(`coverHeight.${path.join('.')}`)
export const coverSize = (...path) => th(`coverSize.${path.join('.')}`)
export const fontFamily = (...path) => th(`fontFamily.${path.join('.')}`)
export const fontSize = (...path) => th(`fontSize.${path.join('.')}`)
export const fontSizeEm = (...path) => th(`fontSizeEm.${path.join('.')}`)
export const fontWeight = (...path) => th(`fontWeight.${path.join('.')}`)
export const gutter = (...path) => th(`gutter.${path.join('.')}`)
export const imagesHeight = (...path) => th(`imagesHeight.${path.join('.')}`)
export const letterSpacing = (...path) => th(`letterSpacing.${path.join('.')}`)
export const modalSize = (...path) => th(`modalSize.${path.join('.')}`)
export const padding = (...path) => th(`space.${path.join('.')}`)
export const radius = (...path) => th(`radius.${path.join('.')}`)
export const ratio = (...path) => th(`ratio.${path.join('.')}`)
export const rgba = (...args) => {
  const opacity = args.pop()
  const path = args
  return props => {
    const color = th(`colors.${path.join('.')}`)(props) || th(`color.${path.join('.')}`)(props)
    return `rgba(${hexToRGB(color)}, ${opacity})`
  }
}
export const roundedButtonSize = (...path) => th(`roundedButtonSize.${path.join('.')}`)
export const searchFormWidth = (...path) => th(`searchFormWidth.${path.join('.')}`)
export const shareButtonSize = (...path) => th(`shareButtonSize.${path.join('.')}`)
export const transition = (...path) => th(`transition.${path.join('.')}`)

// Constants
export const buttonIconWidth = () => th('buttonIconWidth')
export const headerUpperNavHeight = () => th('headerUpperNavHeight')
export const headerLanguageAlertHeight = () => th('headerLanguageAlertHeight')
export const headerLogoOffset = () => th('headerLogoOffset')
export const headerLanguageAlertMobileHeight = () => th('headerLanguageAlertMobileHeight')
export const searchFormHeight = () => th('searchFormHeight')
export const pageNavHeight = () => th('pageNavHeight')
export const metasValueMaxWidth = () => th('metasValueMaxWidth')

export const textStyles = (...path) => ({ theme }) => {
  if (!theme.text) return
  const { size, weight, transform, spacing } = theme.text[path]
  return css`
    font-size: ${size ? theme.fontSize[size] : 'inherit'};
    font-weight: ${weight ? theme.fontWeight[weight] : 'inherit'};
    text-transform: ${transform ? transform : null};
    letter-spacing: ${spacing ? theme.letterSpacing[spacing] : null};
  `
}
