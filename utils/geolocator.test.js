import './geolocator.test.fixtures'

import { formatAddressData, autocomplete, geocode, reverseGeocode } from './geolocator'

const matchLevels = ['city', 'state', 'country']

test(`autocomplete() returns suggestions for valid query`, async () => {
  const [result] = await autocomplete({
    query: 'Le Havre',
    language: 'fr',
    matchLevels: ['city', 'district', 'state', 'country'],
  })

  expect(result).include({
    label: '<em>Le Havre</em>, Seine-Maritime, Normandie, France',
    countryCode: 'FR',
  })
})

test(`autocomplete() returns no city suggestion if cities are filtered`, async () => {
  const results = await autocomplete({
    query: 'Barcelona',
    language: 'en',
    matchLevels: ['district', 'state', 'country'],
  })

  expect(results).not.include({
    value: 'Barcelona, France',
  })
  expect(results).not.include({
    value: 'Barcelona, France',
    matchLevel: 'city',
  })
})

test(`geocode() with locationId returns valid geocoding payload in English`, async () => {
  const locationId = 'NT_1patnZW6CoWLlTNalafh9D'
  const { lat, lng, ...rest } = await geocode({
    locationId,
  })

  expect(lat).toBeTruthy()
  expect(lng).toBeTruthy()
  expect(rest).include({
    locationId,
    state: 'Normandy',
    district: 'Seine-Maritime',
    country: 'France',
    countryCode: 'FR',
  })
})

test(`reverseGeocode() with position returns valid geocoding payload`, async () => {
  const position = '49.493360,0.108080'
  const [lat, lng] = position.split(',')
  const { value, street, ...rest } = await reverseGeocode({
    position,
    language: 'en',
  })

  expect(street).toMatch(/place de l'hôtel de ville/i)
  expect(rest).include({
    lat,
    lng,
    postalCode: '76600',
    city: 'Le Havre',
    state: 'Normandy',
    district: 'Seine-Maritime',
    country: 'France',
    countryCode: 'FR',
  })
})

test(`formatAddressData() returns formatted address for house-number-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      houseNumber: 16,
      street: 'Rue du Mail',
      city: 'Paris',
      district: 'Paris',
      state: 'Ile-de-France',
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('Paris, Ile-de-France, France')
})

test(`formatAddressData() returns formatted address for house-number-precise locations in CS`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      houseNumber: '2256/16',
      street: 'Brněnská',
      city: 'Jitrnice',
      country: 'Czech Republic',
      countryCode: 'CZ',
    },
  })
  expect(result).toEqual('Jitrnice, Czech Republic')
})

test(`formatAddressData() returns formatted address for street-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      street: 'Rue du Mail',
      city: 'Paris',
      district: 'Paris',
      state: 'Ile-de-France',
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('Paris, Ile-de-France, France')
})

test(`formatAddressData() returns formatted address for city-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      city: 'Paris',
      district: 'Paris',
      state: 'Ile-de-France',
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('Paris, Ile-de-France, France')
})

test(`formatAddressData() returns formatted address for district-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      district: 'Agglomeration de Paris',
      state: 'Ile-de-France',
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('Ile-de-France, France')
})

test(`formatAddressData() returns formatted address for state-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      state: 'Ile-de-France',
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('Ile-de-France, France')
})

test(`formatAddressData() returns formatted address for country-precise locations`, () => {
  const result = formatAddressData({
    matchLevels,
    address: {
      country: 'France',
      countryCode: 'FR',
    },
  })
  expect(result).toEqual('France')
})
