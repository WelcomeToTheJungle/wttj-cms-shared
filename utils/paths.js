import { safeGetPath } from './i18n'

export const INDICES = {
  articles: 'wttj',
  tech: 'behind_the_code',
  students: 'students',
  recruiters: 'recruiters',
}

const getCollectionSlugs = article => {
  // Get the first collection & its category related on the article
  const firstCollection = article.getIn(['cms_collections', 0])
  if (firstCollection) {
    return {
      collectionSlug: firstCollection.get('slug'),
      collectionCategorySlug: firstCollection.getIn(['cms_collection_category', 'slug']),
    }
  }

  return {}
}

export const getArticlePath = ({
  article,
  collectionSlug: providedCollectionSlug,
  collectionCategorySlug: providedCollectionCategorySlug,
  lang,
  getPath,
}) => {
  // Default url
  let path = article.get('path')
  if (path) {
    return safeGetPath(getPath, path, lang)
  }

  path = `/articles/${article.get('slug')}`
  const { collectionCategorySlug, collectionSlug } = getCollectionSlugs(article)

  // If collection|category slugs provided use those
  if (providedCollectionCategorySlug && providedCollectionSlug) {
    path = `/collections/${providedCollectionCategorySlug}/${providedCollectionSlug}${path}`
    // Otherwise take from the article
  } else if (collectionCategorySlug && collectionSlug) {
    path = `/collections/${collectionCategorySlug}/${collectionSlug}${path}`
  }

  // return something nice
  return safeGetPath(getPath, path, lang)
}
