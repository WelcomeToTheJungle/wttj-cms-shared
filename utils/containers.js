export const getBlocks = (blocks, container) => {
  const containerBlocks = blocks || container.get('blocks')
  return containerBlocks.sortBy(block => block.get('position'))
}

export const getBlocksFromLayout = (container, providedBlocks) => {
  const blocks = providedBlocks || container.get('blocks')
  const layout = container.get('layout')

  // Sort blocks according to layout
  if (layout && !layout.isEmpty()) {
    return layout
      .filter(item => item)
      .map(item => blocks.find(block => String(block.get('id')) === item.get('id')))
  }

  // Or position (if layout doesn't exist yet)
  return blocks.sortBy(block => block.get('position'))
}

export const getNewBlocks = (oldBlocks, blocks) =>
  blocks.filter(block => {
    return oldBlocks.indexOf(block) === -1
  })

export const getContainersForColumn = (section, column, providedContainers) => {
  const containers = providedContainers || section.get('containers')
  const layout = section.getIn(['layout_info', 'containers'])

  if (!layout) {
    return []
  }

  // Get layout from the section
  // Filter to get containers in the given column
  // Get the _actual_ containers from the section
  // Make sure there are no undefined
  // And finally, sort them
  return layout
    .filter(item => item.get('column') === column.get('reference'))
    .map(item =>
      containers.find(container => container.get('layout_reference') === item.get('reference'))
    )
    .filter(container => container)
    .sortBy(item => item.get('position'))
}
