export const safeGetPath = (getPath, path, lang) => (getPath ? getPath(path, lang) : path)
