// from https://github.com/formatjs/formatjs/blob/ae89b596ada7011f84c12069ee219e8a68922527/packages/formatjs/packages/intl-utils/tests/diff.ts

import { selectDateUnit } from './selectDateUnit'

const SEC = 1e3
const MIN = SEC * 60
const HOUR = MIN * 60
const DAY = HOUR * 24

const past = (v, date = Date.now()) => +date - v
const future = (v, date = Date.now()) => +date + v

describe('selectDateUnit', () => {
  it('should work for sec', () => {
    expect(selectDateUnit(past(44 * SEC))).toEqual({
      value: -44,
      unit: 'second',
    })
    expect(selectDateUnit(future(44 * SEC))).toEqual({
      value: 44,
      unit: 'second',
    })
  })

  it('should work for min', () => {
    expect(selectDateUnit(past(45 * SEC))).toEqual({
      value: -1,
      unit: 'minute',
    })
    expect(selectDateUnit(future(45 * SEC))).toEqual({
      value: 1,
      unit: 'minute',
    })
    expect(selectDateUnit(past(44 * MIN))).toEqual({
      value: -44,
      unit: 'minute',
    })
    expect(selectDateUnit(future(44 * MIN))).toEqual({
      value: 44,
      unit: 'minute',
    })
  })

  it('should work for hour', () => {
    expect(selectDateUnit(past(45 * MIN))).toEqual({
      value: -1,
      unit: 'hour',
    })
    expect(selectDateUnit(future(45 * MIN))).toEqual({
      value: 1,
      unit: 'hour',
    })
    expect(selectDateUnit(past(21 * HOUR))).toEqual({
      value: -21,
      unit: 'hour',
    })
    expect(selectDateUnit(future(21 * HOUR))).toEqual({
      value: 21,
      unit: 'hour',
    })
  })

  it('should work for day', () => {
    expect(selectDateUnit(new Date(2019, 1, 5), new Date(2019, 1, 6))).toEqual({
      value: -1,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 6), new Date(2019, 1, 5))).toEqual({
      value: 1,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 5), new Date(2019, 1, 9))).toEqual({
      value: -4,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 9), new Date(2019, 1, 5))).toEqual({
      value: 4,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 12), new Date(2019, 1, 1))).toEqual({
      value: 11,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 12), new Date(2019, 1, 1))).toEqual({
      value: 11,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 5), new Date(2019, 1, 10))).toEqual({
      value: -5,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 10), new Date(2019, 1, 5))).toEqual({
      value: 5,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 5), new Date(2019, 1, 26))).toEqual({
      value: -21,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 1, 10), new Date(2019, 2, 10))).toEqual({
      value: -28,
      unit: 'day',
    })
    expect(selectDateUnit(new Date(2019, 2, 10), new Date(2019, 1, 10))).toEqual({
      value: 28,
      unit: 'day',
    })
  })
  expect(selectDateUnit(new Date(2020, 12, 31), new Date(2021, 1, 10))).toEqual({
    value: -10,
    unit: 'day',
  })

  it('should work for week', () => {
    expect(selectDateUnit(new Date(2019, 0, 1), new Date(2019, 0, 30))).toEqual({
      value: -4,
      unit: 'week',
    })
  })

  it('should work for month', () => {
    expect(selectDateUnit(new Date(2019, 0, 10), new Date(2019, 2, 27))).toEqual({
      value: -2,
      unit: 'month',
    })
    expect(selectDateUnit(new Date(2019, 2, 27), new Date(2019, 0, 5))).toEqual({
      value: 2,
      unit: 'month',
    })
    expect(selectDateUnit(new Date(2020, 11, 31), new Date(2021, 1, 10))).toEqual({
      value: -2,
      unit: 'month',
    })
  })

  it('should work for year', () => {
    const date = new Date(2020, 12, 31)
    expect(selectDateUnit(past(366 * DAY, date), date)).toEqual({
      value: -1,
      unit: 'year',
    })
    expect(selectDateUnit(future(366 * DAY, date), date)).toEqual({
      value: 1,
      unit: 'year',
    })
    expect(selectDateUnit(past(1000 * DAY, date), date)).toEqual({
      value: -3,
      unit: 'year',
    })
    expect(selectDateUnit(future(1000 * DAY, date), date)).toEqual({
      value: 2,
      unit: 'year',
    })
  })
})
