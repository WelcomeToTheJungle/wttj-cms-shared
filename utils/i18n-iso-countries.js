import { registerLocale } from 'i18n-iso-countries/index'

import { AVAILABLE_LOCALES } from '../constants/locales'

AVAILABLE_LOCALES.forEach(locale => {
  registerLocale(require(`i18n-iso-countries/langs/${locale}.json`))
})

export * from 'i18n-iso-countries'
