import nock from 'nock'
import config from 'configuration'

const {
  here: { apiKey },
} = config

nock('https://autocomplete.geocoder.ls.hereapi.com:443', { encodedQueryParams: true })
  .persist()
  .get('/6.2/suggest.json')
  .query({
    apiKey,
    maxresults: '10',
    language: 'fr',
    query: 'Le%20Havre',
    beginHighlight: '%3Cem%3E',
    endHighlight: '%3C%2Fem%3E',
  })
  .reply(
    200,
    [
      '1f8b0800000000000000d596e16eda3010c75f25cae7a4030a14aa6a1204580b21ed4820a3d3345d88092ec1a68e0345551f60cfb5179b9396d66c4957902a6d1f7d395ffef7e37f39eed5280e0214714c49a49e7ebd5743f050a89eaa1d06648234e5a45a2d1434e50c2d3e9a4839871543671fc441d5442a09620890c89e32719ed09870b631a89f843a83469243279014bff045c872be1797c0c9b55b35a86b868e05214c67f5964804df6728121aeeb7759e4588a711079e14b5285b00f131dabe2ec9b2112648ef03c31c2fd227388d67695e52512a7c9298f6a63e68ea02f86466a255da797afb417b4171059b486f42a429956ae94453da98f833ba42e4102c96d9cac05299dfc6377ec9f106c7505985e67051fc5269e661d90a92c034197840b862a32547224dd4865086f42cfa858f1cf271242e4db808bb14910885ba4529f3d3573084f85b782678fec0f9743d1fa8623befc2d4ea7bdd1bdd9fa3f3a3abfa5267b1158ce78dff91a920f416ac3f7f08e1913e245890bda06b107c4ba572e1f1a0184261065dc5e67f053cb4b366f9ea7c098d5bfbfc12b5efc221400d353831f2004bea24c68932196997ce484425a0cfd27f074a5fe7f8d8d62eca04c621fe74dfc59f7e79509c7ba877e1de86019db678335887b9f8fe6d7fba7bfbd380104f292358ec987aa9523bd6141b88d28509269c66f9d42407fad4635ddf2c143f6f1cbbeba1b1312a937e9cbb73b27dfa2257063cc02bc422ec4b4b476ae2757fa6edeca04c31ec0dd2426b654cd95c538ae552b29e9af1740a6126c2163b10617fe973e450bae1d376df1d9f90cd6614e57e4bb3116e85ee38946189dd93f2d7b9a53dec704bfb3e64ae47ef32d7edd14d4d1f9acef5ca5cd7dbeb9e57ed8ef55cbbfddb733d7a0b560308f862d95c122efe8309e399254b4c33d70de0331121283ad08c46c3cafa709aab8a63596b77bd700dc35d1cd52fe737b9667c5427e17d922903b53004c0409a6259fc2e5213e6684919dfd7a502cadeb3dd133f7f3c998bb55d2e540a654116dd6112709a655765e01f38ddd06b5c8e3ff1e2f8a8d6742f8eb8d78947853da77b2b55c6da810de25c1af067f9afc34b1bd98197b69f8befdbc32fd68c49f2c80c0000',
    ],
    [
      'Access-Control-Allow-Origin',
      '*',
      'Content-Encoding',
      'gzip',
      'Content-Type',
      'application/json;charset=utf-8',
      'Date',
      'Wed, 08 Jan 2020 10:57:08 GMT',
      'Server',
      'nginx-clojure',
      'Vary',
      'Accept-Encoding',
      'X-NLP-IRT',
      '23838',
      'X-NLP-TID',
      'a3c79e70-1364-49b7-bbb7-c42d63125e31',
      'X-Served-By',
      'i-01577b0a2912cd8cb.eu-west-1b',
      'Content-Length',
      '819',
      'Connection',
      'Close',
    ]
  )

nock('https://autocomplete.geocoder.ls.hereapi.com:443', { encodedQueryParams: true })
  .persist()
  .get('/6.2/suggest.json')
  .query({
    apiKey,
    maxresults: '10',
    language: 'en',
    query: 'Barcelona',
    beginHighlight: '%3Cem%3E',
    endHighlight: '%3C%2Fem%3E',
  })
  .reply(
    200,
    [
      '1f8b0800000000000000ad96dd729a4014c75f85f15a2d908892c9744610354d2428f9329d4e6785856cb3eee22e683493a7e975affa0879b1ae8946aa3845eb1572f670f67f7ee7ec599f0b3c0943c86344092f9c7c7d2e603080b8705270238048513a85c3cf06601ec49480d34fe2ad50143e244c4008851b24e2dda30989d9d4a4fedc64b9cedc877a601ef5cc1726fbea3b88aedb17b65f8381adc22638ab7af5bbbe291c81ef33c8c5e6cfcb38cbddc5228f413c8f6982180805082c779b3b65687b29168620f61e2ee0f82d8b85ef4b314762074c9659431af414b5a39f83a975ebca77e347ede0c98a55b47d2da2221e5ec8946bb25cdda483d6d8dc40026709c4a028b968f8fa9b4806c5afbfc680ed05e7c6b233e0d8253c01fac4e8dc995fbaa05d2111af3d6d85f3212905a84e6654fc0a13b462b0a6572cf888c70c7971be4ef9f04ef3301898212c60bcfea49209600c08957c2ab909de0b88d1ab670031aff8a372f964f651b7521b1ba3809e5b682b90774d291a2ea09203124c533036f5fe13c87ac7e8958a5c3a92e57ca4529d73b04609fafd90854924579dfaccf55455d3f452fd7f1b25cfd4583f171f7d0019db2fc3ecca77bb840744a9b36af95a390ffab5a9536e6dcd70a3f2168f1043b1a82f2071bafc7399bb165cd5154d2ba9790bbe4472035888c8c321a93c1ae1178be96dcff4e571d44141252c8d1ab9a97410015c6a4106105f3159eadc15cb515556d592729c138b49311d0e90c061d090c6e2d9289be5bde09897171970ce0613cdbb0f06b8f7236ac8a3c164aa3a96b10dce524efa26490b5bf159b3eec44851144d51f201721e1046518408e445a99bc01925922924ec85c869672122532de4c13d3c6e391daa372715ce8de6d6799a12946a22fb2d22c0628a4648642af560280c698e2ef428f1a5c6224bc9367b2b9ca9ccfea66988cb9d0322b511c6ef1b3208f36256b50dc88befb38ee6c10ea47d3b2a3783c9eda4dcee9ed7ae6a86ea4783ad3db771207b884a2d06880fe777914d99b0eef0dfa5a21f2b7249ce184cef73fadbcb1f1acc8090c60a0000',
    ],
    [
      'Access-Control-Allow-Origin',
      '*',
      'Content-Encoding',
      'gzip',
      'Content-Type',
      'application/json;charset=utf-8',
      'Date',
      'Wed, 08 Jan 2020 13:32:14 GMT',
      'Server',
      'nginx-clojure',
      'Vary',
      'Accept-Encoding',
      'X-NLP-IRT',
      '15045',
      'X-NLP-TID',
      'be5eaaed-760b-45ec-928f-08a8aba1e555',
      'X-Served-By',
      'i-0b5cb8917fe938923.eu-west-1b',
      'Content-Length',
      '774',
      'Connection',
      'Close',
    ]
  )

nock('https://geocoder.ls.hereapi.com:443', { encodedQueryParams: true })
  .persist()
  .get('/6.2/geocode.json')
  .query({
    apiKey,
    jsonattributes: '1',
    language: 'en',
    locationid: 'NT_1patnZW6CoWLlTNalafh9D',
  })
  .reply(
    200,
    [
      '1f8b08000000000000006552616bc23010fd2b92afab92685bd77e1b8a4c703254266cc838dba8616912dad851a4ff7d97569d683f5d5fde7bf7729713c97961b42a38894f24e316a66aa75d6d45c60b0b992131e9d33eed52d6a5cf2bc6e2fe30f6fd5e10844f143f527ba414fc97c45f27677694f65c4a5e824ad098f6a84732b0c961c64b2ed13011b6221e913a012bb472fd2ef534c5f3f9ea9b19b0ea731d8ef47a26577390b03b44e31bd1aa32e84d8c16ca229c8ac248a8de7521ae96c8b3c714597ed4f3a3411039b5da9f41da639446c3da65331fcd15f0dadaccf8ce3ec8039fb2e73b390dc388a17cabadd5d942ec0f8f3a3f6083fbae51306435ea204d716045abd9368399f1ce2b9439f73a739d67a0d2caeb4cf2668c1e49f451d9bc42da64f182ffb81eeb4670a15e288eb1e442f1ee1be4c22dd29d8806bf3440c468349023ed52916118e22e9b4ccdfc408ec142b3c912e4d151ae397eb8731ab569e680f6b5f74fbb49d312972ee603ed21e08debd97453d7f5a67d5dee555007fc01c99077b5b1020000',
    ],
    [
      'Access-Control-Allow-Origin',
      '*',
      'Cache-Control',
      'public, max-age=86400',
      'Content-Encoding',
      'gzip',
      'Content-Type',
      'application/json;charset=utf-8',
      'Date',
      'Wed, 08 Jan 2020 11:27:44 GMT',
      'Server',
      'nginx-clojure',
      'X-NLP-IRT',
      '3094',
      'X-NLP-LOG',
      'Z2NfYz1GUkEgZ2NfbD1jaXR5IGdjX209YWQ=',
      'X-NLP-TID',
      '4c1af2e3-55e3-4d43-b44f-0a245b4fafd1',
      'X-Served-By',
      'i-0d5b4946ba48325d6.eu-west-1b',
      'Content-Length',
      '397',
      'Connection',
      'Close',
    ]
  )

nock('https://reverse.geocoder.ls.hereapi.com:443', { encodedQueryParams: true })
  .persist()
  .get('/6.2/reversegeocode.json')
  .query({
    apiKey,
    language: 'en',
    mode: 'trackPosition',
    pos: '49.493360%2C0.108080',
    jsonattributes: '1',
    maxresults: '1',
    minresults: '1',
  })
  .reply(
    200,
    {
      response: {
        metaInfo: { timestamp: '2020-01-08T11:40:15.312+0000' },
        view: [
          {
            result: [
              {
                relevance: 0.92,
                distance: 8.4,
                matchLevel: 'street',
                matchQuality: {
                  country: 1,
                  state: 1,
                  county: 1,
                  city: 1,
                  street: [1],
                  postalCode: 1,
                },
                location: {
                  locationId: 'NT_xRaG8GTPlnDWMpv54OnxBD_l_1052220169_L',
                  locationType: 'point',
                  displayPosition: { latitude: 49.4934355, longitude: 0.1080823 },
                  navigationPosition: [{ latitude: 49.4934355, longitude: 0.1080823 }],
                  mapView: {
                    topLeft: { latitude: 49.49347, longitude: 0.10693 },
                    bottomRight: { latitude: 49.4934, longitude: 0.10927 },
                  },
                  address: {
                    label: "Place de l'Hôtel de Ville, 76600 Le Havre, France",
                    country: 'FRA',
                    state: 'Normandy',
                    county: 'Seine-Maritime',
                    city: 'Le Havre',
                    street: "Place de l'Hôtel de Ville",
                    postalCode: '76600',
                    additionalData: [
                      { value: 'France', key: 'CountryName' },
                      { value: 'Normandy', key: 'StateName' },
                      { value: 'Seine-Maritime', key: 'CountyName' },
                    ],
                  },
                  mapReference: {
                    referenceId: '1052220169',
                    spot: 0.51,
                    sideOfStreet: 'left',
                    countryId: '20000001',
                    stateId: '20057204',
                    countyId: '20038936',
                    cityId: '20078836',
                  },
                },
              },
            ],
            viewId: 0,
          },
        ],
      },
    },
    [
      'Access-Control-Allow-Origin',
      '*',
      'Cache-Control',
      'public, max-age=86400',
      'Content-Type',
      'application/json;charset=utf-8',
      'Date',
      'Wed, 08 Jan 2020 11:40:15 GMT',
      'Server',
      'nginx-clojure',
      'X-NLP-IRT',
      '3310',
      'X-NLP-TID',
      '2ba342d0-a241-4441-b8bb-776201212a9c',
      'X-Served-By',
      'i-0013b87cab2398e5a.eu-west-1a',
      'Content-Length',
      '1120',
      'Connection',
      'Close',
    ]
  )
