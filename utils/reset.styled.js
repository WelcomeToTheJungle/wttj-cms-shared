import { css, keyframes } from '@xstyled/styled-components'
import { fontSize } from './theme-helpers'

const resetAutofill = keyframes`
  0% {
    background: inherit;
  }
  100% {
    background: rgba(0, 194, 154, 0.07);
  }
`

const resetAutofillRule = css`
  ${resetAutofill} 0.2s linear both;
`

export const reset = css`
  html,
  body,
  div,
  span,
  applet,
  object,
  iframe,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  a,
  abbr,
  acronym,
  address,
  big,
  cite,
  code,
  del,
  dfn,
  em,
  img,
  ins,
  kbd,
  q,
  s,
  samp,
  small,
  strike,
  strong,
  sub,
  sup,
  tt,
  var,
  b,
  u,
  i,
  center,
  dl,
  dt,
  dd,
  ol,
  ul,
  li,
  fieldset,
  form,
  label,
  legend,
  table,
  caption,
  tbody,
  tfoot,
  thead,
  tr,
  th,
  td,
  article,
  aside,
  canvas,
  details,
  embed,
  figure,
  figcaption,
  footer,
  header,
  hgroup,
  menu,
  nav,
  output,
  ruby,
  section,
  summary,
  time,
  mark,
  audio,
  video {
    min-width: 0;
    min-height: 0;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }
  article,
  aside,
  details,
  figcaption,
  figure,
  footer,
  header,
  hgroup,
  menu,
  nav,
  section {
    display: block;
  }
  body {
    line-height: 1;
  }
  ol,
  ul {
    list-style: none;
  }
  blockquote,
  q {
    quotes: none;
  }
  blockquote:before,
  blockquote:after,
  q:before,
  q:after {
    content: '';
    content: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  }
  a {
    text-decoration: none;
  }
  img {
    overflow: hidden;
  }
  input {
    appearance: none;
    &::-webkit-search-cancel-button {
      display: none;
    }
  }
  input,
  textarea,
  select {
    &:-webkit-autofill {
      animation: ${resetAutofillRule};
    }
  }
  :focus {
    outline: none;
  }
  *,
  *::after,
  *::before {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  html {
    height: 100%;
    font-size: ${fontSize('html')};
  }
  body {
    min-height: 100%;
    padding-top: 1px;
    margin-top: -1px;
  }
`

export default reset
