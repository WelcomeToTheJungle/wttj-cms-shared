export const modulo = (value, len) => {
  const m = ((value % len) + len) % len
  return m < 0 ? m + Math.abs(len) : m
}
