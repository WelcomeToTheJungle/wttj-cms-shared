import fetch from 'isomorphic-fetch'
import { getName as getCountryName, alpha3ToAlpha2 } from './i18n-iso-countries'

import config from 'configuration'

const {
  here: { apiKey },
} = config

export const formatShortAddressData = ({ address, matchLevels }) => {
  // Get first of matchLevels (e.g. city, district, state) and add country
  // Being careful we don't end up with country duplicated
  const subject = matchLevels.map(matchLevel => address[matchLevel]).find(Boolean)
  return [subject, address.country].filter(Boolean).join(', ')
}

export const formatAddressData = ({ address, matchLevels }) => {
  // Join all (non-empty) matchLevels with comma
  return matchLevels
    .map(matchLevel => address[matchLevel])
    .filter(Boolean)
    .join(', ')
}

const sanitize = value => (value ? value.replace(/<\/?em>/g, '') : null)

const normalize = ({ address, matchLevel, matchLevels, short = false, ...rest }) => {
  const countryCode = alpha3ToAlpha2(address.countryCode)
  const country = address.country || getCountryName(countryCode, 'en')
  const formattableAddress = {
    houseNumber: address.houseNumber,
    street: address.street,
    city: address.city,
    postalCode: address.postalCode,
    district: address.county,
    state: address.state,
    country,
  }
  const formattedAddressData = formatAddressData({ address: formattableAddress, matchLevels })
  const value = short
    ? formatShortAddressData({ address: formattableAddress, matchLevels })
    : formattedAddressData

  return {
    ...rest,
    houseNumber: sanitize(address.houseNumber),
    street: sanitize(address.street),
    city: sanitize(address.city),
    postalCode: sanitize(address.postalCode),
    district: sanitize(address.county),
    state: sanitize(address.state),
    country: sanitize(country),
    countryCode: countryCode,
    isCity: matchLevel === 'city',
    isDistrict: matchLevel === 'county',
    isState: matchLevel === 'state',
    isCountry: matchLevel === 'country',
    matchLevel,
    name: formattedAddressData,
    address: sanitize(formattedAddressData),
    label: formattedAddressData,
    value: sanitize(value),
  }
}

const compact = obj =>
  Object.entries(obj).reduce((prev, [key, value]) => (value ? { ...prev, [key]: value } : prev), {})

const getQueryString = params =>
  Object.entries(params)
    .map(([key, value]) => `${key}=${value}`)
    .join('&')

const fetchJson = async ({ url: _url, params: _params }) => {
  const params = compact(_params)
  const queryString = getQueryString(params)
  const url = `${_url}?${queryString}`
  const response = await fetch(url, { cache: 'force-cache' })
  return await response.json()
}

const HERE_TYPE_MAPPING = {
  houseNumber: 'houseNumber',
  street: 'street',
  city: 'city',
  county: 'district',
  state: 'state',
  country: 'country',
}

const isDesiredLevel = (matchLevels, matchLevel) =>
  matchLevels.includes(HERE_TYPE_MAPPING[matchLevel])

export const autocomplete = async ({
  query,
  language,
  matchLevels,
  maxresults = 10,
  short = false,
}) => {
  const { suggestions = [] } = await fetchJson({
    url: 'https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json',
    params: {
      apiKey,
      maxresults,
      language,
      query,
      beginHighlight: '<em>',
      endHighlight: '</em>',
    },
  })

  return suggestions
    .filter(item => isDesiredLevel(matchLevels, item?.matchLevel))
    .map(result => {
      result.address.countryCode = result.countryCode
      return normalize({ ...result, matchLevels, short })
    })
}

const getResult = json => json?.response?.view?.[0]?.result?.[0]

const getAddressFromLocation = (location = {}) => {
  let { country, additionalData, ...address } = location.address

  address = {
    ...address,
    district: additionalData.find(i => i['key'] === 'CountyName')?.value,
    state: additionalData.find(i => i['key'] === 'StateName')?.value,
    countryCode: country,
  }

  return address
}

export const geocode = async ({ locationId, query, language = 'en' }) => {
  const json = await fetchJson({
    url: 'https://geocoder.ls.hereapi.com/6.2/geocode.json',
    params: {
      apiKey,
      jsonattributes: 1,
      language,
      locationid: locationId,
      searchtext: query,
    },
  })

  const { matchLevel, location } = getResult(json)
  const address = getAddressFromLocation(location)
  const { latitude: lat, longitude: lng } = location?.displayPosition
  const matchLevels = ['city', 'district', 'state', 'country']

  return normalize({ locationId, matchLevel, address, lat, lng, matchLevels })
}

export const reverseGeocode = async ({ position, language, short = false }) => {
  const json = await fetchJson({
    url: 'https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json',
    params: {
      apiKey,
      language,
      mode: 'trackPosition',
      pos: position,
      jsonattributes: 1,
      maxresults: 1,
      minresults: 1,
    },
  })

  const { matchLevel, location } = getResult(json)
  const address = getAddressFromLocation(location)
  const [lat, lng] = position.split(',')
  const matchLevels = ['city', 'district', 'state', 'country']

  return normalize({
    locationId: location?.locationId,
    matchLevel,
    address,
    lat,
    lng,
    matchLevels,
    short,
  })
}
