import React, { createContext, useContext, useEffect, useState } from 'react'

export const CookiesContext = createContext({
  cookies: {},
  mockCookies: false,
  isReady: false,
})

const filterConfigKeys = key => !/^\$\$/.test(key)

export const CookiesProvider = ({ children, locale, mockCookies = false }) => {
  const [cookies, setCookies] = useState({})
  const [isReady, setIsReady] = useState(false)

  useEffect(() => {
    window._axcb = window._axcb || []
    window._axcb.push(axeptio => {
      const configVendors = axeptio.config.cookies
        .find(cookie => cookie.language.toLowerCase() === locale)
        .steps.flatMap(step => step.vendors)
        .map(vendor => vendor.name)
        .sort()

      const currentVendors = Object.keys(axeptio.userPreferencesManager.choices)
        .filter(filterConfigKeys)
        .sort()

      const hasVendorsChanged = configVendors.some((vendor, i) => vendor !== currentVendors[i])

      if (hasVendorsChanged) {
        showCookies()
      }

      axeptio.on('cookies:complete', choices => {
        // We set the keys of the cookies to lowercase to prevent edge-cases (e.g: Youtube instead of youtube)
        const cookies = Object.entries(choices)
          .filter(([key]) => filterConfigKeys(key))
          .reduce((acc, [key, isAccepted]) => {
            const cookieName = key.toLowerCase()
            return {
              ...acc,
              [cookieName]: isAccepted,
            }
          }, {})
        setCookies(cookies)
      })

      setIsReady(true)
    })
  }, [])

  return (
    <CookiesContext.Provider
      value={{
        cookies,
        isReady,
        mockCookies,
      }}
    >
      {children}
    </CookiesContext.Provider>
  )
}

export const useHasCookie = cookie => {
  const { cookies, mockCookies } = useContext(CookiesContext)

  // We're mocking the cookies in wk-react-dashboard
  if (mockCookies) return true

  return cookies[cookie]
}

export const useIsCookieReady = cookie => {
  const { isReady } = useContext(CookiesContext)

  return isReady
}

export const withCookies = WrappedComponent => props => (
  <CookiesContext.Consumer>
    {({ cookies }) => <WrappedComponent cookies={cookies} {...props} />}
  </CookiesContext.Consumer>
)

export const showCookies = () => window.openAxeptioCookies?.()

export const showVideoCookies = () => window.openAxeptioCookies?.({ currentStepIndex: 3 })
