import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

// WTTJ
import { SearchHighlight } from '../search/search.styled'

import CustomFormattedDate from '../dates/formatted_date'
import {
  Thumb,
  CoverWrapper,
  ThumbCover,
  Link,
  Header,
  Title,
  Description,
  Footer,
  Inner,
  Actions,
  Meta,
} from './thumb.styled'
import { Metas } from '../metas/metas.styled'
import WithLocalePropTypes from '../../proptypes/WithLocale'
import LazyContent from '../lazy_content'
import { getArticlePath } from '../../utils/paths'
import { getVisitedResource } from '../../utils/visited-resources'
import ArticlesIcon from './icon'

const ArticlesThumb = props => {
  const {
    article,
    isFullCover,
    orientation,
    mode,
    actions,
    hoverSize,
    onClick,
    showHighlights,
    universes = [],
    rel,
  } = props

  const name =
    (showHighlights && <SearchHighlight attribute="name" hit={article.toJS()} />) ||
    article.get('name') ||
    article.get('title')
  const path = getArticlePath(props)
  const summary = article.get('summary')
  const publishedAt = article.get('published_at')
  const coverUrl = article.getIn(['image', 'thumb', 'url'])
  const handleClick = e => onClick?.(e, path)

  const isVisited = !isFullCover && getVisitedResource('articles', article)

  return (
    <Thumb
      $isVisited={isVisited}
      data-role="articles:thumb"
      isFullCover={isFullCover}
      orientation={orientation}
      mode={mode}
      hoverSize={hoverSize}
      onClick={handleClick}
    >
      <CoverWrapper>
        <LazyContent placeHolder={ThumbCover}>
          {coverUrl && (
            <Link kind="article" to={path} rel={rel}>
              <ThumbCover cover={coverUrl} hasGradient={isFullCover} role="img" aria-label={name} />
            </Link>
          )}
        </LazyContent>
      </CoverWrapper>
      <Inner>
        <Header>
          <Link kind="article" to={path} rel={rel}>
            <Title>
              <ArticlesIcon article={article} size={isFullCover ? 'lg' : 'xs'} />
              {name}
            </Title>
          </Link>
          {summary && <Description>{summary}</Description>}
        </Header>
        <Footer>
          <Metas
            align="left"
            background={isFullCover ? 'dark' : mode}
            $mobileAlign="left"
            size="sm"
          >
            <Meta
              value={
                <CustomFormattedDate
                  value={publishedAt}
                  format="long"
                  updateIntervalInSeconds={60}
                />
              }
            />
            {universes.map(universe => (
              <Fragment key={universe}>
                <Meta value="•" />
                <Meta value={universe} />
              </Fragment>
            ))}
          </Metas>
          {actions && <Actions>{actions}</Actions>}
        </Footer>
      </Inner>
    </Thumb>
  )
}

ArticlesThumb.propTypes = {
  ...WithLocalePropTypes,
  actions: PropTypes.node,
  hoverSize: PropTypes.oneOf(['md', 'lg']),
  onClick: PropTypes.func,
  collectionCategorySlug: PropTypes.string,
  collectionSlug: PropTypes.string,
  isFullCover: PropTypes.bool,
}

ArticlesThumb.defaultProps = {
  hoverSize: 'md',
}

export default ArticlesThumb
