import styled, { css, th } from '@xstyled/styled-components'
import { fontSize, transition } from '../../utils/theme-helpers'
import { getBackgroundImageWithGradient, media } from '../utils/utils.styled'
import {
  Card,
  CardInner,
  CardCoverWrapper,
  CardCover,
  CardTitle,
  CardDescription,
} from '../card.styled'
import CmsLink from '../link'
import { Metas } from '../metas/metas.styled'
import MetaItem from '../metas/item'

const getHoverSize = size => {
  switch (size) {
    case 'md':
      return '62%'

    case 'lg':
      return '76%'

    default:
      return '62%'
  }
}

export const CoverWrapper = styled(CardCoverWrapper)``

export const ThumbCover = styled(CardCover)`
  ${props => props.hasGradient && getBackgroundImageWithGradient(props.cover)}
`

export const Inner = styled(CardInner)`
  padding: xxl;
  transition: ${transition('sm')};
`

export const Header = styled.header`
  position: relative;
  z-index: 2;
`

export const Title = styled(CardTitle)`
  ${th('texts.h5')};
`

export const Description = styled(CardDescription)`
  color: dark.200;
`

export const Footer = styled.div`
  display: flex;
  background-color: inherit;
`

export const Actions = styled.div`
  display: flex;
  margin-left: auto;
`

export const Link = styled(CmsLink)``

const thumbVerticalStyles = hoverSize => css`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  height: 25rem;
  cursor: pointer;

  ${CoverWrapper} {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 60%;
    flex: 0 1 auto;
  }

  ${Inner} {
    display: flex;
    flex-direction: column;
    height: 40%;
    flex: none;
    z-index: 1;
  }

  ${Description} {
    opacity: 0;
    padding-top: sm;
    ${th('texts.body2')};
    color: dark.200;
    transition: medium;
  }

  ${Header} {
    flex: 0 1 auto;
  }

  ${Footer} {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    z-index: 30;
    align-items: flex-end;
    padding: lg xxl xxl xxl;
    flex: 1 0 auto;
    margin-top: xxl;
  }

  ${Link} {
    flex-direction: column;
    justify-content: flex-end;
  }

  &:hover {
    ${Inner} {
      height: ${hoverSize};
    }
    ${Description} {
      opacity: 1;
    }
  }

  ${media.mobile`
    height: auto;

    &:hover {
      ${Inner} {
        height: auto;
      }
    }

    ${CoverWrapper} {
      height: 10rem;
    }

    ${Inner} {
      height: auto;
      padding: xxl;
      margin-top: 10rem;
    }

    ${Description} {
      display: none;
    }

    ${Footer} {
      position: static;
      padding: 0;
    }

    ${Actions} {
      position: static;
      transform: none;
    }
  `};
`

const thumbHorizontalStyles = hoverSize => css`
  display: flex;
  width: 100%;
  cursor: pointer;

  ${Link} {
    width: 100%;
    flex-direction: row;
  }

  ${CoverWrapper} {
    width: 30%;
    flex: none;
  }

  ${Inner} {
    flex: 1 1 auto;
    padding-right: 5xl;
  }

  ${Description} {
    font-size: ${fontSize('md')};
    margin-top: lg;
  }

  ${Footer} {
    margin-top: lg;
  }

  ${Actions} {
    position: absolute;
    bottom: xxl;
    right: xxl;
  }

  ${media.mobile`
    ${thumbVerticalStyles(hoverSize)}
  `};
`

const isFullCoverStyles = css`
  ${CoverWrapper} {
    height: 100%;
  }

  ${Inner} {
    background-color: transparent;
    min-height: 200;
    transition: medium;
  }

  ${Title} {
    ${th('texts.h2')};
    color: light.900;
  }

  ${Description} {
    color: light.900;
  }

  &:hover {
    ${Inner} {
      height: ${getHoverSize('lg')};
    }
  }

  ${media.tablet`
    ${Inner} {
      height: 60%;
    }
  `}
`

export const Thumb = styled(Card)(
  ({ orientation, hoverSize, isFullCover, $isVisited }) => css`
    ${orientation === 'horizontal'
      ? thumbHorizontalStyles(getHoverSize(hoverSize))
      : thumbVerticalStyles(getHoverSize(hoverSize))};
    ${isFullCover && isFullCoverStyles}
    ${$isVisited &&
    css`
      ${CoverWrapper},
      ${Title} {
        opacity: 0.55;
      }
    `}
  `
)

export const ThumbLoading = styled(Thumb)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`

export const Meta = styled(MetaItem)`
  ${Metas} & {
    margin-right: xxs;
  }
`
