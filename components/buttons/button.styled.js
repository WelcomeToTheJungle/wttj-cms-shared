import React from 'react'
import styled, { css, keyframes, th } from '@xstyled/styled-components'
import { bool, string, oneOf } from 'prop-types'
import { Link } from 'react-router-dom'
import { media, overflowEllipsis } from '../utils/utils.styled'
import { LoadingSpinner } from '../loading.styled'
import {
  buttonIconWidth,
  color,
  rgba,
  fontSize,
  fontWeight,
  letterSpacing,
  radius,
  roundedButtonSize,
  textStyles,
  transition,
} from '../../utils/theme-helpers'

const pulsing = keyframes`
  from {
    transform: scale(1);
    opacity: .1;
  }
  to {
    transform: scale(1.8);
    opacity: .2;
  }
`

const pulsingRule = css`
  ${pulsing} 1.5s ease infinite alternate;
`

export const sizeMdStyles = css`
  padding: lg xxl;
  ${textStyles('button')};

  ${media.mobile`
    padding: lg;
  `};
`

export const sizeXsStyles = css`
  padding: xs;
  ${textStyles('button_small')};
`

export const sizeSmStyles = css`
  padding: xs md;
  ${textStyles('button_small')};
`

export const sizeLgStyles = css`
  padding: lg 5xl;
  font-size: ${fontSize('md')};
  font-weight: ${fontWeight('bold')};
  letter-spacing: ${letterSpacing('lg')};
`

export const primaryStyles = props => css`
  background: ${color('primary')};
  color: ${color('white')};

  &:hover {
    color: ${color('white')};
  }

  &[disabled] {
    color: ${color('white')};
    background-color: ${color('gray', '200')};
  }
`

export const primaryOutlineStyles = props => css`
  background-color: ${color('white')};
  color: ${color('primary')};
  border: 1px solid ${color('primary')};
  font-weight: ${fontWeight('bold')};

  &:hover {
    background-color: ${color('green', '100')};
    color: ${color('primary')};
  }
`

export const closeDarkStyles = props => css`
  background: ${rgba('dark', '900', '0.3')};
  color: ${color('white')};

  &:hover {
    color: ${color('white')};
    &::before {
      background: ${rgba('dark', '900', 0.15)};
    }
  }

  svg path {
    fill: ${props => (props.iconColor ? null : color('white'))};
  }
`

export const darkStyles = props => css`
  background: ${color('gray', '500')};
  color: ${color('white')};

  &:hover {
    color: ${color('white')};
  }

  svg path {
    fill: ${props => (props.iconColor ? null : color('white'))};
  }
`

export const lightStyles = props => css`
  background: ${color('white')};
  color: ${color('texts', 'light')};

  &:hover {
    color: ${color('texts', 'light')};
    &::before {
      background: ${rgba('dark', '900', 0.02)};
    }
  }

  svg path {
    fill: ${color('texts', 'xlight')};
  }
`

export const neutralStyles = props => css`
  background: ${color('gray', '150')};
  color: ${color('gray', '300')};

  &:hover {
    color: ${color('gray', '300')};
    &::before {
      background: ${rgba('dark', '900', 0.02)};
    }
  }

  svg path {
    fill: ${color('gray', '300')};
  }
`

export const dangerStyles = props => css`
  background: ${color('white')};
  color: ${color('red', '500')};

  &:hover {
    color: ${color('red', '500')};
    &::before {
      background: ${rgba('dark', '900', 0.02)};
    }
  }

  svg path {
    fill: ${color('red', '500')};
  }
`

export const dangerReverseStyles = props => css`
  background: ${color('red', 500)};
  color: ${color('white')};

  &:hover {
    color: ${color('white')};
    &::before {
      background: ${rgba('dark', '900', 0.02)};
    }
  }

  svg path {
    fill: ${props => (props.iconColor ? null : color('white'))};
  }
`

export const warningStyles = props => css`
  background: ${color('white')};
  color: ${color('orange', '400')};

  &:hover {
    color: ${color('orange', '400')};
    &::before {
      background: ${rgba('dark', '900', 0.02)};
    }
  }

  svg path {
    fill: ${color('orange', '400')};
  }
`

export const nakedStyles = props => css`
  padding: 0;
  color: ${color('texts', 'light')};
  box-shadow: none;
  border: none;
  font-size: ${fontSize('md')};
  letter-spacing: ${letterSpacing('md')};

  &:hover {
    color: ${color('texts', 'dark')};
  }

  svg path {
    fill: ${color('texts', 'xlight')};
  }
`

export const linkedinStyles = props => css`
  background: ${color('linkedin', 'secondary')};
  color: ${color('white')};

  &[disabled] {
    background: ${color('grey', '200')};
  }

  &:hover {
    color: ${color('white')};
  }
`

export const roundedStyles = css`
  padding: 0;
  align-items: center;
  justify-content: center;
  transform: translateZ(0);
  border-radius: 50%;

  svg {
    display: block;
    transform: rotate(0deg);
    backface-visibility: hidden;
    -webkit-transform-style: preserve-3d;
    -webkit-transform: translate3D(0, 0, 0) scale(1) skewY(0deg);
  }
`

export const roundedSizeAutoStyles = css`
  width: auto;
  height: auto;
`

export const roundedSizeSmStyles = css`
  width: ${roundedButtonSize('sm')};
  height: ${roundedButtonSize('sm')};
`

export const roundedSizeMdStyles = css`
  width: ${roundedButtonSize('md')};
  height: ${roundedButtonSize('md')};
`

export const roundedSizeLgStyles = css`
  width: ${roundedButtonSize('lg')};
  height: ${roundedButtonSize('lg')};
`

export const borderRoundedStyles = css`
  border-radius: 20px;
`

export const fullWidthStyles = css`
  width: 100%;
  &:not(:last-child) {
    margin-bottom: xxl;
  }
`

export const pulsingStyles = css`
  overflow: visible;

  &::before {
    position: absolute;
    width: 100%;
    height: 100%;
    content: ' ';
    background: inherit;
    opacity: 0.1;
    border-radius: 50%;
    transform-origin: center;
    animation: ${pulsingRule};
  }
`

export const ButtonText = styled.span``

export const ButtonLabel = styled.span`
  display: inline-block;
`

export const withIconStyles = css`
  text-align: left;
`

function getButtonMode(props) {
  switch (props.mode) {
    case 'primary':
      return primaryStyles()
    case 'primary-outline':
      return primaryOutlineStyles()
    case 'success':
      return primaryStyles()
    case 'close-dark':
      return closeDarkStyles()
    case 'dark':
      return darkStyles()
    case 'light':
      return lightStyles()
    case 'neutral':
      return neutralStyles()
    case 'danger':
      return dangerStyles()
    case 'danger-reverse':
      return dangerReverseStyles()
    case 'error':
      return dangerStyles()
    case 'warning':
      return warningStyles()
    case 'naked':
      return nakedStyles()
    case 'linkedin':
      return linkedinStyles()
    default:
      return primaryStyles()
  }
}

function getButtonSize(size, rounded) {
  if (rounded) {
    switch (size) {
      case 'auto':
        return roundedSizeAutoStyles
      case 'sm':
        return roundedSizeSmStyles
      case 'md':
        return roundedSizeMdStyles
      case 'lg':
        return roundedSizeLgStyles
      default:
        return roundedSizeMdStyles
    }
  } else {
    switch (size) {
      case 'xs':
        return sizeXsStyles
      case 'sm':
        return sizeSmStyles
      case 'md':
        return sizeMdStyles
      case 'lg':
        return sizeLgStyles
      default:
        return sizeMdStyles
    }
  }
}

export const inlineLinkStyles = css`
  display: inline;
  position: relative;
  color: ${color('texts', 'dark')};
  font-weight: ${fontWeight('regular')};
  transition: ${transition('md')};
  background-image: linear-gradient(${color('gray', '200')}, ${color('gray', '200')});
  background-position: 0% 100%;
  background-repeat: no-repeat;
  background-size: 100% 1px;

  &:hover {
    background-image: linear-gradient(${color('primary')}, ${color('primary')});
  }
`

export const buttonResetStyles = css`
  cursor: pointer;
  outline: none;
  border: none;
  background: none;
`

export const buttonStyles = css`
  position: relative;
  display: ${props => (props.type === 'block' ? 'block' : 'inline-flex')};
  width: ${props => (props.type === 'block' ? '100%' : 'auto')};
  max-width: 100%;
  align-items: center;
  align-self: ${({ alignself }) => alignself || null};
  justify-content: ${({ align }) => align || 'center'};
  width: auto;
  text-align: ${({ align }) => align || 'center'};
  text-transform: uppercase;
  white-space: nowrap;
  cursor: pointer;
  outline: none;
  border: none;
  appearance: none;
  overflow: hidden;
  border-radius: ${radius('sm')};
  transition: ${transition('sm')};
  line-height: 1;
  ${buttonResetStyles};

  &::before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    transition: ${transition('sm')};
  }

  &:active {
    transform: translateY(2px);
    box-shadow: 0 0 4px ${rgba('dark', '900', 0.2)};
  }

  &:hover {
    ${media.mobile`
      transform: translateY(2px);
    `}
    &::before {
      background: ${rgba('light', '900', 0.15)};
    }
  }

  &[disabled] {
    pointer-events: none;
  }

  svg {
    display: block;

    & + span,
    & + ${ButtonText} {
      margin-left: md;
    }
  }

  ${ButtonLabel} {
    margin-left: ${buttonIconWidth()};
  }

  ${ButtonText},
  span {
    ${overflowEllipsis};
    & + svg {
      margin-left: md;
    }
  }

  ${props => getButtonSize(props.size, props.rounded)}
  ${props => getButtonMode(props)}
  ${props => (props.rounded ? roundedStyles : null)}
  ${props => (props.borderRounded ? borderRoundedStyles : null)}
  ${props => (props.pulsing ? pulsingStyles : null)}
  ${props => (props.withicon ? withIconStyles : null)}
  ${props => (props.w === 'full' ? fullWidthStyles : null)}
`

/** @component */
export const Button = styled(
  ({ withicon, iconColor, mode, size, borderRounded, withSpinner, withIcon, rounded, ...rest }) => (
    <button {...rest} />
  )
)`
  ${buttonStyles};

  ${LoadingSpinner} {
    margin-left: md;
    margin-top: calc(${th('space.lg')} * -1);
    margin-right: calc(${th('space.lg')} * -1);
    margin-bottom: calc(${th('space.lg')} * -1);
  }
`

Button.propTypes = {
  /** Just for `LinkButton` and `InlineLink` */
  to: string,
  /** Just for `HrefButton` */
  href: string,
  /** Normally one of `primary`, `dark`, `light`, `neutral`, `danger` or `linkedin`: */
  mode: string,
  size: oneOf(['auto', 'sm', 'md', 'lg']),
  width: oneOf(['full', 'half']),
  rounded: bool,
  pulsing: bool,
  borderRounded: bool,
  withicon: bool,
}

// Only pass real DOM props through
export const LinkButton = styled(({ withicon, iconColor, mode, size, rounded, ...rest }) => (
  <Link {...rest} />
))`
  ${buttonStyles};
`

LinkButton.propTypes = {
  to: string.isRequired,
  /** `true` to use original icon color (and not whitewash the icon) */
  iconColor: bool,
  ...Button.propTypes,
}

export const HrefButton = styled(({ withicon, iconColor, mode, size, ...rest }) => <a {...rest} />)`
  ${buttonStyles};
`

HrefButton.propTypes = {
  href: string.isRequired,
  ...Button.propTypes,
}

export const InlineLink = styled(({ withicon, iconColor, mode, size, ...rest }) => (
  <Link {...rest} />
))`
  ${inlineLinkStyles};
`

InlineLink.propTypes = {
  to: string.isRequired,
  ...Button.propTypes,
}

export const InlineHref = styled(({ withicon, iconColor, mode, size, ...rest }) => <a {...rest} />)`
  ${inlineLinkStyles};
`

InlineHref.propTypes = {
  to: string.isRequired,
  ...Button.propTypes,
}

export const ButtonIcon = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  padding: md;
  background: ${rgba('dark', '900', 0.1)};
`

export default Button

export const ButtonsGroup = styled.div`
  display: flex;
  justify-content: center;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: xxl;
  max-width: ${props => (props.maxwidth ? props.maxwidth : '100%')};

  &:last-child {
    margin-bottom: 0;
  }

  button,
  a {
    flex-grow: ${({ buttonsize }) => (buttonsize === 'auto' ? 0 : 1)};
    flex-shrink: 1;
    flex-basis: ${({ buttonsize }) => (buttonsize === 'auto' ? 'auto' : 1)};
    margin: 0 md;

    &:first-child {
      margin-left: 0;
    }

    &:last-child {
      margin-right: 0;
    }

    ${media.mobile`
      margin: 0 xs;
    `};
  }
`
