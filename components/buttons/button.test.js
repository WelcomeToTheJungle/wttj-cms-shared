import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { mount } from '../../test/support'

import { Button, LinkButton, HrefButton } from './button.styled'
import theme from '../../constants/theme.styled'

test('<Button> renders correctly and has white text by default', () => {
  const button = mount(<Button theme={theme}>Test button</Button>)
  expect(button.html().includes('Test button')).to.equal(true)
  expect(button).toHaveStyleRule('color', '#ffffff')
  expect(button).toHaveStyleRule('background', '#FFCD00')
})

test('<Button> has correct colour for mode light', () => {
  const button = mount(
    <Button theme={theme} mode="light">
      Test button
    </Button>
  )
  expect(button).toHaveStyleRule('color', '#97999d')
})

test('<Button> has correct colour for mode dark', () => {
  const button = mount(
    <Button theme={theme} mode="dark">
      Test button
    </Button>
  )
  expect(button).toHaveStyleRule('color', '#ffffff')
})

test('<Button> has correct colour for mode neutral', () => {
  const button = mount(
    <Button theme={theme} mode="neutral">
      Test button
    </Button>
  )
  expect(button).toHaveStyleRule('color', '#97999d')
})

test('<Button> has correct colour for mode danger', () => {
  const button = mount(
    <Button theme={theme} mode="danger">
      Test button
    </Button>
  )
  expect(button).toHaveStyleRule('color', '#d32f2f')
})

test('<Button> has correct colour for mode linkedin', () => {
  const button = mount(
    <Button theme={theme} mode="linkedin">
      Test button
    </Button>
  )
  expect(button).toHaveStyleRule('color', '#ffffff')
})

test('<LinkButton> renders correctly', () => {
  const button = mount(
    <MemoryRouter>
      <LinkButton theme={theme} to="#nowhere">
        Link button
      </LinkButton>
    </MemoryRouter>
  )
  expect(button.text()).to.equal('Link button')
  expect(button).toHaveStyleRule('color', '#ffffff')
})

test('<HrefButton> renders correctly', () => {
  const button = mount(
    <MemoryRouter>
      <HrefButton theme={theme} href="#nowhere">
        Href button
      </HrefButton>
    </MemoryRouter>
  )
  expect(button.text()).to.equal('Href button')
  expect(button).toHaveStyleRule('color', '#ffffff')
})
