import React from 'react'
import { FormattedMessage } from 'react-intl'
import { withRouter } from 'react-router-dom'

import { CardSurtitle, CardLogoImg } from '../card.styled'
import CmsLink from '../link'
import MetaItem from '../metas/item'
import { Metas } from '../metas/metas.styled'
import { safeGetPath } from '../../utils/i18n'
import LazyContent from '../lazy_content'
import { LoadingImg } from '../loading.styled'
import CustomFormattedDate from '../dates/formatted_date'
import onThumbClick from '../../utils/thumb-click'
import { getVisitedResource } from '../../utils/visited-resources'

import * as S from './thumb.styled'

const JobsThumb = props => {
  const {
    highlights,
    job,
    mainPage,
    multiOrganization,
    lang,
    getPath,
    onClick,
    history,
    rel,
    actions,
  } = props

  const handleClick = e => {
    onClick && onClick(e)

    onThumbClick({
      event: e,
      path: jobPath,
      push: history.push,
    })
  }

  if (!job) {
    return null
  }

  const websiteOrganizationSlug = job.getIn(['organization', 'website_organization', 'slug'])
  const jobSlug = job.get('slug')
  const jobPath = safeGetPath(
    getPath,
    `/companies/${websiteOrganizationSlug}/jobs/${jobSlug}`,
    lang
  )

  const name = job.get('name')
  const nameHighlight = highlights?.name || name

  const organizationName = job.getIn(['organization', 'name'])
  const organizationNameHighlight = highlights?.organizationName || organizationName

  const displayDetail = mainPage || multiOrganization

  const city = job.getIn(['office', 'city'])
  const remote = job.get('remote')
  const contractType = job.get('contract_type')
  const publishedAt = job.get('published_at')
  const logoUrl = job.getIn(['organization', 'logo', 'url'])
  const coverUrl = job.getIn(['organization', 'cover_image', lang, 'small', 'url'])

  const shouldShowRemote = remote === 'partial' || remote === 'fulltime'
  const shouldShowCity = !shouldShowRemote && city

  const isVisited = getVisitedResource('jobs', job)

  return (
    <S.Thumb onClick={handleClick} data-role="jobs:thumb" $isVisited={isVisited}>
      {mainPage && (
        <S.CoverWrapper>
          <LazyContent
            placeHolder={({ forwardRef }) => (
              <S.Cover aria-label={nameHighlight} role="img" ref={forwardRef}>
                <LoadingImg />
              </S.Cover>
            )}
          >
            {coverUrl && (
              <S.Cover
                aria-label={nameHighlight}
                as={CmsLink}
                cover={coverUrl}
                role="img"
                to={jobPath}
                rel={rel}
              />
            )}
            {displayDetail && (
              <S.Logo>
                <LazyContent placeHolder={({ forwardRef }) => <LoadingImg ref={forwardRef} />}>
                  {logoUrl && <CardLogoImg alt={organizationName} src={logoUrl} />}
                </LazyContent>
              </S.Logo>
            )}
          </LazyContent>
        </S.CoverWrapper>
      )}
      <S.Infos>
        <S.Header>
          {displayDetail && <CardSurtitle>{organizationNameHighlight}</CardSurtitle>}
          <CmsLink to={jobPath} rel={rel}>
            <S.Title displayDetail={displayDetail}>{name}</S.Title>
          </CmsLink>
          <Metas size="md" align="left" $mobileDirection="column">
            {contractType && (
              <MetaItem
                label="write"
                value={
                  <FormattedMessage id={`cms.jobs.contract-types.${contractType.toLowerCase()}`} />
                }
              />
            )}
            {shouldShowRemote && (
              <MetaItem
                label="remote"
                value={<FormattedMessage id={`cms.jobs.remote.${remote}`} />}
              />
            )}
            {shouldShowCity && <MetaItem label="office" capitalize value={city} />}
            {publishedAt && (
              <MetaItem
                label="calendar"
                value={<CustomFormattedDate format="short" isRelative value={publishedAt} />}
              />
            )}
          </Metas>
        </S.Header>
        {actions && <S.Actions>{actions}</S.Actions>}
      </S.Infos>
    </S.Thumb>
  )
}

export default withRouter(JobsThumb)
