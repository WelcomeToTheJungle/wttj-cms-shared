import styled, { css, th } from '@xstyled/styled-components'
import { Card, CardCoverWrapper, CardCover, CardLogo, CardTitle } from '../card.styled'
import { media } from '../utils/utils.styled'
import { Meta } from '../metas/metas.styled'
import { imagesHeight, coverSize } from '../../utils/theme-helpers'

export const CoverWrapper = styled(CardCoverWrapper)`
  width: ${coverSize('jobs', 'thumb', 'width')};
  z-index: 1;
  overflow: visible;

  ${media.mobile`
    width: 100%;
    height: ${imagesHeight('organizations', 'thumb', 'cover')};
    margin-right: 0;
  `};
`

export const Cover = styled(CardCover)`
  display: block;
`

export const Header = styled.header`
  flex: 1 1 auto;
  margin-left: 3xl;

  ${media.mobile`
    margin-left: 0;
  `}
`

export const Logo = styled(CardLogo)`
  position: absolute;
  top: 50%;
  right: 0;
  transform: translate(50%, -50%);
  margin: 0;
  border-width: 1px;
  border-style: solid;
  border-color: light.800;

  ${media.mobile`
    margin: 0;
    right: 0;
    left: 50%;
    transform: translate(-50%, -50%);
  `};
`

export const Title = styled(CardTitle)`
  color: dark.900;
  margin-top: ${props => (props.displayDetail ? th('space.sm') : 0)};
  margin-bottom: sm;
`

export const Infos = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  z-index: 2;
  flex: 1 1 auto;
  padding: 3xl;

  ${media.mobile`
    align-items: flex-start;
    padding: lg;
  `};
`

export const Thumb = styled(Card)(
  ({ $isVisited }) => css`
    margin-bottom: xxl;
    flex-direction: row;
    cursor: pointer;

    &:last-child {
      margin-bottom: 0;
    }

    ${Meta} {
      &:last-child {
        ${media.mobile`
        margin-top: xxl;
      `};
      }
    }

    ${media.mobile`
      flex-direction: column;
    `};

    ${$isVisited &&
    css`
      ${Cover},
      ${Title} {
        opacity: 0.55;
      }
    `}
  `
)

export const LoadingThumb = styled(Thumb)`
  display: flex;
  pointer-events: none;
`

export const Actions = styled.div`
  display: flex;
  flex: none;
  margin-left: xxl;

  ${media.mobile`
    position: absolute;
    right: ${th('space.xxl')};
    bottom: ${th('space.xxl')};
  `};
`
