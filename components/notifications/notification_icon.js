import React from 'react'
import { Icons } from '@welcome-ui/icons.font'

import { IconWrapper } from './notification.styled'

const ICONS = {
  success: Icons.Check,
  error: Icons.Alert,
  warning: Icons.Alert,
  info: Icons.Information,
  default: Icons.Information,
}

const NotificationIcon = kind => {
  const Icon = ICONS[kind] || ICONS.default
  return (
    <IconWrapper>
      <Icon />
    </IconWrapper>
  )
}

export default NotificationIcon
