import styled, { css, keyframes, th } from '@xstyled/styled-components'
import { Stack } from '@welcome-ui/stack'
import { media } from '../utils/utils.styled'
import {
  color,
  boxShadow,
  fontSize,
  fontWeight,
  headerUpperNavHeight,
  radius,
  rgba,
} from '../../utils/theme-helpers'
import { Button } from '../buttons/button.styled'

const slideInLeft = keyframes`
  from {
    transform: translateX(100%);
  }
  to {
    transform: translateX(0);
  }
`

const slideInLeftRule = css`
  ${slideInLeft} 0.5s both;
`

const successNotificationStyles = css`
  border-left-color: ${color('primary')};
  color: ${color('primary')};
`

const errorNotificationStyles = css`
  border-left-color: ${color('red', '500')};
  color: ${color('red', '500')};
`

const warningNotificationStyles = css`
  border-left-color: ${color('orange', '400')};
  color: ${color('orange', '400')};
`

const infoNotificationStyles = css`
  border-left-color: ${color('blue', 400)};
  color: ${color('blue', '400')};
`

const staticNotificationStyles = css`
  position: relative;
  margin-bottom: xxl;
  padding: xxl 0 xxl xxl;
`

const fixedNotificationStyles = css`
  animation: ${slideInLeftRule};
  margin-bottom: lg;
  padding: lg;
  min-width: 16rem;
`

const getNotificationStyles = kind => {
  switch (kind) {
    case 'success':
      return successNotificationStyles
    case 'error':
      return errorNotificationStyles
    case 'warning':
      return warningNotificationStyles
    case 'info':
      return infoNotificationStyles
    default:
      return infoNotificationStyles
  }
}

export const StyledNotifications = styled(Stack)`
  position: fixed;
  z-index: 9999;
  max-width: 25rem;
  right: ${th('space.lg')};
  top: -1px;

  ${media.mobile`
    top: ${th('space.xs')};
    right: auto;
    left: ${th('space.xs')};
    width: calc(100vw - ${th('space.xs')} * 2)
  `};
`

export const StyledNotification = styled.div`
  background-color: ${color('white')};
  border-left: ${th('space.5xl')} solid ${color('white')};
  border-radius: ${radius('md')};
  box-shadow: ${boxShadow('md')};
  box-sizing: border-box;
  line-height: 1.4;
  ${props => getNotificationStyles(props.kind)};
  ${props => (props.mode === 'static' ? staticNotificationStyles : fixedNotificationStyles)};
  &:last-child {
    margin-bottom: 0;
  }
`

export const IconWrapper = styled.div`
  position: absolute;
  top: 50%;
  right: calc(100% + ${th('space.5xl')} / 2);
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${th('space.3xl')};
  height: ${th('space.3xl')};
  transform: translate(50%, -50%);
  background-color: ${rgba('light', '900', 0.2)};
  border-radius: 50%;
  svg {
    max-height: 50%;
  }
  path {
    fill: ${color('white')};
  }
`

export const CloseButton = styled(Button)`
  position: absolute;
  top: ${th('space.md')};
  right: ${th('space.md')};
  margin: 0;
`

export const NotificationTitle = styled.h4`
  font-weight: ${fontWeight('bold')};
  font-size: ${fontSize('md')};
`

export const NotificationContent = styled.div`
  margin-top: xs;
  color: ${color('gray', '350')};
  font-size: ${fontSize('sm')};
`

export const NotificationFooter = styled.footer`
  margin-top: md;
`
