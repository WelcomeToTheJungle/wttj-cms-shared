export const getAlt = (title, organizationName) =>
  [title, organizationName].filter(str => str).join(' - ')

export const getOrganizationName = (content, contentProps) =>
  content.getIn(['properties', 'organization', 'name']) ||
  (contentProps && contentProps.organization && contentProps.organization.get('name'))

export const getTitle = content =>
  content.getIn(['properties', 'title']) || content.getIn(['properties', 'name'])

export const getName = content => content.getIn(['properties', 'name'])

export const getSubtitle = content => content.getIn(['properties', 'surtitle'])

const SIZES = ['small', 'medium', 'large']

export const getContentSize = columns => SIZES[columns - 1] || SIZES[1]
