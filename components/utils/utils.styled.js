import { css, th } from '@xstyled/styled-components'
import { breakpoint, transition, rgba } from '../../utils/theme-helpers'

// Custom scrollbar mixin
export const scrollbar = (w, thumbColor, trackColor) => {
  return `
    &::-webkit-scrollbar {
        width:  ${w};
        height: ${w};
    }

    &::-webkit-scrollbar-thumb {
        background: ${thumbColor};
        border-radius: 10px;
    }

    &::-webkit-scrollbar-track {
        background: ${trackColor};
        border-radius: 10px;
    }

    & {
      scrollbar-face-color: ${thumbColor};
      scrollbar-track-color: ${trackColor};
    }
  `
}

export const getBackgroundImage = image => {
  return image
    ? css`
        background-image: url('${image}');
      `
    : null
}

export const getBackgroundImageWithGradient = image => {
  return (
    image &&
    css`
      background-image: linear-gradient(to bottom, transparent 0%, ${th('colors.dark.900')} 100%),
        url('${image}');
    `
  )
}

export const getHeaderOffset = props => {
  const showLanguageAlert = props.showLanguageAlert
  const offset = showLanguageAlert ? th('header.desktop.languageAlert.height')(props) : 0
  return offset
}

export const media = {
  print: (...args) => css`
    @media print {
      ${css(...args)};
    }
  `,
  mobile: (...args) => css`
    @media (max-width: ${breakpoint('mobile')}) {
      ${css(...args)};
    }
  `,
  tablet: (...args) => css`
    @media (max-width: ${breakpoint('tablet')}) {
      ${css(...args)};
    }
  `,
  smallscreen: (...args) => css`
    @media (max-width: ${breakpoint('smallscreen')}) {
      ${css(...args)};
    }
  `,
  mediumscreen: (...args) => css`
    @media (max-width: ${breakpoint('mediumscreen')}) {
      ${css(...args)};
    }
  `,
  widescreen: (...args) => css`
    @media (max-width: ${breakpoint('widescreen')}) {
      ${css(...args)};
    }
  `,
}

export const overflowEllipsis = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const backgroundCover = css`
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`

export const bannerMask = css`
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;

  &::after {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${rgba('dark', '900', 0.5)};
    transition: ${transition('md')};
    content: ' ';
  }
`

export const coverMask = css`
  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${rgba('dark', '900', 0.3)};
    transition: medium;
  }
`

export const printHiddenStyles = css`
  ${media.print`
    display: none;
  `};
`

export const hoverCardStyles = css`
  transition: medium;

  &:hover {
    border-color: light.700;
  }
`

export const hoverScaleStyles = css`
  will-change: transform;
  transition: medium;

  &:hover {
    transform: scale(1.02);
  }
`
