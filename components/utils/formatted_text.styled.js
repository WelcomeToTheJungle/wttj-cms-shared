import styled, { css, th } from '@xstyled/styled-components'
import { media } from './utils.styled'
import {
  centeredContainerWidth,
  color,
  fontSize,
  fontSizeEm,
  ratio,
} from '../../utils/theme-helpers'

export const linkStyles = css`
  ${th('links.default')};
  ${th('underline.default')};

  &:hover {
    ${th('underline.hover')};
  }
`

export const titleStyles = css`
  position: relative;
  margin-bottom: 3xl;

  &::before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    height: 4px;
  }

  &:last-child {
    margin-bottom: 0;
  }
`

export const h2Styles = css`
  ${th('texts.h3')};
  padding-left: 35px;
  padding-bottom: sm;
  border-bottom: 1px solid;
  border-bottom-color: light.800;

  &::before {
    width: 20px;
    background-color: primary.500;
    top: 14;
  }

  ${media.mobile`
    ${th('texts.h4')};
  `}

  ${media.print`
    padding-left: 0;

    &::before {
      display: none;
    }
  `};
`

export const h3Styles = css`
  padding-left: 30;
  ${th('texts.h4')};

  &::before {
    width: 20;
    background-color: nude.200;
    top: 10;
  }
`

export const h4Styles = css`
  padding-left: calc(${th('space.5xl')} + ${th('space.md')});
  font-size: ${fontSizeEm('md')};

  &::before {
    width: 5xl;
    background-color: ${color('gray', '150')};
    top: 6;
  }
`

const formattedTextResponsiveStyles = css`
  ${media.mobile`
    li {
      margin-left: 0;
    }

    .medium-insert-images,
    .medium-insert-embeds,
    .images-swiper-wrapper,
    .video-wrapper {
      margin: 5xl 0;
    }
  `};
`

const formattedTextInheritStyles = css`
  p,
  li {
    color: inherit;
  }

  blockquote {
    color: light.900;
  }
`

const formattedTextLightStyles = css`
  p,
  li {
    color: dark.200;
  }

  blockquote {
    color: light.900;
  }

  h2,
  h3,
  h4 {
    color: dark.900;
  }
`

const formattedTextDarkStyles = css`
  p,
  li {
    color: light.700;

    a {
      color: light.900;
    }
  }

  blockquote {
    color: light.900;

    a {
      color: light.900;
    }
  }

  h2,
  h3,
  h4 {
    color: light.900;

    a {
      color: light.900;
    }
  }
`

const getColorthemeStyles = colortheme => {
  if (colortheme === 'dark') return formattedTextDarkStyles
  if (colortheme === 'inherit') return formattedTextInheritStyles
  return formattedTextLightStyles
}

const formattedTextPrintStyles = css`
  ${media.print`
    ul li {
      list-style-type: disc;
      padding-left: 0;

      &::before {
        display: none;
      }
    }

    .medium-insert-images,
    .medium-insert-embeds,
    .video-wrapper {
      width: 70%;
      margin-left: auto;
      margin-right: auto;
    }

    .swiper-container {
      display: none;
    }

    .video-wrapper {
      display: none;
    }
  `};
`

export const BoldText = styled.span`
  font-weight: bold;
`

export const FormattedText = styled.div`
  font-size: inherit;
  word-break: break-word;
  margin-bottom: xxl;

  &:last-child {
    margin-bottom: 0;
  }

  &.article {
    iframe {
      height: calc(${centeredContainerWidth('sm')} / ${ratio('720p')}) !important;
      position: static !important;
    }
  }
  p,
  li,
  blockquote {
    ${th('texts.body1')};
    line-height: 1.5em;

    a {
      ${linkStyles};
    }
  }

  p,
  .medium-insert-images,
  .medium-insert-embeds,
  h2,
  h3,
  ul,
  ol,
  blockquote {
    margin-bottom: 3xl;

    &:last-child {
      margin-bottom: 0;
    }
  }

  p + ul,
  p + ol {
    margin-top: xs;
  }

  li {
    position: relative;
    margin-left: xl;
    padding-left: xl;
    margin-bottom: lg;

    &:last-child {
      margin-bottom: 0;
    }
  }

  em,
  i {
    font-style: italic;
  }

  strong,
  b {
    font-weight: bold;
  }

  ul li {
    &::before {
      content: '';
      position: absolute;
      left: 0;
      top: 0.6em;
      width: 8px;
      height: 8px;
      background-color: primary.500;
      border-radius: 50%;
    }
  }

  ol li {
    counter-increment: item;

    &::before {
      content: counter(item) '.';
      position: absolute;
      top: 0.16em;
      left: 0;
      font-size: inherit;
      color: primary.500;
      font-weight: normal;
    }

    &:first-child {
      counter-reset: item;
    }

    li {
      counter-increment: inside-item;
      padding-left: calc(${th('space.xxl')} + 0.5rem);

      &::before {
        content: counter(item) '.' counter(inside-item) '.';
      }

      &:first-child {
        counter-reset: inside-item;
      }
    }
  }

  .same-counter {
    counter-reset: global-item;

    & > ol {
      & > li {
        counter-increment: global-item;

        &::before {
          content: counter(global-item) '.';
        }

        ol li {
          &::before {
            content: counter(global-item) '.' counter(inside-item) '.';
          }
        }
      }
    }
  }

  h2,
  h3,
  h4 {
    ${titleStyles};

    a {
      ${linkStyles};
      font-weight: bold;
    }
  }

  h2 {
    ${h2Styles};
  }

  h3 {
    ${h3Styles};
  }

  h4 {
    ${h4Styles};
  }

  blockquote {
    position: relative;
    margin-left: -xxl;
    padding-left: 3xl;
    font-style: italic;
    ${th('texts.h4')};

    &::before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 3px;
      height: 100%;
      background-color: primary.500;
    }
  }

  p img,
  .medium-insert-images,
  .medium-insert-embeds,
  .images-swiper-wrapper,
  .video-wrapper,
  pre[class*='language-'],
  pre {
    display: block;
    margin: 6xl -7xl;
    max-width: calc(100% + ${th('space.7xl')} * 2);

    ${media.mediumscreen`
      margin-left: -xxl;
      margin-right: -xxl;
      max-width: calc(100% + ${th('space.xxl')} * 2);
    `};

    ${media.smallscreen`
      margin-left: 0;
      margin-right: 0;
      max-width: 100%;
    `};

    ${media.tablet`
      margin-left: -xxl;
      margin-right: -xxl;
      max-width: calc(100% + ${th('space.xxl')} * 2);
    `};

    ${media.mobile`
      margin: 5xl -lg;
      max-width: calc(100% + (${th('space.lg')} * 2));
    `};
  }

  .video-wrapper {
    position: relative;
    &::before {
      content: '';
      display: block;
      width: 100%;
      padding-top: calc(100% / ${ratio('720p')});
    }
    iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      margin: 0;
    }
  }

  p img + figcaption,
  span figcaption,
  p img + em,
  p img + br + em {
    margin-top: -5xl;
    margin-bottom: 6xl;
    font-size: ${fontSizeEm('sm')};
    font-style: italic;
    color: light.900;
    display: block;
    text-align: center;

    ${media.mobile`
      margin-top: -xxl;
      margin-bottom: 5xl;
    `};
  }

  p img + br {
    display: none;
  }

  span figcaption {
    margin-bottom: lg;
  }

  p img + figcaption,
  p img + em {
    margin-top: -lg;
    margin-bottom: xxl;
  }

  p img + br + em,
  p img + figcaption {
    position: relative;
    top: -${th('space.6xl')};
  }

  .medium-insert-images {
    font-style: italic;
    text-align: center;
    color: light.900;

    img {
      margin-bottom: xxl;

      &:last-child {
        margin-bottom: 0;
      }
    }
  }

  hr {
    display: block;
    margin: xxl 0;
    border: none;
    border-bottom: 1px solid ${color('gray', '150')};
  }

  iframe {
    display: block;
    width: 100%;
    height: 31.45rem;
    margin: 6xl 0;

    ${media.mobile`
      height: 16rem;
      margin: 5xl 0;
    `};
  }

  .align-center {
    text-align: center;
  }

  ${({ colortheme }) => getColorthemeStyles(colortheme)};

  pre,
  pre[class*='language-'] {
    font-size: ${fontSize('sm')};
    padding: lg;

    /* prism-okaidia.css */
    line-height: 1.5;
    background: #272822;
    color: #f8f8f2;
    font-family: Consolas, Monaco, 'Andale Mono', 'Ubuntu Mono', monospace;
    /* prism-okaidia.css */
  }

  p > code {
    font-size: ${fontSize('md')};
  }

  ${formattedTextResponsiveStyles};
  ${formattedTextPrintStyles};
`
