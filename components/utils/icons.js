import { Icons } from '@welcome-ui/icons.font'

const ICONS = {
  video: Icons.Play,
  podcast: Icons.Podcast,
}

export const getIcon = kind => ICONS[kind] || null
