import React from 'react'

import CmsContentsText from './contents/text'
import CmsContentsTitle from './contents/title'
import CmsContentsImage from './contents/image'
import CmsContentsVideo from './contents/video'
import CmsContentsJobs from './contents/jobs'
import CmsContentsCompanyStats from './contents/company_stats'
import CmsContentsTechStack from './contents/tech_stack'
import CmsContentsMap from './contents/map'
import CmsContentsTwitter from './contents/twitter'
import CmsContentsFacebook from './contents/facebook'
import CmsContentsSocialNetworks from './contents/social_networks'
import CmsContentsOrganizationEmbed from './contents/organization_embed'
import CmsContentsOrganizationLink from './contents/organization_link'
import CmsContentsStats from './contents/stats'
import CmsContentsDeprecatedNumber from './contents/CmsContentsDeprecatedNumber'
import CmsContentsNumber from './contents/CmsContentsNumber'

import { ContentWrapper } from './content.styled'

const COMPONENTS = {
  text: CmsContentsText,
  title: CmsContentsTitle,
  image: CmsContentsImage,
  video: CmsContentsVideo,
  'company-stats': CmsContentsCompanyStats,
  jobs: CmsContentsJobs,
  'tech-stack': CmsContentsTechStack,
  map: CmsContentsMap,
  twitter: CmsContentsTwitter,
  facebook: CmsContentsFacebook,
  'social-networks': CmsContentsSocialNetworks,
  number: CmsContentsDeprecatedNumber,
  'number-v2': CmsContentsNumber,
  'organization-embed': CmsContentsOrganizationEmbed,
  'organization-link': CmsContentsOrganizationLink,
  stats: CmsContentsStats,
}

const CmsBlockContentContainer = props => {
  const { content, contentProps, columns } = props

  if (content.get('renderedComponent')) {
    return content.get('renderedComponent')
  }

  const kind = content.get('kind')
  const Component = props.Component || COMPONENTS[kind]

  if (!Component) {
    return <div>{`UNKNOWN BLOCK ${kind}`}</div>
  }

  return (
    <ContentWrapper className={`cms-content ${kind}`}>
      <Component content={content} contentProps={contentProps} columns={columns} />
    </ContentWrapper>
  )
}

export default CmsBlockContentContainer
