import React, { PureComponent } from 'react'

import StyledText from './text.styled'

class Text extends PureComponent {
  render() {
    const { mode, children } = this.props
    return <StyledText mode={mode}>{children}</StyledText>
  }
}

export default Text
