import styled from '@xstyled/styled-components'

import { textStyles } from '../../utils/theme-helpers'

export const StyledText = styled.span`
  ${({ mode }) => textStyles(mode)};
`

export default StyledText
