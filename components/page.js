import React from 'react'
import PropTypes from 'prop-types'
import kebabCase from 'lodash/kebabCase'
import { Icons } from '@welcome-ui/icons.font'

import { Section } from './section'
import { StyledPage } from './page.styled'
import {
  PageSubnav,
  PageSubnavList,
  PageSubnavListItem,
  PageSubnavLink,
  PageSubnavListItemIcon,
} from './pages/nav.styled'

const isActiveClass = page => {
  return page.get('exactMatch') ? 'active' : ''
}

const Page = props => {
  const { page, children, origin } = props

  const pagePath = page.getIn(['path', '0'])
  const pagePathChildren = pagePath?.get('children')?.sortBy(page => page.get('position'))

  return (
    <StyledPage data-testid={origin}>
      {pagePathChildren?.size > 0 && (
        <PageSubnav data-testid={`${origin}-nav`}>
          <PageSubnavList>
            <PageSubnavListItem>
              <PageSubnavLink to={pagePath.get('link')} className="active">
                {pagePath.get('name')}
              </PageSubnavLink>
              <PageSubnavListItemIcon>
                <Icons.Right height={10} />
              </PageSubnavListItemIcon>
            </PageSubnavListItem>
            {pagePathChildren.map((child, key) => (
              <PageSubnavListItem key={key}>
                <PageSubnavLink
                  data-testid={`${origin}-subnav-${kebabCase(child.get('name'))}`}
                  to={child.get('link')}
                  className={isActiveClass(child)}
                >
                  {child.get('name')}
                </PageSubnavLink>
              </PageSubnavListItem>
            ))}
          </PageSubnavList>
        </PageSubnav>
      )}
      {children}
    </StyledPage>
  )
}

Page.propTypes = {
  sectionComponent: PropTypes.func,
}

Page.defaultProps = {
  sectionComponent: Section,
}

export default Page
