import styled, { css, keyframes, th } from '@xstyled/styled-components'
import { color, fontSize, rgba } from '../utils/theme-helpers'

const loadingAnimation = keyframes`
  0% {
    left: -300px;
  }
  50% {
    left: 100%;
  }
  100% {
    left: -300px;
  }
`

const loadingAnimationRule = css`
  ${loadingAnimation} 2s linear infinite forwards;
`

const spinnerAnimation = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`

const spinnerAnimationRule = css`
  ${spinnerAnimation} 1.1s infinite linear;
`

export const loadingStyles = css`
  position: relative;
  overflow: hidden;
  backface-visibility: hidden;
  opacity: ${props => (props.opacity === 'light' ? '.5' : '1')};

  &::after {
    position: absolute;
    top: 0;
    left: -300px;
    width: 300px;
    height: 100%;
    background: linear-gradient(
      to right,
      transparent,
      ${rgba('light', '900', 0.3)} 30%,
      ${rgba('light', '900', 0.3)} 70%,
      transparent
    );
    animation: ${loadingAnimationRule};
    z-index: 10;
    content: ' ';
  }
`

export const loadingDarkStyles = css`
  ${loadingStyles};
  &::after {
    background: linear-gradient(
      to right,
      transparent,
      ${rgba('light', '900', 0.2)} 30%,
      ${rgba('light', '900', 0.2)} 70%,
      transparent
    );
  }
`

function getLoadingTextLineWidth(width) {
  switch (width) {
    case 'full':
      return '100%'
    case 'three_quarters':
      return '75%'
    case 'half':
      return '50%'
    case 'third':
      return 'calc(100%/3)'
    case 'quarter':
      return 'calc(100%/4)'
    case 'fifth':
      return 'calc(100%/5)'
    case 'sixth':
      return 'calc(100%/6)'
    case 'button':
      return '7rem'
    default:
      return '100%'
  }
}

function getNewLoadingTextLineHeight(props) {
  return `
    calc(${th(`lineHeights.${props.fontSize}`)});
  `
}

// TODO: remove old values
function getLoadingTextLineHeight(props) {
  switch (props.h) {
    case 'xs':
      return fontSize('xs')
    case 'sm':
      return fontSize('sm')
    case 'md':
      return fontSize('md')
    case 'lg':
      return fontSize('lg')
    case 'xl':
      return fontSize('xl')
    case 'xxl':
      return fontSize('xxl')
    case 'full':
      return '100%'
    default:
      return fontSize('sm')
  }
}

export const LoadingTextLine = styled.span`
  display: inline-block;
  flex: 0 0 auto;
  width: ${props => getLoadingTextLineWidth(props.w)};
  ${props => (props.mode && props.mode === 'dark' ? backgroundDarkStyles : backgroundLightStyles)};
  height: ${props =>
    props.fontSize
      ? getNewLoadingTextLineHeight(props)
      : css`calc(${props => getLoadingTextLineHeight(props)} * 1.2);`};

  &:last-child {
    margin-bottom: 0;
  }
`

export const LoadingBlock = styled(LoadingTextLine)`
  width: ${props => props.w};
  height: ${props => props.h};
`

const backgroundDarkStyles = css`
  background-color: ${rgba('dark', '900', 0.07)};
  ${loadingDarkStyles};
`

const backgroundLightStyles = css`
  background-color: ${color('gray', 150)};
  ${loadingStyles};
`

export const LoadingImg = styled.div`
  width: 100%;
  height: 100%;
  ${props => (props.mode && props.mode === 'dark' ? backgroundDarkStyles : backgroundLightStyles)};
`

export const LoadingZone = styled.div`
  display: block;
  width: ${props => {
    if (props.widthFactor) {
      return `calc(${props.theme.space[props.w]} * ${props.widthFactor})`
    }
    return props.theme.space[props.w]
  }};
  height: ${props => {
    if (props.heightFactor) {
      return `calc(${props.theme.space[props.h]} * ${props.heightFactor})`
    }
    return props.theme.space[props.h]
  }};
  ${props => (props.mode && props.mode === 'dark' ? backgroundDarkStyles : backgroundLightStyles)};
  &:last-child {
    margin-bottom: 0;
  }
`

export const LoadingTextMultiLines = styled.div`
  margin-bottom: 5xl;

  &:last-child {
    margin-bottom: 0;
  }

  ${LoadingTextLine} {
    margin-bottom: md;

    &:last-child {
      margin-bottom: 0;
    }
  }
`

const loadingSpinnerMdStyles = css`
  width: 1.875rem;
  height: 1.875rem;
  border-width: 3px;
`

const loadingSpinnerLgStyles = css`
  width: 4rem;
  height: 4rem;
  border-width: 6px;
`

const loadingSpinnerBackgroundDarkStyles = css`
  border-color: ${rgba('light', '900', 0.2)};
`

const loadingSpinnerBackgroundLightStyles = css`
  border-color: ${rgba('dark', '900', 0.2)};
`

export const LoadingSpinner = styled.div`
  position: relative;
  border-radius: 50%;
  border-style: solid;
  transform: translateZ(0);
  animation: ${spinnerAnimationRule};
  ${props =>
    props.background === 'light'
      ? loadingSpinnerBackgroundLightStyles
      : loadingSpinnerBackgroundDarkStyles}
  ${props => (props.size === 'lg' ? loadingSpinnerLgStyles : loadingSpinnerMdStyles)}
  border-left-color: ${props =>
    props.color === 'white' ? rgba('light', '900', 0.8)(props) : color('primary')(props)};
`
