import styled, { css, th } from '@xstyled/styled-components'
import { Content, ContentHeader, ContentTitle, ContentContent } from './content.styled'
import {
  ContentHeader as TextContentHeader,
  ContentTitle as TextContentTitle,
  ContentContent as TextContentContent,
} from './contents/text.styled'
import { color, rgba, fontSize, fontWeight } from '../utils/theme-helpers'
import { scrollbar, media, hoverScaleStyles } from './utils/utils.styled'

export const PackeryGridSizer = styled.div`
  width: ${({ columns }) => `${100 / columns}%`};

  ${media.smallscreen`
    width: 50%;
  `};

  ${media.tablet`
    width: 100%;
  `};
`

export const BlockWrapper = styled.div(({ span, containerColumns }) => {
  const width = (Math.min(containerColumns, span) * 100) / containerColumns
  const smallScreenWidth = width <= 50 ? 50 : 100

  return css`
    &.block {
      width: ${width}%;

      ${media.smallscreen`
        width: ${smallScreenWidth}%;
      `}

      ${media.tablet`
        width: 100%;
      `};
    }

    // Packery draggable item
    &.is-dragging,
    &.is-positioning-post-drag {
      z-index: 101;

      .cms-block {
        background-color: white;
      }
    }

    position: relative;
    box-sizing: border-box;
    padding: lg;

    ${media.tablet`
      width: 100%;
    `};
  `
})

export const BlockContainer = styled.div(
  ({ withHover = true }) => css`
    background-color: light.900;
    overflow: hidden;
    margin-bottom: 3xl;
    ${withHover && hoverScaleStyles}

    &:last-child {
      margin-bottom: 0;
    }

    iframe {
      display: block;
      width: 100%;
    }

    &.social-networks,
    &.text,
    &.articles,
    &.jobs-search,
    &.meetings-search,
    &.organization-embed {
      background: none;
      box-shadow: none;
      border-radius: 0;

      &:hover {
        transform: none;
      }
    }

    &.text,
    &.jobs-search,
    &.meetings-search,
    &.articles {
      overflow: visible;
    }

    &.company-stats {
      border: 1px solid ${color('gray', '150')};
    }

    &.stats {
      padding-bottom: xxl;
    }

    &.video-with-text,
    &.image-with-text {
      display: flex;

      & > .cms-content.video,
      & > .cms-content.image {
        flex: 0 0 45%;
      }

      & > .cms-content.text {
        position: relative;
        flex: 1 1 auto;

        &::after {
          content: '';
          position: absolute;
          left: 0;
          bottom: 0;
          right: 6px;
          z-index: 3;
          height: ${th('space.5xl')};
          background: linear-gradient(
            to top,
            ${rgba('light', '900', 1)},
            ${rgba('light', '900', 0)}
          );
        }
        ${Content} {
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 2;
          overflow: auto;
          -webkit-overflow-scrolling: touch;
          padding: 0 xxl xxl;
          ${props => `${scrollbar('6px', color('gray', 300)(props), color('gray', 150)(props))}`};
        }
      }

      ${media.mobile`
      display: block;

      & > .cms-content.text {

        ${Content} {
          position: relative;
          height: auto;
          padding: lg;
        }
      }
    `};
    }

    &.texts-carousel {
      min-height: 11.5625rem;
      background-color: primary.500;
      display: flex;

      ${Content} {
        text-align: center;
        flex: 1;
        display: flex;
        flex-direction: column;
      }

      ${ContentHeader} {
        border: none;
        text-align: center;
        padding-top: xxl;
      }

      ${ContentContent} {
        padding: 0;
        flex: 1;
      }

      ${ContentTitle} {
        text-align: center;
        font-size: ${fontSize('sm')};
        color: ${color('black')};
        text-transform: uppercase;
      }

      ${TextContentHeader} {
        border: none;
        padding-bottom: 0;
        margin-bottom: md;
      }

      ${TextContentTitle} {
        color: ${color('black')};
      }

      ${TextContentContent} {
        p,
        li {
          color: ${color('black')};
        }
      }
    }

    &.title {
      background: transparent;
      box-shadow: none;
      border-bottom: 1px solid;
      border-bottom-color: dark.900;
      padding: lg 0;

      &:hover {
        transform: none;
      }
    }

    &.articles {
      margin: 5xl 0;

      ${media.mobile`
      margin: lg 0;
    `};

      ${ContentHeader} {
        border: none;
        padding: 0;
        margin-bottom: 5xl;

        ${media.mobile`
        margin-bottom: xxl;
      `};
      }

      ${ContentTitle} {
        font-size: ${fontSize('xxl')};
        font-weight: ${fontWeight('bold')};
        text-align: center;

        ${media.mobile`
        font-size: ${fontSize('xl')};
      `};
      }

      ${ContentContent} {
        padding: 0;
      }
    }
  `
)
