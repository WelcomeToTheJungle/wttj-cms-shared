import { modulo } from '../../../../utils/number'

const keyMap = {
  enter: 'enter',
  up: 'up',
  down: 'down',
  esc: 'esc',
}

const useHotKeys = ({
  items,
  indexName,
  onChange,
  selected = -1,
  select,
  inputRef,
  firstIndex = 0,
}) => {
  const wrapKeyPress = fn => {
    const index = items.map(({ [indexName]: indexValue }) => indexValue).indexOf(selected)
    fn(index)
  }

  const handlePressArrow = modifier => e => {
    e.preventDefault()
    wrapKeyPress(index => {
      const i = modulo(index + modifier, items.length)
      const selectedItem = items[i]
      const { [indexName]: indexValue } = selectedItem
      select(indexValue)
    })
  }

  const handlePressUp = handlePressArrow(-1)

  const handlePressDown = handlePressArrow(+1)

  const handlePressEnter = e => {
    e.preventDefault()
    wrapKeyPress(index => {
      const item = items[selected ? index : firstIndex]
      if (item) onChange(item)(e)
      if (inputRef && inputRef.current) inputRef.current.blur()
    })
  }

  const handlePressEsc = e => {
    e.preventDefault()
    if (inputRef && inputRef.current) inputRef.current.blur()
  }

  const keyHandlers = {
    up: handlePressUp,
    down: handlePressDown,
    enter: handlePressEnter,
    esc: handlePressEsc,
  }

  return [keyHandlers, keyMap]
}

export default useHotKeys
