import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class CmsLink extends Component {
  // FIXME add host when process.env.WTTJ_CMS_ENV === 'back'
  path() {
    const { kind, values = {}, to } = this.props
    if (to) {
      return to
    }
    switch (kind) {
      case 'article':
        return `/articles/${values.slug}`
      case 'interview':
        return `/professions/${values.slug}`
      case 'job':
        return `/companies/${values.organizationSlug}/jobs/${values.slug}`
      case 'jobs':
        return `/companies/${values.organizationSlug}/jobs`
      case 'cms_page':
        const slug = values.page.get('slug')
        return `/companies/${values.organization.get('slug')}${slug ? `/${slug}` : ''}`
      default:
        return ''
    }
  }

  render() {
    const { children, ...rest } = this.props
    if (process.env.WTTJ_CMS_ENV === 'back') {
      return (
        <a href={this.path()} className={this.props.className} {...rest}>
          {children}
        </a>
      )
    } else {
      return (
        <Link to={this.path()} className={this.props.className} {...rest}>
          {children}
        </Link>
      )
    }
  }
}

export default CmsLink
