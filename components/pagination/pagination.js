import React, { Component } from 'react'
import { Icons } from '@welcome-ui/icons.font'

import {
  Pagination as StyledPagination,
  PaginationList,
  PaginationItem,
  PaginationLink,
} from './pagination.styled'

export default class Pagination extends Component {
  render() {
    const { pagination, url } = this.props
    if (pagination.get('total_pages') === 1) return null
    return (
      <StyledPagination background="dark">
        <PaginationList>
          <PaginationItem disabled={pagination.get('page_number') === '1'}>
            <PaginationLink to={`${url}`}>
              <Icons.Left />
            </PaginationLink>
          </PaginationItem>
          {[...Array(pagination.get('total_pages'))].map((e, i) => (
            <PaginationItem key={i} active={i + 1 === pagination.get('page_number')}>
              <PaginationLink to={i + 1 === 1 ? `${url}` : `${url}?page=${i + 1}`}>
                {i + 1}
              </PaginationLink>
            </PaginationItem>
          ))}
          <PaginationItem
            disabled={pagination.get('total_pages') === pagination.get('page_number')}
          >
            <PaginationLink to={`${url}?page=${pagination.get('total_pages')}`}>
              <Icons.Right />
            </PaginationLink>
          </PaginationItem>
        </PaginationList>
      </StyledPagination>
    )
  }
}
