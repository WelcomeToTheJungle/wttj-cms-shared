import styled, { css } from '@xstyled/styled-components'
import { Link } from 'react-router-dom'
import { color, fontSize, fontWeight, transition } from '../../utils/theme-helpers'
import { media } from '../utils/utils.styled'

export const paginationListStyles = css`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 5xl;
  padding-top: xxl;
`

export const PaginationList = styled.ul`
  ${paginationListStyles};
`

export const paginationItemActiveStyles = css`
  background-color: primary.500;
  border-color: primary.500;
  color: dark.900;
  pointer-events: none;

  &:hover {
    color: ${color('white')};
  }
`

export const paginationItemDisabledStyles = css`
  display: none;
`

export const paginationItemStyles = css`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${fontSize('xxl')};
  height: ${fontSize('xxl')};
  margin: 0 xxs;
  font-size: ${fontSize('sm')};
  text-align: center;
  transition: ${transition('md')};
  font-weight: ${fontWeight('bold')};

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    margin-right: 0;
  }

  &.ais-Pagination-item--page {
    ${media.mobile`
      display: none;
    `};
  }
`

export const PaginationItem = styled.li`
  ${paginationItemStyles};
  ${props => (props.active ? paginationItemActiveStyles : null)};
  ${props => (props.disabled ? paginationItemDisabledStyles : null)};
`

export const paginationLinkStyles = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  color: inherit;
`

export const PaginationLink = styled(Link)`
  ${paginationLinkStyles};
`

const paginationBackgroundDarkStyles = css`
  ${PaginationList} {
    border-top: 1px solid ${color('gray', '550')};
  }

  ${PaginationItem} {
    color: dark.900;
    border: 1px solid ${color('gray', '400')};

    svg path {
      fill: ${color('texts', 'xlight')};
    }

    &:hover {
      background-color: light.800;
    }
  }
`

export const paginationBackgroundLightListStyles = css`
  border-top: 1px solid ${color('gray', '150')};
`

export const paginationBackgroundLightItemStyles = css`
  color: dark.900;
  border: 1px solid;
  border-color: dark.900;

  svg path {
    fill: ${color('texts', 'light')};
  }

  &:hover {
    background-color: light.800;
  }
`

export const paginationBackgroundLightStyles = css`
  ${PaginationList} {
    ${paginationBackgroundLightListStyles};
  }

  ${PaginationItem} {
    ${paginationBackgroundLightItemStyles};
  }
`

export const paginationStyles = css`
  position: relative;
`

export const Pagination = styled.div`
  ${paginationStyles};
  ${props =>
    props.background === 'dark' ? paginationBackgroundDarkStyles : paginationBackgroundLightStyles};
`
