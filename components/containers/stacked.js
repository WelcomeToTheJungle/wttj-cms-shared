import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Block from '../block'
import { getBlocks } from '../../utils/containers'

class ContainerStacked extends Component {
  render() {
    const { blocks: propBlocks, container, section, blockComponent, onMove } = this.props
    const BlockComponent = blockComponent
    const blocks = getBlocks(propBlocks, container)

    return blocks.map((block, i) => (
      <BlockComponent
        key={i}
        block={block}
        blocks={blocks}
        idx={i}
        container={container}
        section={section}
        onMove={onMove}
      />
    ))
  }
}

ContainerStacked.propTypes = {
  blockComponent: PropTypes.func,
}

ContainerStacked.defaultProps = {
  blockComponent: Block,
}

export default ContainerStacked
