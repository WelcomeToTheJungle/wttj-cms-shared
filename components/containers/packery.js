import React, { Component, Fragment } from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import PropTypes from 'prop-types'
import Packery from 'react-packery'
import debounce from 'lodash/debounce'
import { List } from 'immutable'

import Block from '../block'
import GridLoading from '../layouts/grid_loading'
import StackedLoading from '../layouts/stacked_loading'
import { PackeryGridSizer } from '../block.styled'
import { getNewBlocks } from '../../utils/containers'

const packeryOptions = {
  itemSelector: '.block',
  transitionDuration: '0.25s',
  percentPosition: true,
  columnWidth: '.grid-sizer',
}

class ContainerPackery extends Component {
  state = {
    loaded: false,
  }

  componentDidMount() {
    const { Draggable, blocks } = this.props
    if (!this.packery) {
      return
    }

    const { masonry } = this.packery

    this.preloadImages(blocks).then(images => {
      this.setState({ loaded: true })
    })

    if (Draggable) {
      masonry.items.forEach(this.bindDragEvents)
      masonry.on('dragItemPositioned', this.onMove)
    }
  }

  componentWillUnmount() {
    const { Draggable } = this.props
    if (!this.packery || !Draggable) return

    this.packery.masonry.off('dragItemPositioned', this.onMove)
  }

  componentDidUpdate(prevProps) {
    // Bind Draggabilly events to new blocks
    const { blocks, Draggable } = this.props
    const { blocks: prevBlocks } = prevProps

    if (!this.packery || !Draggable) return

    const { masonry } = this.packery

    const newBlocks = getNewBlocks(prevBlocks, blocks)
    const filtered = this.filterBlocks(newBlocks)

    filtered.forEach(block => {
      if (!block || !block.size) {
        return
      }
      const blockId = block.get('id')
      // If there are new blocks and the don't have id of `0` (which denotes temporary blocks)
      if (blockId !== 0) {
        // Wait for element to be in dom ???
        setTimeout(() => {
          const item = masonry.items.find(item => item.element.dataset.id === String(blockId))
          this.bindDragEvents(item)
        }, 1000)
      }
    })
  }

  preloadImages = blocks => {
    const filtered = this.filterBlocks(blocks)
    const blocksWithImages = [
      'image',
      'image-with-text',
      'image-with-text-v2',
      'video',
      'video-with-text',
    ]
    const blocksToLoad = filtered.filter(block => blocksWithImages.includes(block.get('kind')))

    let imagesToPreload = blocksToLoad.map(block => {
      const contents = block.get('contents')
      const imageContent = contents.find(content => blocksWithImages.includes(content.get('kind')))
      const source = imageContent.getIn(['properties', 'image', 'small', 'url'])

      if (!source) {
        return null
      }

      return new Promise((resolve, reject) => {
        const image = new Image()
        image.onload = () => resolve(`success: ${source}`)
        image.onerror = () => resolve(`error: ${source}`)
        image.src = source
      })
    })

    imagesToPreload = imagesToPreload.filter(e => e !== null)

    return Promise.all(imagesToPreload)
  }

  bindDragEvents = item => {
    const { Draggable } = this.props
    const { masonry } = this.packery
    if (!Draggable || !item) return

    masonry.bindDraggabillyEvents(new Draggable(item.element))
  }

  onMove = () => {
    const { onMove } = this.props
    onMove && setTimeout(onMove, 0, this.packery.masonry.items)
  }

  onContentChange = debounce(() => {
    this.packery && this.packery.masonry.shiftLayout()
  }, 200)

  filterBlocks = blocks => (blocks ? blocks.filter(Boolean) : List())

  render() {
    const {
      blocks,
      blockComponent: BlockComponent = Block,
      children,
      contentProps,
      columns,
      additionalBlock,
    } = this.props
    const filtered = this.filterBlocks(blocks)
    const additionalBlockComponent =
      (additionalBlock && additionalBlock.get('renderedComponent')) || null

    return (
      <Fragment>
        {this.state.loaded && additionalBlockComponent}
        {!this.state.loaded && parseInt(columns, 10) === 1 && <StackedLoading />}
        {!this.state.loaded && parseInt(columns, 10) > 1 && <GridLoading />}
        <Packery
          ref={el => (this.packery = el)}
          className={`packery-container ${this.state.loaded ? 'show' : 'hide'}`}
          options={packeryOptions}
        >
          {children}
          <PackeryGridSizer className="grid-sizer" columns={columns} />
          {filtered.map(block => (
            <BlockComponent
              key={block.get('id')}
              block={block}
              containerColumns={columns}
              contentProps={{
                ...contentProps,
                onChange: this.onContentChange,
              }}
            />
          ))}
        </Packery>
      </Fragment>
    )
  }
}

ContainerPackery.propTypes = {
  blocks: ImmutablePropTypes.list,
  blockComponent: PropTypes.func,
  width: PropTypes.number,
  Draggable: PropTypes.func,
  onMove: PropTypes.func,
}

ContainerPackery.defaultProps = {
  blockComponent: Block,
}

export default ContainerPackery
