import styled from '@xstyled/styled-components'
import { media } from './utils/utils.styled'
import { color, fontSize, fontWeight } from '../utils/theme-helpers'
import { Button } from './buttons/button.styled'

export const Section = styled.section`
  display: flex;
  align-items: flex-start;
  max-width: ${props => props.maxWitdh || null};
  ${media.mobile`
    display: block;
  `};
`

export const SectionSurtitle = styled.h4`
  position: relative;
  font-size: ${fontSize('sm')};
  font-weight: ${fontWeight('regular')};
  color: ${color('gray', '200')};
  text-transform: uppercase;
  margin-bottom: lg;

  ${Button} {
    margin-left: auto;
    margin-right: auto;
  }
`
