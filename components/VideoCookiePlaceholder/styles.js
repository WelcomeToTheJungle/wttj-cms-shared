import styled, { css } from '@xstyled/styled-components'
import { Box } from '@xstyled/styled-components'
import { ModalFullVideoIframe } from '../modals/modals.styled'

export const Wrapper = styled(ModalFullVideoIframe).attrs({
  as: Box,
})(
  ({ $isInline }) => css`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    background: 50% 50% no-repeat;
    background-size: cover;
    text-align: center;
    color: light.900;

    ${$isInline &&
    css`
      height: 100%;
    `}

    &::before {
      content: '';
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background: rgba(0, 0, 0, 0.55);
    }
  `
)

export const Content = styled.box`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
`
