import React from 'react'
import { bool, string } from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Text } from '@welcome-ui/text'
import { Button } from '@welcome-ui/button'

import { showVideoCookies } from '../../contexts/cookies'
import * as S from './styles'

const capitalize = str => {
  // We had a sentry error but we can't reproduce
  // we think the issue is that at some point the data is undefined
  // Since youtube is the main video source, we decided to set it by default
  if (!str) return 'Youtube'

  return str.slice(0, 1).toUpperCase() + str.slice(1)
}

export const VideoCookiePlaceholder = props => {
  const { placeholder, source, isInline } = props

  return (
    <S.Wrapper backgroundImage={`url(${placeholder})`} $isInline={isInline}>
      <S.Content>
        <Text as="div" variant="h4" mb="xs">
          <FormattedMessage id="cms.videoCookiePlaceholder.title" />
        </Text>
        <Text as="div" mb="3xl">
          <FormattedMessage
            id="cms.videoCookiePlaceholder.subtitle"
            values={{
              source: capitalize(source),
            }}
          />
        </Text>
        <Button onClick={showVideoCookies}>
          <FormattedMessage id="cms.videoCookiePlaceholder.button" />
        </Button>
      </S.Content>
    </S.Wrapper>
  )
}

VideoCookiePlaceholder.propTypes = {
  placeholder: string.isRequired,
  source: string.isRequired,
  isInline: bool,
}
