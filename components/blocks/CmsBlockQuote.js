import React from 'react'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'
import { FormattedMessage } from 'react-intl'
import * as S from './CmsContentsQuote.styled'

const CmsContentsQuote = props => {
  const { block } = props

  const contentQuote = block.get('contents').find(c => c.get('kind') === 'quote')

  const backgroundColor = contentQuote.getIn(['properties', 'background']) || 'dark.900'
  const color = backgroundColor === 'dark.900' ? 'light.900' : 'dark.900'

  const name = contentQuote.getIn(['properties', 'name'])
  const subtitle = contentQuote.getIn(['properties', 'subtitle'])
  const author = [name, subtitle].filter(Boolean).join(', ')

  const body = contentQuote.getIn(['properties', 'body'])
  const startQuote = name && <FormattedMessage id="cms.quotes.beginning" />
  const endQuote = name && <FormattedMessage id="cms.quotes.ending" />

  const contentImage = block.get('contents').find(c => c.get('kind') === 'image')
  let avatar
  if (contentImage) {
    let size = 'small'
    if (block.getIn(['properties', 'columns']) >= 3) {
      size = 'large'
    }
    avatar = contentImage.getIn(['properties', 'image', size, 'url'])
  }

  return (
    <Box padding="xl" backgroundColor={backgroundColor} color={color}>
      <Text variant="h4">
        {startQuote}
        {body}
        {endQuote}
      </Text>
      {name && (
        <Box display="flex" alignItems="center" mt="sm">
          {avatar && <S.Avatar src={avatar} alt={avatar} />}
          <Text variant="body3">{author}</Text>
        </Box>
      )}
    </Box>
  )
}

export default CmsContentsQuote
