import React from 'react'
import { Swiper, useSwiper } from '@welcome-ui/swiper'
import { Text } from '@welcome-ui/text'
import { Box } from '@welcome-ui/box'
import ReactMarkdown from 'react-markdown'

const BlockCarousel = props => {
  const {
    block,
    contentProps: { onLinkClick },
  } = props
  const contents = block.get('contents').sortBy(contents => contents.get('position'))
  const swiper = useSwiper({
    autoplay: true,
    loop: true,
    duration: 4000,
    height: '100%',
    renderPaginationItem:
      contents.size <= 1
        ? null
        : ({ idx, onClick, pageIdx }) => {
            return (
              <Swiper.Bullet
                w={7}
                height={7}
                active={idx === pageIdx}
                backgroundColor="dark.900"
                onClick={onClick}
                opacity={idx === pageIdx ? 1 : 0.25}
              />
            )
          },
  })

  return (
    <Box backgroundColor="primary.500" p="3xl" textAlign="center" w="100%">
      <Text variant="subtitle1" textTransform="uppercase">
        {block.getIn(['properties', 'title'])}
      </Text>
      <Swiper {...swiper}>
        {contents
          .map((content, i) => (
            <Swiper.Slide key={i}>
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="space-around"
                height="100%"
                pb="3xl"
              >
                <Text variant="h4" lines={2}>
                  {content.getIn(['properties', 'title'])}
                </Text>
                <Text variant="body2" color="dark.700" lines={2} onClick={onLinkClick}>
                  <ReactMarkdown source={content.getIn(['properties', 'body'])} />
                </Text>
              </Box>
            </Swiper.Slide>
          ))
          .toJS()}
      </Swiper>
    </Box>
  )
}

export default BlockCarousel
