import styled, { css, th } from '@xstyled/styled-components'
import { Link as WuiLink } from '@welcome-ui/link'
import { Swiper as WuiSwiper } from '@welcome-ui/swiper'

import { media } from '../utils/utils.styled'
import { FormattedText } from '../utils/formatted_text.styled'

export const Swiper = styled(WuiSwiper)(
  ({ isImageActive }) => css`
    display: none;

    ${media.mobile`
      position: relative;
      display: flex;

      &::before {
        content: '';
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 1;
        height: 30px;
        background: linear-gradient(transparent, rgba(0, 0, 0, 0.5));
        opacity: ${isImageActive ? 1 : 0};
        transform: ${isImageActive ? 'scale(1)' : 'translateY(30px)'};
        transition: medium;
      }
    `}
  `
)

export const ImageBlock = styled.div`
  flex-shrink: 0;
  width: 33%;

  ${media.mobile`
    width: 100%;
    height: 100%;
  `}
`

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

export const Content = styled.div`
  flex: 1;
  padding: 3xl;
`

export const Body = styled(FormattedText)`
  margin-bottom: 0;

  p,
  li {
    ${th('texts.body2')};
  }
`

// We need to do this because Link doesn't work on multiple lines otherwise
export const Link = styled(WuiLink)`
  display: inline-block;
  margin-top: xl;
`
