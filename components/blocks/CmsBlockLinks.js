import React from 'react'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'
import { Stack } from '@welcome-ui/stack'
import { Link } from '@welcome-ui/link'

const CmsBlockLinks = ({ block, onShowcaseLinkClick }) => {
  const links = block.get('contents').sortBy(link => link.get('position'))

  return (
    <Box display="flex" padding="3xl" border="1px solid" borderColor="light.800">
      <Text variant="h4" w="33%" mr="3xl">
        {block.getIn(['properties', 'title'])}
      </Text>
      <Stack spacing="xl">
        {links.map(link => (
          <Box key={link.get('id')}>
            <Link
              display="inline"
              href={link.getIn(['properties', 'url'])}
              onClick={onShowcaseLinkClick}
              target="_blank"
            >
              <Text variant="h5" display="inline">
                {link.getIn(['properties', 'title'])}
              </Text>
            </Link>
            <Text variant="meta2" color="dark.200" mt="xxs">
              {link.getIn(['properties', 'subtitle'])}
            </Text>
          </Box>
        ))}
      </Stack>
    </Box>
  )
}

export default CmsBlockLinks
