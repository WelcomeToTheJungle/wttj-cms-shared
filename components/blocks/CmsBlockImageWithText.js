import React from 'react'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'
import { Swiper, useSwiper } from '@welcome-ui/swiper'
import ReactMarkdown from 'react-markdown'

import * as S from './CmsBlockImageWithText.styled'

const CmsBlockImageWithText = props => {
  const { block, onShowcaseLinkClick } = props

  const content = block
    .get('contents')
    .find(content => content.get('kind') === 'richtext')
    .get('properties')
  const title = content.get('title')
  const body = content.get('body')
  const linkTitle = content.get('link_title')
  const linkUrl = content.get('link_url')
  const hasLink = linkTitle && linkUrl

  const swiper = useSwiper({
    loop: true,
    height: '100%',
    slidesToShow: 1,
    renderPaginationItem: ({ idx, onClick, pageIdx }) => {
      const isActive = idx === pageIdx
      const isImageActive = (isImageLeft && pageIdx === 0) || (!isImageLeft && pageIdx === 1)

      return (
        <Swiper.Bullet
          w={7}
          height={7}
          active={isActive}
          backgroundColor={isImageActive ? 'light.900' : 'dark.900'}
          onClick={onClick}
          opacity={isActive ? 1 : 0.5}
          transition="medium"
        />
      )
    },
  })
  const imageContent = block.get('contents').find(content => content.get('kind') === 'image')
  const imagePosition = imageContent.get('position') === 1 ? 'left' : 'right'
  const isImageLeft = imagePosition === 'left'
  const isImageActive =
    (isImageLeft && swiper.pageIdx === 0) || (!isImageLeft && swiper.pageIdx === 1)

  const mobileContent = (
    <Box p="xl">
      {title && (
        <Text variant="h4" mb="xl">
          {title}
        </Text>
      )}
      <S.Body>
        <ReactMarkdown source={body} />
      </S.Body>
      {hasLink && (
        <S.Link href={linkUrl} onClick={onShowcaseLinkClick} target="blank">
          {linkTitle}
        </S.Link>
      )}
    </Box>
  )
  const mobileImage = (
    <S.ImageBlock>
      <S.Image src={imageContent.getIn(['properties', 'image', 'large', 'url'])} alt={title} />
    </S.ImageBlock>
  )
  const desktopImage = (
    <S.ImageBlock>
      <S.Image src={imageContent.getIn(['properties', 'image', 'large', 'url'])} alt={title} />
    </S.ImageBlock>
  )

  return (
    <Box border="1px solid" borderColor="light.800">
      <S.Swiper {...swiper} isImageActive={isImageActive}>
        <Swiper.Slide height="auto">
          {isImageLeft && mobileImage}
          {!isImageLeft && mobileContent}
        </Swiper.Slide>
        <Swiper.Slide height="auto">
          {!isImageLeft && mobileImage}
          {isImageLeft && mobileContent}
        </Swiper.Slide>
      </S.Swiper>
      <Box display={{ xs: 'none', md: 'flex' }}>
        {isImageLeft && desktopImage}
        <S.Content>
          {title && (
            <Text variant="h4" mb="xl">
              {title}
            </Text>
          )}
          <S.Body>
            <ReactMarkdown source={body} />
          </S.Body>
          {hasLink && (
            <S.Link href={linkUrl} onClick={onShowcaseLinkClick} target="blank" mt="xl">
              {linkTitle}
            </S.Link>
          )}
        </S.Content>
        {!isImageLeft && desktopImage}
      </Box>
    </Box>
  )
}

export default CmsBlockImageWithText
