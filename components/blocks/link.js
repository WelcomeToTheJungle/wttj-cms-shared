import React, { Component } from 'react'

import { ImageLink, Image as StyledImage } from '../contents/image.styled'
import { Content } from '../content.styled'
import { ContentBlockLink, Header, Surtitle, Title } from '../contents/link.styled'
import { getTitle, getOrganizationName, getAlt, getSubtitle } from '../utils/content'

class CmsBlockLink extends Component {
  alt() {
    return getAlt(this.title(), this.organizationName())
  }

  organizationName() {
    const content = this.contentLink()
    return getOrganizationName(content, content.get('contentProps'))
  }

  title() {
    const content = this.contentLink()
    return getTitle(content)
  }

  subtitle() {
    const content = this.contentLink()
    return getSubtitle(content)
  }

  contentImage() {
    const { block } = this.props
    return block.get('contents').find(c => c.get('kind') === 'image')
  }

  contentLink() {
    const { block } = this.props
    return block.get('contents').find(c => c.get('kind') === 'link')
  }

  thumbUrl() {
    const { block } = this.props

    if (!this.contentImage()) {
      return
    }
    let size = 'small'
    if (block && block.getIn(['properties', 'columns']) >= 3) {
      size = 'large'
    }

    return this.contentImage().getIn(['properties', 'image', size, 'url'])
  }

  render() {
    const { onShowcaseLinkClick } = this.props
    const url = this.contentLink().getIn(['properties', 'url'])
    const target = this.contentLink().getIn(['properties', 'target'])
    const title = this.title()
    const subtitle = this.subtitle()
    const alt = this.alt()
    const thumb = this.thumbUrl()

    if (thumb) {
      return (
        <Content>
          <ImageLink href={url} onClick={onShowcaseLinkClick} target={target} title={alt}>
            <StyledImage src={thumb} alt={alt} />
            <Header>
              {subtitle && <Surtitle>{subtitle}</Surtitle>}
              <Title>{title}</Title>
            </Header>
          </ImageLink>
        </Content>
      )
    }

    return (
      <Content>
        <ContentBlockLink href={url} onClick={onShowcaseLinkClick} target={target} title={alt}>
          {subtitle && <Surtitle>{subtitle}</Surtitle>}
          <Title>{title}</Title>
        </ContentBlockLink>
      </Content>
    )
  }
}

export default CmsBlockLink
