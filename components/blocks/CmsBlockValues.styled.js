import styled from '@xstyled/styled-components'
import { Swiper as WuiSwiper } from '@welcome-ui/swiper'

export const Swiper = styled(WuiSwiper)`
  .swiper-pagination {
    position: static;
    padding: 0;
  }
`

export const Values = styled.div`
  flex: 1;
  padding: 0 sm;
  border-left: 1px solid;
  border-color: rgba(0, 0, 0, 0.1);
  &:first-child {
    border: none;
  }
`
