import styled, { th } from '@xstyled/styled-components'
import { Link as WuiLink } from '@welcome-ui/link'

import { FormattedText } from '../utils/formatted_text.styled'

export const Body = styled(FormattedText)`
  margin-bottom: 0;

  p,
  li {
    ${th('texts.body2')};
  }
`

// We need to do this because Link doesn't work on multiple lines otherwise
export const Link = styled(WuiLink)`
  display: inline-block;
  margin-top: xl;
`
