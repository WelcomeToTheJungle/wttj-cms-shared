import React from 'react'
import { Swiper, useSwiper } from '@welcome-ui/swiper'
import { Text } from '@welcome-ui/text'
import { Box } from '@welcome-ui/box'
import { FormattedMessage } from 'react-intl'

import groupBySize from '../../utils/groupBySize'
import * as S from './CmsBlockValues.styled'

const CmsBlockValues = props => {
  const { block } = props

  const values = block.get('contents').sortBy(contents => contents.get('position'))

  const swiper = useSwiper({
    autoplay: true,
    loop: true,
    height: '100%',
    duration: 4000,
    slidesToShow: 1,
    renderPaginationItem: ({ idx, onClick, pageIdx }) => (
      <Swiper.Bullet
        w={7}
        height={7}
        active={idx === pageIdx}
        backgroundColor="dark.900"
        onClick={onClick}
        opacity={idx === pageIdx ? 1 : 0.5}
      />
    ),
  })

  const groupedValues = groupBySize(values, values.size === 4 ? 2 : 3)

  return (
    <Box backgroundColor="primary.500" p="3xl" textAlign="center">
      <Text variant="subtitle1" textTransform="uppercase">
        <FormattedMessage id="cms.contents.values.title" />
      </Text>
      <Box display={{ xs: 'block', md: 'none' }}>
        <S.Swiper {...swiper}>
          {values
            .map(value => (
              <Swiper.Slide key={value.get('position')}>
                <Box py="3xl">
                  <Text variant="h3" lines={2}>
                    {value.getIn(['properties', 'name'])}
                  </Text>
                </Box>
              </Swiper.Slide>
            ))
            .toJS()}
        </S.Swiper>
      </Box>
      <Box display={{ xs: 'none', md: 'block' }}>
        {groupedValues.map((values, i) => (
          <Box key={i} display="flex" justifyContent="space-between" pt="3xl">
            {values.map(value => (
              <S.Values key={value.get('position')}>
                <Text variant="h3" lines={2} textAlign="center">
                  {value.getIn(['properties', 'name'])}
                </Text>
              </S.Values>
            ))}
          </Box>
        ))}
      </Box>
    </Box>
  )
}

export default CmsBlockValues
