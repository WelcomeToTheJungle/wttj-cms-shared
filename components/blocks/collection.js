import React, { Component } from 'react'

import Content from '../content'
import {
  Content as StyledContent,
  ContentHeader,
  ContentTitle,
  ContentContent,
} from '../content.styled'

class BlockCollection extends Component {
  constructor(props) {
    super(props)
    this.swiper = null
  }

  contents() {
    const { block } = this.props
    return block.get('contents').sortBy(c => c.get('position'))
  }

  render() {
    const { block, contentProps } = this.props
    return (
      <StyledContent>
        <ContentHeader short>
          <ContentTitle>{block.getIn(['properties', 'title'])}</ContentTitle>
        </ContentHeader>
        <ContentContent>
          <ul>
            {this.contents().map((content, i) => (
              <li key={i}>
                <Content content={content} contentProps={contentProps} />
              </li>
            ))}
          </ul>
        </ContentContent>
      </StyledContent>
    )
  }
}

export default BlockCollection
