import styled from '@xstyled/styled-components'

// xstyled doesn't support object-fit prop in v1
export const Avatar = styled.img`
  width: 30;
  height: 30;
  border-radius: 50%;
  margin-right: sm;
  object-fit: cover;
`
