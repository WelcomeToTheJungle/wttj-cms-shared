import React from 'react'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'
import ReactMarkdown from 'react-markdown'

import * as S from './CmsBlockText.styled'

const CmsBlockText = props => {
  const { block, onShowcaseLinkClick } = props

  const content = block.getIn(['contents', 0, 'properties'])
  const title = content.get('title')
  const body = content.get('body')
  const linkTitle = content.get('link_title')
  const linkUrl = content.get('link_url')
  const hasLink = linkTitle && linkUrl

  return (
    <Box p="3xl" backgroundColor="nude.200">
      {title && (
        <Text variant="h4" mb="xl">
          {title}
        </Text>
      )}
      <S.Body>
        <ReactMarkdown source={body} />
      </S.Body>
      {hasLink && (
        <S.Link href={linkUrl} onClick={onShowcaseLinkClick} target="blank">
          {linkTitle}
        </S.Link>
      )}
    </Box>
  )
}

export default CmsBlockText
