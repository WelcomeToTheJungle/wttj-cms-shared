import React from 'react'

import Content from '../content'
import CmsBlockLink from './link'
import CmsBlockQuote from './CmsBlockQuote'
import CmsBlockText from './CmsBlockText'
import CmsBlockImageWithText from './CmsBlockImageWithText'
import CmsBlockLinks from './CmsBlockLinks'
import CmsBlockValues from './CmsBlockValues'

const BlockBasic = props => {
  const { contentProps, block } = props

  const updateBlock = (block, contentProps) =>
    block.set(
      'contents',
      block.get('contents').map(content => content.set('contentProps', contentProps))
    )

  if (!block) {
    return null
  }

  const kind = block.get('kind')
  const contents = block.get('contents').sortBy(c => c.get('position'))
  const columns = block.getIn(['properties', 'columns'])
  const Component = block.getIn(['properties', 'Component'])
  const onShowcaseLinkClick = contentProps?.onShowcaseLinkClick

  const updatedBlock = updateBlock(block, contentProps)

  if (kind === 'link') {
    return <CmsBlockLink block={updatedBlock} onShowcaseLinkClick={onShowcaseLinkClick} />
  }
  if (kind === 'quote') {
    return <CmsBlockQuote block={updatedBlock} />
  }
  if (kind === 'text-v2') {
    return <CmsBlockText block={updatedBlock} onShowcaseLinkClick={onShowcaseLinkClick} />
  }
  if (kind === 'image-with-text-v2') {
    return <CmsBlockImageWithText block={updatedBlock} onShowcaseLinkClick={onShowcaseLinkClick} />
  }
  if (kind === 'links') {
    return <CmsBlockLinks block={updatedBlock} onShowcaseLinkClick={onShowcaseLinkClick} />
  }
  if (kind === 'values') {
    return <CmsBlockValues block={updatedBlock} />
  }

  return contents.map((content, i) => (
    <Content
      key={i}
      content={content}
      contentProps={contentProps}
      columns={columns}
      Component={Component}
    />
  ))
}

export default BlockBasic
