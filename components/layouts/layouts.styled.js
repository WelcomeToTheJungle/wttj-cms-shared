import styled, { css, th } from '@xstyled/styled-components'
import { oneOf } from 'prop-types'

import { media } from '../utils/utils.styled'

const getFlexStyles = (align, justify) => css`
  display: flex;
  align-items: ${align || 'center'};
  justify-content: ${justify || 'flex-start'};

  // TODO: Check if these are accesible given we don't have props…
  ${media.tablet`
    flex-direction: ${({ $tabletDirection }) => $tabletDirection || null};
    align-items: ${({ $tabletAlign }) => $tabletAlign || null};
  `};

  ${media.mobile`
    flex-direction: ${({ $mobileDirection }) => $mobileDirection || null};
    align-items: ${({ $mobileAlign }) => $mobileAlign || null};
  `};
`

export const CenteredContainer = styled.div(
  ({ size, margin, padding, align, justify, position = 'initial' }) => css`
    min-width: 0;
    width: 100%;
    max-width: ${th(`centeredContainerWidth.${size || 'lg'}`)};
    margin: ${margin ?? '0 auto'};
    padding: ${padding ?? '0 xl'};
    position: ${position};
    ${(align || justify) && getFlexStyles(align, justify)};

    ${media.tablet`
      display: block;
      padding: ${padding ?? '0 lg'};
    `};

    ${media.mobile`
      padding: ${padding ?? '0 md'};
    `};
  `
)

export const RowContainer = styled.div`
  flex-direction: row;
  flex: ${({ flex }) => (flex !== undefined ? flex : null)};
  ${({ align, justify }) => getFlexStyles(align, justify)};
`

export const StackContainer = styled.div`
  flex-direction: column;
  ${({ align, justify }) => getFlexStyles(align, justify)};
`

CenteredContainer.propTypes = {
  align: oneOf(['center', 'flex-start', 'flex-end']),
  justify: oneOf(['center', 'flex-start', 'flex-end', 'space-between', 'space-around']),
}
