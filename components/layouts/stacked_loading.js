import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'

import { Stacked } from './stacked_loading.styled'
import { Content } from '../content.styled'
import { ContentHeader, ContentTitle, ContentContent } from '../contents/text.styled'
import {
  Content as SNContent,
  ContentHeader as SNContentHeader,
  ContentTitle as SNContentTitle,
  ContentContent as SNContentContent,
  List,
  Item,
} from '../contents/social_networks.styled'
import { BlockWrapper } from '../block.styled'
import { LoadingZone, LoadingTextLine, LoadingTextMultiLines } from '../loading.styled'

class StackedLoading extends Component {
  render() {
    return (
      <Stacked>
        <BlockWrapper>
          <SNContent>
            <SNContentHeader>
              <SNContentTitle>
                <FormattedMessage id="cms.contents.social-networks.title" />
              </SNContentTitle>
            </SNContentHeader>
            <SNContentContent>
              <List>
                <Item>
                  <LoadingZone w="xxl" height="xxl" />
                </Item>
                <Item>
                  <LoadingZone w="xxl" height="xxl" />
                </Item>
                <Item>
                  <LoadingZone w="xxl" height="xxl" />
                </Item>
                <Item>
                  <LoadingZone w="xxl" height="xxl" />
                </Item>
              </List>
            </SNContentContent>
          </SNContent>
        </BlockWrapper>
        <BlockWrapper>
          <Content>
            <ContentHeader>
              <ContentTitle>
                <LoadingTextLine w="full" height="xl" />
              </ContentTitle>
            </ContentHeader>
            <ContentContent>
              <LoadingTextMultiLines>
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="three-quarters" height="sm" />
              </LoadingTextMultiLines>
            </ContentContent>
          </Content>
        </BlockWrapper>
        <BlockWrapper>
          <Content>
            <ContentHeader>
              <ContentTitle>
                <LoadingTextLine w="full" height="xl" />
              </ContentTitle>
            </ContentHeader>
            <ContentContent>
              <LoadingTextMultiLines>
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="three-quarters" height="sm" />
              </LoadingTextMultiLines>
            </ContentContent>
          </Content>
        </BlockWrapper>
        <BlockWrapper>
          <Content>
            <ContentHeader>
              <ContentTitle>
                <LoadingTextLine w="full" height="xl" />
              </ContentTitle>
            </ContentHeader>
            <ContentContent>
              <LoadingTextMultiLines>
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="full" height="sm" />
                <LoadingTextLine w="three-quarters" height="sm" />
              </LoadingTextMultiLines>
            </ContentContent>
          </Content>
        </BlockWrapper>
      </Stacked>
    )
  }
}

export default StackedLoading
