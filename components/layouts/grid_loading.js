import React, { Component } from 'react'
import { LoadingZone } from '../loading.styled'
import { BlockWrapper, BlockContainer } from '../block.styled'
import { Grid } from './grid_loading.styled'

class GridLoading extends Component {
  render() {
    return (
      <Grid>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="6.125" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="2.9" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="4.78" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="2.9" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="3.84" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="2.9" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="2.9" />
          </BlockContainer>
        </BlockWrapper>
        <BlockWrapper>
          <BlockContainer>
            <LoadingZone height="6xl" heightFactor="2.9" />
          </BlockContainer>
        </BlockWrapper>
      </Grid>
    )
  }
}

export default GridLoading
