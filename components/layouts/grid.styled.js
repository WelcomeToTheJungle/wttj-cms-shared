import styled, { css, th } from '@xstyled/styled-components'
import { media } from '../utils/utils.styled'

export const Column = styled.div``

const gutterSmResponsiveStyles = css`
  ${media.mobile`
    margin-left: 0;
    margin-right: 0;
    margin-bottom: -md;

    & > div, & > li {
      padding: 0;
      padding-bottom: md;

      &:last-child{
        margin-bottom: 0;
      }
    }
  `};
`

const gutterSmStyles = css`
  margin-left: -md;
  margin-right: -md;
  margin-bottom: calc(${th('space.md')} * -2);

  & > div,
  & > li {
    padding: md;
    padding-top: 0;
    padding-bottom: calc(${th('space.md')} * 2);
  }

  ${gutterSmResponsiveStyles};
`

const gutterMdResponsiveStyles = css`
  ${media.mobile`
    margin-left: 0;
    margin-right: 0;
    margin-bottom: -lg;

    & > div, & > li {
      padding: 0;
      padding-bottom: lg;

      &:last-child{
        margin-bottom: 0;
      }
    }
  `};
`

const gutterMdStyles = css`
  margin-left: -lg;
  margin-right: -lg;
  margin-bottom: calc(${th('space.lg')} * -2);

  & > div,
  & > li {
    padding: lg;
    padding-top: 0;
    padding-bottom: calc(${th('space.lg')} * 2);
  }

  ${gutterMdResponsiveStyles};
`

const gutterLgResponsiveStyles = css`
  ${media.tablet`
    margin-left: -lg;
    margin-right: -lg;

    & > div, & > li {
      padding: lg;
      padding-top: 0;
      padding-bottom: calc(${th('space.lg')} * 2);
    }
  `};

  ${media.mobile`
    margin-left: ${props => (props.columnsmobile ? '-lg' : '0')};
    margin-right: ${props => (props.columnsmobile ? '-lg' : '0')};
    margin-bottom: -xxl;

    & > div, & > li {
      padding: ${props => (props.columnsmobile ? 'lg' : '0')};
      padding-top: 0;
      padding-bottom: xxl;

      &:last-child{
        margin-bottom: 0;
      }
    }
  `};
`

const gutterLgStyles = css`
  margin-left: -xxl;
  margin-right: -xxl;
  margin-bottom: calc(${th('space.xxl')} * -2);

  & > div,
  & > li {
    padding: xxl;
    padding-top: 0;
    padding-bottom: calc(${th('space.xxl')} * 2);
  }

  ${gutterLgResponsiveStyles};
`

const gutterXsResponsiveStyles = css`
  ${media.mobile`
    margin-left: 0;
    margin-right: 0;
    margin-bottom: -xs;

    & > div, & > li {
      padding: 0;
      padding-bottom: xs;

      &:last-child{
        margin-bottom: 0;
      }
    }
  `};
`

const gutterXsStyles = css`
  margin-left: -xs;
  margin-right: -xs;
  margin-bottom: calc(${th('space.xs')} * -2);

  & > div,
  & > li {
    padding: xs;
    padding-top: 0;
    padding-bottom: calc(${th('space.xs')} * 2);
  }

  ${gutterXsResponsiveStyles};
`

function getGutter(gutter) {
  switch (gutter) {
    case 'xs':
      return gutterXsStyles
    case 'sm':
      return gutterSmStyles
    case 'md':
      return gutterMdStyles
    case 'lg':
      return gutterLgStyles
    default:
      return null
  }
}

function getVertical(position) {
  switch (position) {
    case 'bottom':
      return 'flex-end'
    case 'center':
      return 'center'
    case 'stretch':
      return 'stretch'
    default:
      return 'flex-start'
  }
}

function getHorizontal(position) {
  switch (position) {
    case 'right':
      return 'flex-end'
    case 'left':
      return 'flex-start'
    default:
      return 'center'
  }
}

const gridResponsiveStyles = css`
  ${media.smallscreen`
    & > div, & > li {
      width: ${props =>
        props.columnssmallscreen ? `calc(100%/${props.columnssmallscreen})` : '100%'};

      &:nth-child(2n+1), &:nth-child(2n+2) {
        width: ${props =>
          props.columnssmallscreen ? `calc(100%/${props.columnssmallscreen})` : '100%'};
      }
    }
  `};

  ${media.mobile`
    & > div, & > li {
      width: ${props => (props.columnsmobile ? `calc(100%/${props.columnsmobile})` : '100%')};

      &:nth-child(2n+1), &:nth-child(2n+2) {
        width: ${props => (props.columnsmobile ? `calc(100%/${props.columnsmobile})` : '100%')};
      }
    }
  `};
`

export const gridStyles = css`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  justify-content: ${props => getHorizontal(props.horizontal)};
  align-items: ${props => getVertical(props.vertical)};
  ${props => getGutter(props.gutter)} & > div, & > li {
    width: calc(100% / ${props => props.columns});
    flex: 0 0 auto;

    &:nth-child(2n + 2) {
      width: ${props =>
        props.mode && props.mode === 'sidebar-left'
          ? (100 / props.columns) * 2 + '%'
          : 100 / props.columns + '%'};
    }

    &:nth-child(2n + 1) {
      width: ${props =>
        props.mode && props.mode === 'sidebar-right'
          ? (100 / props.columns) * 2 + '%'
          : 100 / props.columns + '%'};
    }
  }

  ${gridResponsiveStyles};
`

export const Grid = styled.div`
  ${props => gridStyles};
`
