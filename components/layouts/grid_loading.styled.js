import styled, { th } from '@xstyled/styled-components'
import { BlockWrapper } from '../block.styled'
import { media } from '../utils/utils.styled'
import { LoadingZone } from '../loading.styled'

export const Grid = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  & > ${BlockWrapper} {
    width: 33.33%;
    &:first-child {
      width: 66.66%;
    }
    &:nth-child(5) {
      margin-top: calc(-${th('space.6xl')} * 3.225);
    }
    &:nth-child(7) {
      margin-top: calc(-${th('space.6xl')} * 1.88);
    }
    &:nth-child(8) {
      margin-top: calc(-${th('space.6xl')} * 4.165);
    }
  }
  & + .xmasonry {
    height: 0;
  }
  ${media.mobile`
    display: block;
    & > ${BlockWrapper} {
      width: 100%;
      &:first-child {
        width: 100%;
      }
      &:nth-child(5),
      &:nth-child(7),
      &:nth-child(8) {
        margin-top: 0;
      }
    }
    ${LoadingZone} {
      height: 14.4rem;
    }
  `};
`
