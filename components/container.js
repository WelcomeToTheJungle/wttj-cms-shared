import React, { Component } from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import PropTypes from 'prop-types'

import StyledContainer from './container.styled'

class Container extends Component {
  render() {
    const { container, w, children } = this.props

    if (!container) {
      return 'EMPTY CONTAINER'
    }

    return (
      <StyledContainer
        className="cms-container"
        kind={container.get('kind')}
        columns={container.getIn(['properties', 'columns'])}
        w={w}
      >
        {children}
      </StyledContainer>
    )
  }
}

Container.propTypes = {
  container: ImmutablePropTypes.map,
  width: PropTypes.string,
  children: PropTypes.node,
}

export default Container
