import styled, { css, keyframes, th } from '@xstyled/styled-components'
import { oneOf, bool } from 'prop-types'
import { Button } from '@welcome-ui/button'
import { CenteredContainer } from '../layouts/layouts.styled'
import { media, getBackgroundImage } from '../utils/utils.styled'
import { rgba, radius, fontSize, fontWeight, modalSize } from '../../utils/theme-helpers'

const slideInDown = keyframes`
  from {
    transform: translateY(-100%);
  }
  to {
    transform: translateY(0);
  }
`

const slideInDownRule = css`
  ${slideInDown} 0.5s both;
`

const slideInUp = keyframes`
  from {
    transform: translateY(100%);
  }
  to {
    transform: translateY(0);
  }
`

const slideInUpRule = css`
  ${slideInUp} 0.5s both;
`

export const ModalHeader = styled.header`
  position: relative;
  padding: 3xl 50 0 50;
  background-color: light.900;
  flex: 0 0 auto;

  ${media.mobile`
    padding: ${th('buttons.sizes.md.height')} ${th('buttons.sizes.md.height')} 0;
  `};
`

export const ModalTitle = styled.h2`
  text-align: center;
  ${th('texts.h3')};
  color: dark.900;
`

export const ModalSubtitle = styled.h4`
  text-align: center;
  margin-top: lg;
  ${th('texts.body1')};
  font-weight: medium;
  color: light.500;
`

export const ModalContent = styled.div`
  flex: 1 1 auto;
  padding: 0;
  min-height: auto;
  background-color: light.900;
  ${th('texts.body1')};

  ${props =>
    !props.fillMode &&
    css`
      padding: xxl 5xl 0 5xl;
      &::after {
        content: '';
        height: ${th('space.xxl')};
        display: block;
      }

      ${media.mobile`
        padding: xxl xxl 0 xxl;
      `};
    `};

  &:last-child {
    border-radius: 0 0 ${radius('md')} ${radius('md')};
  }

  ${props =>
    props.$hasSidebar &&
    css`
      height: 70vh;
      padding: 0;
      margin-left: ${modalSize('sidebar')};
      background-color: nude.100;

      &::after {
        display: none;
      }

      ${media.mobile`
        // Adding specificity because of issues caused by react-prerendered-component
        && {
          flex-shrink: 1;
          padding: 0;
          margin-left: 0;
          height: auto;
        }
      `}
    `}
`

ModalContent.propTypes = {
  fillMode: bool,
}

export const ModalFooter = styled.footer`
  flex: 0 0 auto;
  padding: 20;
  font-size: ${th('texts.body2')};
  background-color: nude.200;
  text-align: center;
  color: dark.700;

  span,
  a {
    margin-left: xs;

    &:first-child {
      margin-left: 0;
    }
  }

  ${media.mobile`
    padding: 3xl;
  `};
`

export const ModalCloseButton = styled(Button).attrs(props => ({
  variant: props.variant,
  shape: 'circle',
}))`
  position: absolute;
  top: ${th('space.xl')};
  right: ${th('space.xl')};
  width: ${th('buttons.sizes.sm.height')};
  height: ${th('buttons.sizes.sm.height')};
  overflow: hidden;
  z-index: 10;

  ${media.mobile`
    top: ${th('space.sm')};
    right: ${th('space.sm')};
    width: ${th('space.xl')};
    height: ${th('space.xl')};
  `};
`

export const ModalConfirmationMessage = styled.p`
  text-align: center;
  margin-top: lg;
  font-size: ${fontSize('md')};
  font-weight: regular;
  color: light.500;
`

const modalXlStyles = css`
  width: ${modalSize('xl')};
  animation: ${slideInDownRule};
`

const modalLgStyles = css`
  width: ${modalSize('lg')};
  animation: ${slideInDownRule};
`

const modalMdStyles = css`
  width: ${modalSize('md')};
  animation: ${slideInDownRule};
`

const modalXsStyles = css`
  width: ${modalSize('xs')};
  animation: ${slideInDownRule};
`

const modalSmStyles = css`
  width: ${modalSize('sm')};
  animation: ${slideInDownRule};
`

const modalFullStyles = css`
  width: 100%;
  height: 100%;
  border-radius: 0;
  display: flex;
  flex-direction: column-reverse;
  animation: ${slideInUpRule};

  ${ModalHeader} {
    background-color: dark.500;
    color: light.900;
    border-radius: 0;
    flex: 0 0 auto;
  }

  ${ModalContent} {
    min-height: 0;
    background: none;
    border-radius: 0;
    flex: 1 1 auto;
    display: flex;
    flex-direction: column;
    padding: xxl 0;
    ${props =>
      props.size === 'full' &&
      css`
        padding: 0;
        &::after {
          display: none;
        }
      `};
  }

  ${ModalCloseButton} {
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${th('buttons.sizes.lg.height')};
    height: ${th('buttons.sizes.lg.height')};
  }

  ${media.mobile`
    ${ModalContent} {
      padding: 0;
      &::after {
        display: none;
      }
    }
  `};
`

const modalAutoStyles = css`
  width: auto;
  animation: ${slideInDownRule};
`

const modalDarkBackgroundStyles = css`
  background-color: dark.700;
`

const modalDefaultBackgroundStyles = css`
  background-color: light.900;
`

function getModalStyles(size) {
  switch (size) {
    case 'xs':
      return modalXsStyles
    case 'sm':
      return modalSmStyles
    case 'md':
      return modalMdStyles
    case 'lg':
      return modalLgStyles
    case 'xl':
      return modalXlStyles
    case 'full':
      return modalFullStyles
    case 'auto':
      return modalAutoStyles
    default:
      return modalXsStyles
  }
}

function getModalBackgroundStyles(background) {
  switch (background) {
    case 'dark':
      return modalDarkBackgroundStyles
    default:
      return modalDefaultBackgroundStyles
  }
}

function getModalScrollStyles(scroll) {
  if (scroll === 'content') {
    return css`
      ${ModalContent} {
        min-height: 0;
        overflow: auto;
      }
    `
  }
  return null
}

/** @component */
export const Modal = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  max-width: 100%;
  border-radius: ${radius('md')};

  ${({ size }) => getModalStyles(size)};
  ${({ background }) => getModalBackgroundStyles(background)};

  ${media.mobile`
    border-radius: 0;
    min-height: 100%;

    // Workaround issues caused by react-prerendered-component because it wraps elements like the header in a div while we're using flex
    > * {
      flex-shrink: 0;
    }
  `};
`

Modal.propTypes = {
  size: oneOf(['xs', 'sm', 'md', 'lg', 'full', 'auto']),
  mode: oneOf(['fullscreen']),
  background: oneOf(['dark']),
}

export const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: ${({ align }) => align || 'center'};
  justify-content: ${({ justify }) => justify || 'safe center'};
  overflow: auto;
  -webkit-overflow-scrolling: touch;
  z-index: 9999;
  padding: 0;
  background: ${rgba('dark', '900', 0.3)};
  ${({ scroll }) => getModalScrollStyles(scroll)};
`

export const ModalFullHeader = styled.header`
  background-color: dark.500;
  color: light.900;
  border-radius: 0;
  padding: xxl 0;

  ${CenteredContainer} {
    display: flex;
    align-items: center;
  }

  ${media.mobile`
    padding: lg 0;

    ${CenteredContainer} {
      display: block;
    }
  `};
`

export const ModalFullHeaderSurtitle = styled.header`
  margin-bottom: sm;
  ${th('texts.meta1')};
  color: light.700;

  ${media.mobile`
    text-align: center;
  `};
`

export const ModalFullHeaderTitle = styled.h1`
  ${th('texts.h4')};

  ${media.mobile`
    text-align: center;
  `};
`

export const ModalFullHeaderInfos = styled.div``

export const ModalFullHeaderSubtitle = styled.header`
  font-size: ${fontSize('md')};
  font-weight: ${fontWeight('regular')};
  color: light.500;
  margin-bottom: md;
  margin-top: md;

  ${media.mobile`
    text-align: center;
  `};
`

export const ModalFullContent = styled.div`
  width: 100%;
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  ${CenteredContainer} {
    width: 100%;
    height: 100%;
    flex: 1 1 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`

export const ModalFullVideoIframe = styled.iframe`
  width: 100%;
  flex: 0 0 auto;
  max-width: calc(65vh * 1.77778);
  height: 65vh;
`

export const ModalFullContentPreview = styled.div`
  width: 100%;
  flex: 1 1 auto;
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  ${props => getBackgroundImage(props.cover)};
  img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`

export const ModalFullHeaderActions = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;

  ${media.mobile`
    flex-direction: column;
    margin-top: lg;
  `};
`

export const ModalFullHeaderActionsLabel = styled.p`
  font-size: ${fontSize('sm')};
  text-transform: uppercase;
  font-weight: bold;
  color: light.500;
  margin-right: xxl;

  ${media.mobile`
    text-align: center;
    margin-right: 0;
    margin-bottom: lg;
  `};
`

export const ModalContentSection = styled.section`
  margin-bottom: xxl;

  &:last-child {
    margin-bottom: 0;
  }
`

export const ModalContentSectionTitle = styled.h3`
  position: relative;
  margin-bottom: lg;
  color: dark.200;
  font-weight: bold;
  font-size: ${fontSize('lg')};
  padding-bottom: md;
  border-bottom: 1px solid;
  border-bottom-color: light.800;
`
