import React, { Component } from 'react'
import { bool, func, node, oneOf, string } from 'prop-types'
import { mapContains } from 'react-immutable-proptypes'
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock'
import { Icons } from '@welcome-ui/icons.font'

import * as S from './modals.styled'

class Modal extends Component {
  static propTypes = {
    modal: mapContains({
      header: node,
      content: node.isRequired,
      footer: node,
      title: string,
      subtitle: string,
      size: oneOf(['sm', 'md', 'lg', 'xl', 'full']),
      background: oneOf(['dark']),
      mode: oneOf(['fullscreen']),
      /* Whether to add padding or not ({ fill: true} has no padding) */
      fill: bool,
    }),
    onClose: func,
  }
  closeFromOverlay = e => {
    if (e.target.classList.contains('modal-overlay')) {
      this.props.onClose(e)
    }
  }

  closeIconSize() {
    const { modal } = this.props
    return modal.get('size') === 'full' ? 'lg' : 'xs'
  }

  componentDidMount() {
    const { modal } = this.props
    this.scrollTarget = modal.get('scroll') === 'content' ? this.modalContent : this.modalOverlay
    disableBodyScroll(this.scrollTarget)
  }

  componentWillUnmount() {
    enableBodyScroll(this.scrollTarget)
  }

  render() {
    const { modal, onClose } = this.props
    const header = modal.get('header')

    return (
      <S.ModalOverlay
        justify={modal.get('justify')}
        align={modal.get('align')}
        mode={modal.get('mode')}
        onClick={this.closeFromOverlay}
        className="modal-overlay"
        scroll={modal.get('scroll')}
        ref={e => (this.modalOverlay = e)}
      >
        <S.Modal size={modal.get('size')} background={modal.get('background')}>
          <S.ModalCloseButton
            onClick={onClose}
            variant={modal.get('background') === 'dark' ? 'tertiary-negative' : 'tertiary'}
          >
            <Icons.Cross size={this.closeIconSize()} />
          </S.ModalCloseButton>
          {header && header}
          {!header && modal.get('title') && (
            <S.ModalHeader>
              <S.ModalTitle>{modal.get('title')}</S.ModalTitle>
              {modal.get('subtitle') && <S.ModalSubtitle>{modal.get('subtitle')}</S.ModalSubtitle>}
            </S.ModalHeader>
          )}
          <S.ModalContent
            fillMode={modal.get('fill') ? true : false}
            ref={e => (this.modalContent = e)}
          >
            {modal.get('content')}
          </S.ModalContent>
          {modal.get('footer') && <S.ModalFooter>{modal.get('footer')}</S.ModalFooter>}
        </S.Modal>
      </S.ModalOverlay>
    )
  }
}

export default Modal
