import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { FormattedText } from './utils/formatted_text.styled'

// Prevent multiple whitespace on listitems
// 4 whitespaces are converted to code block
const trimListItems = content => content.replace(/(\*|-) [\s\uFEFF\xA0]+/g, '* ')

// Trim trailing whitespace or tabs
const trimLines = content => content.replace(/[ \t]+$/gm, '')

const cleanText = content => {
  let c = content.normalize()
  c = trimListItems(c)
  c = trimLines(c)
  return c
}

export default class FormattedMarkdown extends Component {
  render() {
    const { content, colortheme, className, onLinkClick } = this.props
    if (!content) {
      return null
    }

    return (
      <FormattedText
        colortheme={colortheme}
        className={className}
        dangerouslySetInnerHTML={{ __html: cleanText(content) }}
        onClick={onLinkClick}
      />
    )
  }
}

FormattedMarkdown.propTypes = {
  content: PropTypes.node,
  onLinkClick: PropTypes.func,
}
