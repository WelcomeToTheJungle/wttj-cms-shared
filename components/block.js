import React, { Component } from 'react'

import BlockBasic from './blocks/basic'
import BlockCarousel from './blocks/carousel'
import BlockCollection from './blocks/collection'
import { BlockWrapper, BlockContainer } from './block.styled'

const BLOCKS_WITH_HOVER = ['twitter', 'facebook', 'social-networks']

class Block extends Component {
  getColumnSpan = () => {
    const { block, containerColumns } = this.props
    // Get column span but default to full width (number of columns in container)
    let blockColumns = block.getIn(['properties', 'columns'])
    if (!blockColumns || isNaN(blockColumns)) {
      blockColumns = 3
    }
    return Math.min(blockColumns, containerColumns)
  }

  getWrapperClassName = columns => {
    return `block block-span-${columns}`
  }

  getBlockClassName = () => {
    const { block } = this.props
    return `cms-block ${block.get('kind')}`
  }

  getHasTransformOnHover = () => {
    const { block } = this.props

    return BLOCKS_WITH_HOVER.includes(block.get('kind'))
  }

  renderBlock = () => {
    const { block, contentProps = {} } = this.props

    switch (block.get('kind')) {
      case 'texts-carousel':
        return <BlockCarousel block={block} contentProps={contentProps} />
      case 'stats':
        return <BlockCollection block={block} contentProps={contentProps} />
      default:
        return <BlockBasic block={block} contentProps={contentProps} />
    }
  }

  render() {
    const { block, styles, children, containerColumns } = this.props
    const span = this.getColumnSpan()
    const wrapperClassName = this.getWrapperClassName(span)
    const blockClassName = this.getBlockClassName()
    const hasTransformOnHover = this.getHasTransformOnHover()

    return (
      <BlockWrapper
        data-position={block.get('position')}
        data-id={block.get('id')}
        span={span}
        containerColumns={containerColumns}
        style={styles}
        className={wrapperClassName}
      >
        {children}
        <BlockContainer
          data-testid={`organization-content-block-${block.get('kind')}`}
          className={blockClassName}
          withHover={hasTransformOnHover}
        >
          {this.renderBlock()}
        </BlockContainer>
      </BlockWrapper>
    )
  }
}

export default Block
