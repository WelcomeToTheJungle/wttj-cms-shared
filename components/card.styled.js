import styled, { css, th } from '@xstyled/styled-components'
import CmsLink from './link'

import { getBackgroundImage, hoverCardStyles, backgroundCover, media } from './utils/utils.styled'

import { color, transition, radius, imagesHeight } from './../utils/theme-helpers'

export const CardCoverWrapper = styled.div`
  position: relative;
  overflow: hidden;
  background-color: ${color('gray', '150')};
  flex: 0 0 auto;
`

export const CardCover = styled.div`
  width: 100%;
  height: 100%;
  transition: ${transition('md')};
  ${props => getBackgroundImage(props.cover)};
  ${backgroundCover};
`

export const CardLink = styled(CmsLink)`
  flex: 1 1 auto;
  position: relative;
  display: flex;
  flex-direction: column;
`

export const CardInner = styled.div`
  position: relative;
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  padding: 0 xxl xxl;
`

export const CardHeader = styled.header`
  flex: 0 0 auto;
`

export const CardContent = styled.div`
  flex: 1 1 auto;
`

export const CardFooter = styled.footer`
  flex: 0 0 auto;
`

export const CardLogo = styled.div`
  display: block;
  flex: 0 0 auto;
  background-color: light.900;
  width: ${imagesHeight('jobs', 'thumb', 'logo')};
  height: ${imagesHeight('jobs', 'thumb', 'logo')};
  border-radius: ${radius('sm')};
  box-shadow: none;
  border-radius: ${radius('sm')};
  overflow: hidden;
  margin-right: xxl;

  ${media.mobile`
    margin-right: 0;
    margin-bottom: lg;
  `};
`

export const CardLogoImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
`

export const CardTitle = styled.h3`
  ${th('texts.h4')};
`

export const CardSurtitle = styled.h4`
  ${th('texts.meta1')};
  color: dark.200;
`

export const CardDescription = styled.p`
  ${th('texts.body2')};
`

const cardLightStyles = css`
  border-color: light.800;
  background-color: light.900;

  ${CardInner} {
    background-color: light.900;
  }

  ${CardTitle} {
    color: dark.900;
  }

  ${CardDescription} {
    color: dark.200;
  }
`

const cardDarkStyles = css`
  border-color: dark.800;
  background-color: dark.500;

  ${CardInner} {
    background-color: dark.500;
  }

  ${CardTitle} {
    color: light.900;
  }

  ${CardDescription} {
    color: light.700;
  }

  ${CardCoverWrapper} {
    background-color: dark.200;
  }
`

export const Card = styled.article`
  position: relative;
  display: flex;
  user-select: none;
  overflow: hidden;
  backface-visibility: hidden;
  transition: medium;
  border: 1px solid;
  ${props => (props.mode === 'dark' ? cardDarkStyles : cardLightStyles)};

  ${hoverCardStyles};

  &:last-child {
    margin-bottom: 0;
  }

  &:hover ${CardCover}::after {
    opacity: 0;
  }
`
