import React, { Component } from 'react'

import { NumberContent } from './number.styled'

import { ContentHeader, ContentTitle, ContentFooter, ContentFooterP } from '../content.styled'

class CmsContentsDeprecatedNumber extends Component {
  render() {
    const { content } = this.props
    return (
      <NumberContent>
        <ContentHeader>
          <ContentTitle>{content.getIn(['properties', 'value'])}</ContentTitle>
        </ContentHeader>
        <ContentFooter>
          <ContentFooterP>{content.getIn(['properties', 'label'])}</ContentFooterP>
        </ContentFooter>
      </NumberContent>
    )
  }
}

export default CmsContentsDeprecatedNumber
