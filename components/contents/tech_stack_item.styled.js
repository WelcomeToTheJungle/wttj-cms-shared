import styled, { th } from '@xstyled/styled-components'
import { withTooltipStyles } from '../tooltips/tooltip.styled'

export const StackCategoryListItem = styled.li`
  margin-bottom: lg;

  &:last-child {
    margin-bottom: 0;
  }
`

export const Picture = styled.div`
  flex: 0 0 auto;
  width: 20px;
  height: 20px;
  margin-right: lg;
  overflow: hidden;
`

export const PictureImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: contain;
`

export const Header = styled.header`
  flex: 1 1 auto;
`

export const Title = styled.h5`
  ${th('texts.body3')}
  ${withTooltipStyles};
`

export const Score = styled.div`
  position: relative;
  height: 3px;
  background-color: dark.100;
  margin-top: xs;
`

export const ScoreBar = styled.div`
  height: 100%;
  background-color: primary.500;
  width: ${props => props.score}%;
`

export const ScoreValue = styled.span`
  position: absolute;
  right: 0;
  bottom: calc(100% + ${th('space.xs')});
  ${th('texts.meta2')};
  color: primary.500;
  opacity: 0;
  transition: medium;
`

export const Tool = styled.article`
  display: flex;

  &:hover {
    ${ScoreValue} {
      opacity: 1;
    }
  }
`
