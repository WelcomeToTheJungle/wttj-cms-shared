import styled, { th } from '@xstyled/styled-components'
import {
  Content,
  ContentHeader,
  ContentTitle,
  ContentFooter,
  ContentFooterP,
} from '../content.styled'

export const NumberContent = styled(Content)`
  padding-top: 3xl;
  padding-bottom: 3xl;
  background-color: primary.500;

  ${ContentHeader}, ${ContentFooter}, ${ContentFooterP} {
    border: none;
    text-align: center;
  }

  ${ContentTitle} {
    ${th('texts.h2')};
    color: dark.900;
  }

  ${ContentFooter} {
    padding-top: 0;
  }

  ${ContentFooterP} {
    ${th('texts.body2')};
    color: dark.900;
  }
`
