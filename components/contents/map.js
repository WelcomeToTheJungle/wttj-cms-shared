import React, { Component } from 'react'

import * as S from './map.styled'
import { getContentSize } from '../utils/content'

class CmsContentsMap extends Component {
  mapUrl() {
    const { columns, content } = this.props
    const size = getContentSize(columns)
    const mapUrl = content.getIn(['properties', 'image_map_url', size, 'url'])

    const coordinates = this.coordinates()
    const googleMapsApiKey = content.getIn(['properties', 'google_maps_api_key'])
    const defaultMapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${coordinates}&size=300x300&sensor=true&scale=2&key=${googleMapsApiKey}&zoom=15&markers=color:0x00c69e%7C${coordinates}`

    return mapUrl || defaultMapUrl
  }

  googleMapsUrl() {
    return `http://maps.google.com/?q=${this.fullAddress(', ')}`
  }

  fullAddress(separator) {
    const { content } = this.props
    return [
      content.getIn(['properties', 'headquarter', 'address']),
      content.getIn(['properties', 'headquarter', 'zip_code']),
      content.getIn(['properties', 'headquarter', 'city']),
    ].join(separator)
  }

  coordinates() {
    const { content } = this.props
    return `${content.getIn(['properties', 'headquarter', 'latitude'])},${content.getIn([
      'properties',
      'headquarter',
      'longitude',
    ])}`
  }

  render() {
    const { content } = this.props
    return (
      <S.Wrapper>
        <S.MapLink href={this.googleMapsUrl()} target="_blank">
          <S.MapWrapper cover={this.mapUrl()} />
          <S.MapAddress>
            <S.MapAddressLine>
              {content.getIn(['properties', 'headquarter', 'address'])}
            </S.MapAddressLine>
            <S.MapAddressCity>
              <span>{content.getIn(['properties', 'headquarter', 'zip_code'])}</span>
              <span>{content.getIn(['properties', 'headquarter', 'city'])}</span>
            </S.MapAddressCity>
          </S.MapAddress>
        </S.MapLink>
      </S.Wrapper>
    )
  }
}

export default CmsContentsMap
