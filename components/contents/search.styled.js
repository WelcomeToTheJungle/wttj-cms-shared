import styled, { th } from '@xstyled/styled-components'
import { fontSize } from '../../utils/theme-helpers'
import { media } from '../utils/utils.styled'
import { Dropdown, DropdownHeader, DropdownTitle } from '../search/dropdown.styled'
import { Search, Dropdowns } from '../search/search.styled'

export const ContentSearch = styled(Search)`
  margin-bottom: xxl;

  &:last-child {
    margin-bottom: 0;
  }

  ${Dropdowns} {
    ${media.tablet`
      display: none;
    `};
  }
`

export const Results = styled.div`
  margin-top: xxl;
`

export const ResultsHeader = styled.header`
  position: relative;
  display: flex;
  justify-content: center;
  padding-bottom: xl;
  margin-bottom: 3xl;
  border-bottom: 1px solid;
  border-bottom-color: light.800;

  ${Dropdown} {
    &::before {
      display: none;
    }
  }

  ${DropdownHeader} {
    padding: 0 0 lg 0;
    border: none;
  }

  ${DropdownTitle} {
    text-transform: none;
    font-size: ${fontSize('md')};
    letter-spacing: 0;
  }

  ${media.tablet`
    flex-direction: column;
    align-items: center;
  `};
`

export const ResultsHeaderTitle = styled.h3`
  ${th('texts.h5')};
`

export const ResultsHeaderReset = styled.div`
  position: absolute;
  top: -${th('space.xs')};
  left: 0;

  ${media.tablet`
    position: static;
    margin-bottom: md;
  `};
`

export const ResultsHeaderFilters = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
`

export const ResultsContent = styled.div``
