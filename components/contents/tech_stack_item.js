import React from 'react'

import * as S from './tech_stack_item.styled'
import { Tooltip } from '../tooltips/tooltip.styled'

export const StackCategoryListItem = ({ tool, position }) => {
  if (!tool) {
    return null
  }
  const name = tool.get('name')
  const comment = tool.get('comment')
  const percent = tool.get('percent')
  const thumb = tool.getIn(['image', 'thumb', 'url'])

  return (
    <S.StackCategoryListItem>
      <S.Tool>
        <S.Picture>
          <S.PictureImg alt={name} className="thumb" src={thumb} />
        </S.Picture>
        <S.Header>
          <S.Title>
            {name}
            {comment && (
              <Tooltip position={position === 0 ? 'left-bottom' : 'left'}>{comment}</Tooltip>
            )}
          </S.Title>
          <S.Score>
            <S.ScoreValue>{percent}%</S.ScoreValue>
            <S.ScoreBar score={percent} />
          </S.Score>
        </S.Header>
      </S.Tool>
    </S.StackCategoryListItem>
  )
}
