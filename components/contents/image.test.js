import React from 'react'
import { fromJS } from 'immutable'
import { spy } from 'sinon'

import { createRenderer, shallow, mount } from '../../test/support'

import CmsContentsImage, {
  CmsContentsImageModalContent,
  CmsContentsImageModalHeader,
} from './image'
import { ImageLink } from './image.styled'
import { Image as StyledImage } from './image.styled'
import {
  ModalFullHeaderSurtitle,
  ModalFullHeaderTitle,
  ModalFullHeaderSubtitle,
  ModalFullContent,
  ModalFullContentPreview,
} from '../modals/modals.styled'

const content = fromJS({
  kind: 'image',
  position: 1,
  properties: {
    title: 'Star Wars',
    surtitle: 'Return of the Jedi',
    organization: {
      name: 'WTTJ',
    },
    image: {
      url: 'http://img.com/image.png',
      thumb: {
        url: 'http://img.com/image-thumb.png',
      },
      small: {
        url: 'http://img.com/image-small.png',
      },
      medium: {
        url: 'http://img.com/image-medium.png',
      },
      large: {
        url: 'http://img.com/image-large.png',
      },
    },
  },
})

test('CmsContentsImage [snapshot] should render proper DOM', () => {
  const component = createRenderer(<CmsContentsImage content={content} />)

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsImage [shallow] should render ImageLink', () => {
  const wrapper = shallow(<CmsContentsImage content={content} />)

  const imageLink = wrapper.find(ImageLink)
  expect(imageLink).to.have.length(1)
  expect(imageLink).to.have.prop('href', '#play-image')
})

test('CmsContentsImage [shallow] should render small Image if block has 1 columns', () => {
  const wrapper = shallow(<CmsContentsImage content={content} columns={1} block={fromJS({})} />)

  expect(wrapper).to.contain(
    <StyledImage src="http://img.com/image-small.png" alt="Star Wars - WTTJ" />
  )
})

test('CmsContentsImage [shallow] should render medium Image if block has 2 columns', () => {
  const wrapper = shallow(<CmsContentsImage content={content} columns={2} block={fromJS({})} />)

  expect(wrapper).to.contain(
    <StyledImage src="http://img.com/image-medium.png" alt="Star Wars - WTTJ" />
  )
})

test('CmsContentsImage [shallow] should render large Image if block has >= 3 columns', () => {
  const wrapper = shallow(<CmsContentsImage content={content} columns={3} block={fromJS({})} />)

  expect(wrapper).to.contain(
    <StyledImage src="http://img.com/image-large.png" alt="Star Wars - WTTJ" />
  )
})

test('CmsContentsImage [full dom] should call onOpenModal with modal params as argument when click on ImageLink', () => {
  const onOpenModal = spy()
  const wrapper = mount(<CmsContentsImage content={content} contentProps={{ onOpenModal }} />)

  const link = wrapper.find(ImageLink)
  expect(link).to.have.length(1)
  link.simulate('click')
  expect(onOpenModal.called).to.equal(true)
  const arg = onOpenModal.getCall(0).args[0]
  expect(arg.size).to.equal('full')
  expect(Object.keys(arg)).to.include('header')
  expect(Object.keys(arg)).to.include('content')
  expect(arg.header.type).to.equal(CmsContentsImageModalHeader)
  expect(arg.content.type).to.equal(CmsContentsImageModalContent)
})

test('CmsContentsImageModalHeader [shallow] should render title, subtitle and organization name', () => {
  const wrapper = shallow(<CmsContentsImageModalHeader content={content} />)

  expect(wrapper).to.contain(
    <ModalFullHeaderTitle>{content.getIn(['properties', 'title'])}</ModalFullHeaderTitle>
  )
  expect(wrapper).to.contain(
    <ModalFullHeaderSubtitle>{content.getIn(['properties', 'surtitle'])}</ModalFullHeaderSubtitle>
  )
  expect(wrapper).to.contain(
    <ModalFullHeaderSurtitle>
      {content.getIn(['properties', 'organization', 'name'])}
    </ModalFullHeaderSurtitle>
  )
})

test('CmsContentsImageModalContent [shallow] should render image', () => {
  const wrapper = shallow(<CmsContentsImageModalContent content={content} />)

  expect(wrapper).to.contain(
    <ModalFullContent>
      <ModalFullContentPreview cover={content.getIn(['properties', 'image', 'large', 'url'])} />
    </ModalFullContent>
  )
})
