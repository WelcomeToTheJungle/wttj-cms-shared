import React from 'react'
import { fromJS } from 'immutable'
import { FormattedMessage } from 'react-intl'
import { Button } from '@welcome-ui/button'

import { shallow } from '../../test/support'

import CmsContentsJobs, { CmsContentsJobsItem } from './jobs'
import { Job } from './jobs.styled'

const content = fromJS({
  kind: 'jobs',
  position: 1,
  properties: {
    jobs: [
      {
        slug: 'wttj-full-stack-developer',
        name: 'Full-Stack Developer',
        website_organization: {
          slug: 'wttj',
        },
        contract_type: 'CDI',
        city: 'Paris',
      },
      {
        slug: 'wttj-full-stack-developer-remote',
        name: 'Full-Stack Developer Remote',
        website_organization: {
          slug: 'wttj',
        },
        contract_type: 'CDI',
        city: 'Nantes',
      },
      {
        slug: 'wttj-full-stack-developer-remote-2',
        name: 'Full-Stack Developer Remote',
        website_organization: {
          slug: 'wttj',
        },
        contract_type: 'CDI',
        city: 'Rennes',
      },
    ],
    website_organization: {
      slug: 'wttj',
    },
  },
})

const contentWithoutJobs = content.setIn(['properties', 'jobs'], fromJS([]))

test('CmsContentsJobs [shallow] should render title', () => {
  const wrapper = shallow(<CmsContentsJobs content={content} contentProps={{ lang: 'en' }} />)

  expect(wrapper).to.contain(<FormattedMessage id="cms.contents.jobs.title" />)
})

test('CmsContentsJobs [shallow] should render jobs list and link to all jobs', () => {
  const wrapper = shallow(
    <CmsContentsJobs content={content} contentProps={{ lang: 'en' }} intl={{ locale: 'en' }} />
  )

  expect(wrapper.find(Button)).to.have.prop(
    'to',
    `/companies/${content.getIn(['properties', 'website_organization', 'slug'])}/jobs`
  )
})

test('CmsContentsJobs [shallow] without jobs should render specific message', () => {
  const wrapper = shallow(
    <CmsContentsJobs content={contentWithoutJobs} contentProps={{ lang: 'en' }} />
  )

  expect(wrapper).to.contain(<FormattedMessage id="cms.contents.jobs.no-jobs" />)
  expect(wrapper.find(Button)).to.have.length(0)
})

test('CmsContentsJobsItem [shallow] should render job', () => {
  const job = content.getIn(['properties', 'jobs', 0])
  const wrapper = shallow(<CmsContentsJobsItem job={job} lang="en" />)

  const link = wrapper.find(Job)
  expect(link).to.have.length(1)
  expect(link).to.have.prop('kind', 'job')
  expect(link).to.have.prop(
    'to',
    `/companies/${job.getIn(['website_organization', 'slug'])}/jobs/${job.get('slug')}`
  )
  expect(wrapper).to.contain(job.get('name'))
  expect(wrapper).to.contain(job.get('contract_type'))
  expect(wrapper).to.contain(job.get('city'))
})
