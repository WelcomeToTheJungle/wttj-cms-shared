import React, { Component, PureComponent } from 'react'
import { Icons } from '@welcome-ui/icons.font'

import {
  Image as StyledImage,
  Header,
  Title,
  Subtitle,
  ImageLink,
  ImageLinkIcon,
} from './image.styled'
import {
  ModalFullHeader,
  ModalFullHeaderInfos,
  ModalFullHeaderSurtitle,
  ModalFullHeaderTitle,
  ModalFullHeaderSubtitle,
  ModalFullContent,
  ModalFullContentPreview,
} from '../modals/modals.styled'
import { CenteredContainer } from '../layouts/layouts.styled'
import { Content } from '../content.styled'
import {
  getContentSize,
  getTitle,
  getSubtitle,
  getOrganizationName,
  getAlt,
} from '../utils/content'

export const CmsContentsImageModalContent = ({ content }) => (
  <ModalFullContent>
    <ModalFullContentPreview cover={content.getIn(['properties', 'image', 'large', 'url'])} />
  </ModalFullContent>
)

export class CmsContentsImageModalHeader extends PureComponent {
  alt() {
    return getAlt(this.title(), this.organizationName())
  }

  organizationName() {
    const { content, contentProps } = this.props
    return getOrganizationName(content, contentProps)
  }

  title() {
    const { content } = this.props
    return getTitle(content)
  }

  subtitle() {
    const { content } = this.props
    return getSubtitle(content)
  }

  render() {
    const subtitle = this.subtitle()
    const title = this.title()
    const organizationName = this.organizationName()
    if (!subtitle && !title && !organizationName) return null
    return (
      <ModalFullHeader>
        <CenteredContainer>
          <ModalFullHeaderInfos>
            {organizationName && (
              <ModalFullHeaderSurtitle>{organizationName}</ModalFullHeaderSurtitle>
            )}
            {title && <ModalFullHeaderTitle>{title}</ModalFullHeaderTitle>}
            {subtitle && <ModalFullHeaderSubtitle>{subtitle}</ModalFullHeaderSubtitle>}
          </ModalFullHeaderInfos>
        </CenteredContainer>
      </ModalFullHeader>
    )
  }
}

class CmsContentsImage extends Component {
  alt() {
    return getAlt(this.title(), this.organizationName())
  }

  organizationName() {
    const { content, contentProps } = this.props
    return getOrganizationName(content, contentProps)
  }

  title() {
    const { content } = this.props
    return getTitle(content)
  }

  subtitle() {
    const { content } = this.props
    return getSubtitle(content)
  }

  thumbUrl() {
    const { columns, content } = this.props
    const size = getContentSize(columns)
    return content.getIn(['properties', 'image', size, 'url'])
  }

  open = e => {
    const {
      contentProps: { onOpenModal },
    } = this.props

    e.preventDefault()

    onOpenModal && onOpenModal(this.modalParams())
  }

  modalParams() {
    const { content } = this.props
    return {
      background: 'dark',
      size: 'full',
      kind: 'image',
      organizationReference: content.getIn(['properties', 'organization', 'reference']),
      imageReference: content.getIn(['properties', 'image', 'url']),
      header: <CmsContentsImageModalHeader content={content} />,
      content: <CmsContentsImageModalContent content={content} />,
    }
  }

  render() {
    const title = this.title()
    const subtitle = this.subtitle()
    return (
      <Content>
        <ImageLink href="#play-image" onClick={this.open}>
          <StyledImage src={this.thumbUrl()} alt={this.alt()} />
          <ImageLinkIcon>
            <Icons.Search size="lg" />
          </ImageLinkIcon>
          {title && (
            <Header>
              <Title>{title}</Title>
              {subtitle && <Subtitle>{subtitle}</Subtitle>}
            </Header>
          )}
        </ImageLink>
      </Content>
    )
  }
}

export default CmsContentsImage
