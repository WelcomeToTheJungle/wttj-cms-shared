import React from 'react'
import { fromJS } from 'immutable'

import { createRenderer, shallow, mount } from '../../test/support'

import ReactMarkdown from 'react-markdown'
import CmsContentsFacebook from './facebook'
import { ContentTitle } from '../content.styled'
import { FacebookLink, FacebookListItem } from './facebook.styled'

const content = fromJS({
  kind: 'facebook',
  position: 1,
  properties: {
    username: 'WTTJ',
    messages: ['Hello World'],
    url: 'http://facebook.com/WTTJ',
  },
})

const contentProps = { onSocialLinkClick: console.debug }

test('CmsContentsFacebook [snapshot] should render proper DOM', () => {
  const component = createRenderer(
    <CmsContentsFacebook content={content} contentProps={contentProps} />
  )

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsFacebook [shallow] should render screen name and messages', () => {
  const wrapper = shallow(<CmsContentsFacebook content={content} contentProps={contentProps} />)

  expect(wrapper).to.contain(<ContentTitle>@WTTJ</ContentTitle>)
  expect(wrapper).to.contain(
    <FacebookListItem key={0}>
      <ReactMarkdown source={content.getIn(['properties', 'messages']).get(0)} />
    </FacebookListItem>
  )
  expect(wrapper.find('ReactMarkdown').html()).contains('Hello World')
})

test('CmsContentsFacebook [full dom] should decode html characters', () => {
  let c = content.setIn(['properties', 'messages', 0], '&commat;Hello &lt; World &gt;')
  const wrapper = mount(<CmsContentsFacebook content={c} contentProps={contentProps} />)
  expect(wrapper.find('ReactMarkdown').first().text()).to.equal('@Hello < World >')
})

test('CmsContentsFacebook [full dom] should render link that is not an <a>', () => {
  const wrapper = mount(<CmsContentsFacebook content={content} contentProps={contentProps} />)
  const onFacebookLink = jest.spyOn(wrapper.instance(), 'onFacebookLink')

  // https://github.com/airbnb/enzyme/issues/365
  wrapper.update()
  wrapper.instance().forceUpdate()

  const link = wrapper.find(FacebookLink)
  expect(link.type().target).not.to.equal('a')
  expect(link.type().target).not.to.equal('button')
  expect(link).to.have.length(1)
  expect(wrapper.instance().url()).to.equal('http://facebook.com/WTTJ')
  expect(onFacebookLink).toHaveBeenCalledTimes(0)
  expect(global.open).toHaveBeenCalledTimes(0)

  link.first().simulate('click')
  expect(onFacebookLink).toHaveBeenCalledTimes(1)
  expect(global.open).toBeCalledWith('http://facebook.com/WTTJ', 'blank')

  onFacebookLink.mockRestore()
  global.open.mockRestore()
})

test('CmsContentsFacebook [full dom] containing a link should be clickable', () => {
  let c = content.setIn(['properties', 'messages', 0], 'Hello World [link](http://google.fr/)')
  const wrapper = mount(<CmsContentsFacebook content={c} contentProps={contentProps} />)
  const onFacebookLink = jest.spyOn(wrapper.instance(), 'onFacebookLink')

  // https://github.com/airbnb/enzyme/issues/365
  wrapper.update()
  wrapper.instance().forceUpdate()

  const externalLink = wrapper.find('a')
  expect(externalLink).to.have.length(1)
  expect(externalLink.prop('href')).to.equal('http://google.fr/')
  expect(externalLink.type()).to.equal('a')
  expect(onFacebookLink).toHaveBeenCalledTimes(0)
  expect(global.open).toHaveBeenCalledTimes(0)

  externalLink.simulate('click')
  expect(onFacebookLink).toHaveBeenCalledTimes(1)
  expect(global.open).toBeCalledWith('http://google.fr/', 'blank')

  onFacebookLink.mockRestore()
  global.open.mockRestore()
})
