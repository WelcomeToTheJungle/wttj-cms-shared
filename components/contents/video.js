import React, { useEffect, useMemo, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Icons } from '@welcome-ui/icons.font'
import { Box } from '@welcome-ui/box'

import { useHasCookie } from '../../contexts/cookies'
import { Content } from '../content.styled'
import { Image, VideoLink, Infos, Header, Title, Subtitle, Button } from './video.styled'
import {
  ModalFullHeader,
  ModalFullHeaderInfos,
  ModalFullHeaderSurtitle,
  ModalFullHeaderTitle,
  ModalFullHeaderSubtitle,
  ModalFullContent,
  ModalFullVideoIframe,
} from '../modals/modals.styled'
import { CenteredContainer } from '../layouts/layouts.styled'
import {
  getContentSize,
  getTitle,
  getSubtitle,
  getOrganizationName,
  getAlt,
} from '../utils/content'
import { VideoCookiePlaceholder } from '../VideoCookiePlaceholder'

const getEmbedUrl = content => {
  const ref = content.getIn(['properties', 'external_reference'])

  switch (content.getIn(['properties', 'source'])) {
    case 'youtube':
      return `https://www.youtube.com/embed/${ref}?rel=0&autoplay=1&showinfo=0`
    case 'dailymotion':
      return `https://www.dailymotion.com/embed/video/${ref}?autoplay=1&showinfo=0`
    case 'vimeo':
      return `https://player.vimeo.com/video/${ref}?badge=0&amp;autopause=0`
    default:
      return null
  }
}

const Modal = props => {
  const { columns, content, contentProps } = props

  const videoSource = content.getIn(['properties', 'source'])
  const hasVideoCookie = useHasCookie(videoSource)

  const title = getTitle(content)
  const subtitle = getSubtitle(content)
  const organizationName = getOrganizationName(content, contentProps)
  const embedUrl = getEmbedUrl(content)

  const modalHeader = useMemo(
    () =>
      title && (
        <ModalFullHeader>
          <CenteredContainer>
            <ModalFullHeaderInfos>
              {organizationName && (
                <ModalFullHeaderSurtitle>{organizationName}</ModalFullHeaderSurtitle>
              )}
              <ModalFullHeaderTitle>{title}</ModalFullHeaderTitle>
              {subtitle && <ModalFullHeaderSubtitle>{subtitle}</ModalFullHeaderSubtitle>}
            </ModalFullHeaderInfos>
          </CenteredContainer>
        </ModalFullHeader>
      ),
    [organizationName, title, subtitle]
  )

  const modalContent = useMemo(
    () => (
      <ModalFullContent>
        {!hasVideoCookie && (
          <VideoCookiePlaceholder
            placeholder={content.getIn(['properties', 'image', 'large', 'url'])}
            source={videoSource}
          />
        )}
        {hasVideoCookie && embedUrl && (
          <ModalFullVideoIframe
            title={title}
            src={embedUrl}
            frameBorder="0"
            allowFullScreen
            allow="autoplay; fullscreen"
          />
        )}
      </ModalFullContent>
    ),
    [hasVideoCookie, title, embedUrl]
  )
  const organizationReference = content.getIn(['properties', 'organization', 'reference'])
  const videoReference = content.getIn(['properties', 'external_reference'])

  const [isModalOpen, setIsModalOpen] = useState(false)
  useEffect(() => {
    if (isModalOpen) {
      contentProps.onOpenModal({
        header: modalHeader,
        content: modalContent,
        background: 'dark',
        size: 'full',
        kind: 'video',
        organizationReference,
        videoReference,
        onClose: () => setIsModalOpen(false),
      })
    }
  }, [isModalOpen, modalHeader, modalContent, organizationReference, videoReference])

  const handleClick = e => {
    e.preventDefault()
    setIsModalOpen(true)
  }

  return (
    <VideoPlaceholder
      columns={columns}
      content={content}
      contentProps={contentProps}
      onClick={handleClick}
    />
  )
}

const Inline = props => {
  const { content, contentProps, columns } = props

  const videoSource = content.getIn(['properties', 'source'])
  const hasVideoCookie = useHasCookie(videoSource)
  const [status, setStatus] = useState(hasVideoCookie ? 'placeholder' : 'cookie-request')
  useEffect(() => {
    setStatus(hasVideoCookie ? 'placeholder' : 'cookie-request')
  }, [hasVideoCookie])

  const title = getTitle(content)
  const embedUrl = getEmbedUrl(content)
  const thumbUrl = content.getIn(['properties', 'image', 'large', 'url'])
  const handleClick = () => {
    setStatus('video')
  }

  return (
    <Box pt="177.78%">
      <Box position="absolute" top="0" left="0" w="100%" h="100%">
        {status === 'cookie-request' && (
          <VideoCookiePlaceholder isInline placeholder={thumbUrl} source={videoSource} />
        )}
        {status === 'placeholder' && (
          <VideoPlaceholder
            columns={columns}
            content={content}
            contentProps={contentProps}
            onClick={handleClick}
          />
        )}
        {status === 'video' && (
          <Box
            as="iframe"
            height="100%"
            title={title}
            src={embedUrl}
            frameBorder="0"
            allowFullScreen
            allow="autoplay; fullscreen"
          />
        )}
      </Box>
    </Box>
  )
}

const VideoPlaceholder = props => {
  const { content, contentProps, columns, onClick } = props

  const title = getTitle(content)
  const subtitle = getSubtitle(content)
  const organizationName = getOrganizationName(content, contentProps)
  const size = getContentSize(columns)
  const thumbUrl = content.getIn(['properties', 'image', size, 'url'])

  return (
    <VideoLink onClick={onClick}>
      <Header>
        <Button data-testid="organization-block-video-button-play">
          <Icons.Play size="sm" color="black" />
        </Button>
        <Infos>
          {title && <Title>{title}</Title>}
          {subtitle && <Subtitle>{subtitle}</Subtitle>}
        </Infos>
      </Header>
      {!thumbUrl && content.get('id') ? (
        <FormattedMessage id="cms.contents.video.image-missing" />
      ) : (
        <Image src={thumbUrl} alt={getAlt(title, organizationName)} />
      )}
    </VideoLink>
  )
}

const CmsContentsVideo = props => {
  const { content } = props
  if (content.getIn(['properties', 'display']) === 'modal') {
    return (
      <Content>
        <Modal {...props} />
      </Content>
    )
  }

  return (
    <Content>
      <Inline {...props} />
    </Content>
  )
}

export default CmsContentsVideo
