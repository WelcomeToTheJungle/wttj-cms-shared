import React from 'react'
import { fromJS } from 'immutable'
import { FormattedMessage } from 'react-intl'

import { createRenderer, shallow } from '../../test/support'

import CmsContentsSocialNetworks from './social_networks'

const content = fromJS({
  kind: 'social-networks',
  position: 1,
  properties: {
    networks: {
      youtube: 'https://www.youtube.com/welcomejobs',
      twitter: 'https://twitter.com/wttj',
      pinterest: 'https://pinterest.com/wttj',
      linkedin: 'https://www.linkedin.com/company/care-er-',
      instagram: 'https://instagram.com/welcome-to-the-jungle',
      facebook: 'https://www.facebook.com/wttj',
    },
  },
})

const contentProps = {
  organization: fromJS({
    name: 'My Orga',
  }),
}

test('CmsContentsSocialNetworks [snapshot] should render proper DOM', () => {
  const component = createRenderer(
    <CmsContentsSocialNetworks content={content} contentProps={contentProps} />
  )

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsSocialNetworks [shallow] should render title', () => {
  const wrapper = shallow(
    <CmsContentsSocialNetworks content={content} contentProps={contentProps} />
  )

  expect(wrapper).to.contain(<FormattedMessage id="cms.contents.social-networks.title" />)
})
