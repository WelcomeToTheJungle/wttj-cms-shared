import React, { Component } from 'react'

import { safeGetPath } from '../../utils/i18n'

import { Button } from '@welcome-ui/button'
import * as S from './title.styled'

class CmsContentsTitle extends Component {
  render() {
    const {
      content,
      contentProps: { getPath, lang },
    } = this.props
    const title = content.getIn(['properties', 'title'])
    const pageSlug = content.getIn(['properties', 'link_page', 'slug'])
    const organizationSlug = content.getIn(['properties', 'link_page', 'website_organization_slug'])
    const linkLabel = content.getIn(['properties', 'link_label'])
    const linkTarget = content.getIn(['properties', 'link_target'])

    return (
      <S.ContainerBox>
        <S.TitleBox>
          <S.Title>{title}</S.Title>
        </S.TitleBox>
        {pageSlug && (
          <S.ButtonBox>
            <Button
              as="a"
              href={safeGetPath(getPath, `/companies/${organizationSlug}${pageSlug}`, lang)}
              target={linkTarget}
              variant="secondary"
            >
              {linkLabel}
            </Button>
          </S.ButtonBox>
        )}
      </S.ContainerBox>
    )
  }
}

export default CmsContentsTitle
