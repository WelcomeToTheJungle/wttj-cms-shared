import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Swiper, useSwiper } from '@welcome-ui/swiper'
import { Icons } from '@welcome-ui/icons.font'

import { Content } from '../content.styled'
import { StackCategoryListItem } from './tech_stack_item'
import * as S from './tech_stack.styled'

const sortTools = tool =>
  [Number(tool.get('percent')).toString(10).padStart(3, '0'), tool.get('name').toLowerCase()].join(
    '-'
  )

const CmsContentsTechStack = props => {
  const { content } = props

  const tools = content.getIn(['properties', 'tools'])
  const categories =
    tools && tools.filter(t => t.get('tools') && t.get('tools').size).sortBy(t => t.get('position'))

  const slidesToShow = categories ? Math.min(categories.size, 3) : 3
  const swiper = useSwiper({
    slidesToShow,
    slidesToSwipe: 1,
    loop: true,
    renderPaginationItem: () => null,
  })

  return (
    <Content>
      <S.StackCategories>
        <Swiper {...swiper}>
          {categories &&
            categories.map(category => {
              const categoryTools = category.get('tools').sortBy(sortTools).reverse()
              return (
                <Swiper.Slide key={category.get('reference')}>
                  <S.StackCategory>
                    <S.StackCategoryHeader>
                      <S.StackCategoryTitle>{category.get('name')}</S.StackCategoryTitle>
                    </S.StackCategoryHeader>
                    <S.StackCategoryList>
                      {categoryTools.map((tool, i) => (
                        <StackCategoryListItem
                          key={tool.get('reference')}
                          position={i}
                          tool={tool}
                        />
                      ))}
                    </S.StackCategoryList>
                  </S.StackCategory>
                </Swiper.Slide>
              )
            })}
        </Swiper>
        <S.SwiperControls>
          <S.SwiperControl onClick={swiper.goPrev}>
            <Icons.Left mr="xs" size="sm" />
            <S.SwiperControlLabel>
              <FormattedMessage id="cms.contents.tech-stack.swiper-controls.previous" />
            </S.SwiperControlLabel>
          </S.SwiperControl>
          <S.SwiperControl onClick={swiper.goNext} rounded>
            <S.SwiperControlLabel>
              <FormattedMessage id="cms.contents.tech-stack.swiper-controls.next" />
            </S.SwiperControlLabel>
            <Icons.Right ml="xs" size="sm" />
          </S.SwiperControl>
        </S.SwiperControls>
      </S.StackCategories>
    </Content>
  )
}

export default CmsContentsTechStack
