import styled, { th } from '@xstyled/styled-components'

export const Content = styled.article``

export const ContentHeader = styled.header`
  position: relative;
  text-align: center;
  overflow: hidden;

  &::before,
  &::after {
    content: '';
    position: absolute;
    top: 50%;
    width: 4;
    height: 50%;
    border-top: 1px solid;
    border-top-color: light.800;
  }

  &::before {
    left: 0;
    border-left: 1px solid;
    border-left-color: light.800;
  }

  &::after {
    right: 0;
    border-right: 1px solid;
    border-right-color: light.800;
  }
`

export const ContentTitle = styled.h3`
  overflow: hidden;
  span {
    position: relative;
    display: inline-block;
    ${th('texts.subtitle1')};
    text-transform: uppercase;
    color: dark.900;

    &::before,
    &::after {
      position: absolute;
      top: 50%;
      width: 200%;
      height: 1px;
      margin-top: -1px;
      background-color: light.800;
      content: ' ';
    }

    &::before {
      right: 100%;
      margin-right: xs;
    }

    &::after {
      left: 100%;
      margin-left: xs;
    }
  }
`

export const ContentContent = styled.div`
  padding: xl lg;
  border: 1px solid;
  border-color: light.800;
  border-top: none;
  text-align: center;
`

export const List = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Item = styled.li`
  margin: 0 md;

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    margin-right: 0;
  }
`

export const ItemLink = styled.a`
  display: block;
  color: light.100;
  transition: medium;

  &:hover {
    color: dark.900;
  }
`
