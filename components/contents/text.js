import React, { Component } from 'react'
import ReactMarkdown from 'react-markdown'

import { Content } from '../content.styled'
import * as S from './text.styled'

class CmsContentsText extends Component {
  title() {
    const { content } = this.props
    if (content.getIn(['properties', 'title'])) {
      return content.getIn(['properties', 'title'])
    }
    return content.getIn(['properties', 'default_title'])
  }

  body() {
    const { content } = this.props
    if (content.getIn(['properties', 'body'])) {
      return content.getIn(['properties', 'body'])
    }
    return content.getIn(['properties', 'default_body'])
  }

  variant() {
    const { content } = this.props
    return content.getIn(['properties', 'theme'])
  }

  render() {
    const {
      contentProps: { onLinkClick },
    } = this.props
    const title = this.title()
    const body = this.body()
    const variant = this.variant()

    return (
      <Content variant={variant}>
        {title && (
          <S.ContentHeader>
            <S.ContentTitle>{title}</S.ContentTitle>
          </S.ContentHeader>
        )}
        {body && (
          <S.ContentContent onClick={onLinkClick}>
            <ReactMarkdown source={body} />
          </S.ContentContent>
        )}
      </Content>
    )
  }
}

export default CmsContentsText
