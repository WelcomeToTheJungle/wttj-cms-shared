import React, { Component } from 'react'
import ReactMarkdown from 'react-markdown'
import { Icons } from '@welcome-ui/icons.font'

import {
  Content,
  ContentHeader,
  ContentContent,
  ContentTitle,
  ContentIcon,
} from '../content.styled'
import * as S from './twitter.styled'

class CmsContentsTwitter extends Component {
  tweets() {
    const { content } = this.props
    return content.getIn(['properties', 'tweets'])
  }

  screenName() {
    const { content } = this.props
    return `@${content.getIn(['properties', 'screen_name'])}`
  }

  url() {
    const { content } = this.props
    return content.getIn(['properties', 'url'])
  }

  error() {
    const { content } = this.props
    return content.getIn(['properties', 'display_error']) && content.getIn(['properties', 'error'])
  }

  display() {
    const error = this.error()
    const tweets = this.tweets()
    if (error) {
      return error
    }
    if (!tweets || !tweets.size) {
      return null
    }
    if (tweets.size > 0 && tweets.get(0)) {
      return <ReactMarkdown source={tweets.get(0).normalize()} />
    }
  }

  onTwitterLink = e => {
    const {
      contentProps: { onSocialLinkClick },
    } = this.props
    e.preventDefault()
    onSocialLinkClick && onSocialLinkClick({ network: 'twitter', origin: 'social_network_block' })
    let link = e.target && e.target.tagName === 'A' ? e.target : null
    if (link) {
      global.open(link.href, 'blank')
      return
    }
    global.open(this.url(), 'blank')
  }

  render() {
    return (
      <Content>
        <S.TwitterLink onClick={this.onTwitterLink}>
          <ContentIcon>
            <Icons.Twitter w={30} h={30} />
          </ContentIcon>
          <ContentHeader>
            <ContentTitle>{this.screenName()}</ContentTitle>
          </ContentHeader>
          <ContentContent>
            <S.TwitterList>
              <S.TwitterListItem>{this.display()}</S.TwitterListItem>
            </S.TwitterList>
          </ContentContent>
        </S.TwitterLink>
      </Content>
    )
  }
}

export default CmsContentsTwitter
