import React, { PureComponent } from 'react'
import { Stat, Headline, Title, Percent, Measure } from './stats.styled'
import { Tooltip } from '../tooltips/tooltip.styled'

class CmsContentsStats extends PureComponent {
  render() {
    const { content } = this.props
    const title = content.getIn(['properties', 'title'])
    const subtitle = content.getIn(['properties', 'subtitle'])
    const percent = content.getIn(['properties', 'percent'])
    return (
      <Stat>
        <Headline>
          <Title>
            {title}
            {subtitle && <Tooltip position="left">{subtitle}</Tooltip>}
          </Title>
          <Percent>{percent}%</Percent>
        </Headline>
        <Measure percent={percent} />
      </Stat>
    )
  }
}

export default CmsContentsStats
