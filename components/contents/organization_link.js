import React, { Component } from 'react'
import { Icons } from '@welcome-ui/icons.font'

import { Content } from '../content.styled'
import { Metas, Meta, MetaIcon, MetaValue } from '../metas/metas.styled'
import { getName, getTitle, getOrganizationName, getAlt } from '../utils/content'
import { safeGetPath } from '../../utils/i18n'
import * as S from './organization_link.styled'

class CmsContentOrganizationLink extends Component {
  alt() {
    return getAlt(this.title(), this.organizationName())
  }

  organizationName() {
    const { content, contentProps } = this.props
    return getOrganizationName(content, contentProps)
  }

  title() {
    const { content } = this.props
    return getTitle(content)
  }

  background() {
    const { content } = this.props
    return content.getIn(['properties', 'background'])
  }

  logo() {
    const { content } = this.props
    return content.getIn(['properties', 'logo', 'url'])
  }

  metas() {
    const { content } = this.props
    if (!content.getIn(['properties', 'offices'])) {
      return null
    }
    return content.getIn(['properties', 'offices']).join(', ')
  }

  name() {
    const { content } = this.props
    return getName(content)
  }

  url() {
    const {
      content,
      contentProps: { host, lang, getPath },
    } = this.props

    return lang
      ? safeGetPath(getPath, `/companies/${content.getIn(['properties', 'slug'])}`, lang)
      : `${host}/companies/${content.getIn(['properties', 'slug'])}`
  }

  size() {
    const { columns } = this.props
    switch (columns) {
      case '1':
        return 'sm'
      case '2':
        return 'md'
      case '3':
        return 'lg'
      default:
        return 'sm'
    }
  }

  iconSize() {
    const { columns } = this.props
    switch (columns) {
      case '2':
      case '3':
        return 15
      default:
        return 12
    }
  }

  displayWarning() {
    const {
      content,
      contentProps: { source },
    } = this.props

    const status = content.getIn(['properties', 'status'])
    if (source === 'back' && status !== 'published') {
      return status
    }
    return false
  }

  displayToValidate() {
    const { content } = this.props
    const status = content.getIn(['properties', 'status'])
    const parentStatus = content.getIn(['properties', 'parent_status'])
    return status === 'to_validate' && parentStatus === 'to_validate'
  }

  displayContent() {
    const {
      content,
      contentProps: { source },
    } = this.props
    const status = content.getIn(['properties', 'status'])

    return source === 'back' || status === 'published' || this.displayToValidate()
  }

  render() {
    if (!this.displayContent()) {
      return null
    }

    return (
      <Content>
        <S.OrganizationLink
          href={this.url()}
          cover={this.background()}
          size={this.size()}
          title={this.alt()}
        >
          {this.displayWarning() && (
            <S.OrganizationLinkWarning>
              <Icons.Alert height={10} /> <span>{this.displayWarning()}</span>
            </S.OrganizationLinkWarning>
          )}
          <S.Header>
            <S.Logo src={this.logo()} alt={this.alt()} />
            <S.Infos>
              <S.Title>{this.name()}</S.Title>
              {this.metas() && (
                <Metas align="left" background="dark">
                  <Meta>
                    <MetaIcon>
                      <Icons.Location height={this.iconSize()} />
                    </MetaIcon>
                    <MetaValue>{this.metas()}</MetaValue>
                  </Meta>
                </Metas>
              )}
            </S.Infos>
          </S.Header>
        </S.OrganizationLink>
      </Content>
    )
  }
}

export default CmsContentOrganizationLink
