import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from '@welcome-ui/button'
import { Icons } from '@welcome-ui/icons.font'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'

import CmsLink from '../link'
import { Metas, Meta, MetaIcon, MetaValue } from '../metas/metas.styled'
import { safeGetPath } from '../../utils/i18n'
import * as S from './jobs.styled'

export const CmsContentsJobsItem = props => {
  const { job, lang, getPath } = props

  const path = safeGetPath(
    getPath,
    `/companies/${job.getIn(['website_organization', 'slug'])}/jobs/${job.get('slug')}`,
    lang
  )

  const contractType = job.get('contract_type')
  const city = job.get('city')
  const remote = job.get('remote')

  const shouldShowRemote = remote === 'partial' || remote === 'fulltime'
  const shouldShowCity = !shouldShowRemote && city

  return (
    <S.Job kind="job" to={path}>
      <S.Title>{job.get('name')}</S.Title>
      <Metas align="left" mobilealign="left">
        <Meta>
          <MetaIcon>
            <Icons.Write />
          </MetaIcon>
          <MetaValue>{contractType}</MetaValue>
        </Meta>

        {shouldShowRemote && (
          <Meta>
            <MetaIcon>
              <Icons.Remote />
            </MetaIcon>
            <MetaValue>
              <FormattedMessage id={`cms.jobs.remote.${remote}`} />
            </MetaValue>
          </Meta>
        )}
        {shouldShowCity && (
          <Meta>
            <MetaIcon>
              <Icons.Location />
            </MetaIcon>
            <MetaValue>{city}</MetaValue>
          </Meta>
        )}
      </Metas>
    </S.Job>
  )
}

const CmsContentsJobs = props => {
  const {
    content,
    contentProps: { lang, getPath },
  } = props
  const jobs = content.getIn(['properties', 'jobs'])
  const hasJobs = jobs && jobs.size > 0

  return (
    <Box border="1px solid" borderColor="light.800">
      <Text
        variant="subtitle1"
        px="xl"
        py="lg"
        backgroundColor="nude.200"
        textTransform="uppercase"
      >
        <FormattedMessage id="cms.contents.jobs.title" />
      </Text>
      {hasJobs &&
        jobs.map((job, i) => {
          const showFooter = jobs.size >= 3 && jobs.size - 1 === i

          return (
            <Box key={i} position="relative" minHeight={showFooter ? 120 : 'auto'}>
              <CmsContentsJobsItem job={job} lang={lang} getPath={getPath} />
              {showFooter && (
                <S.Footer>
                  <Button
                    as={CmsLink}
                    to={safeGetPath(
                      getPath,
                      `/companies/${content.getIn([
                        'properties',
                        'website_organization',
                        'slug',
                      ])}/jobs`,
                      lang
                    )}
                    w="100%"
                    dataTestId="jobs-button-all-jobs"
                  >
                    <FormattedMessage id="cms.contents.jobs.all-jobs" />
                  </Button>
                </S.Footer>
              )}
            </Box>
          )
        })}
      {!hasJobs && (
        <Text padding="xl" color="dark.200">
          <FormattedMessage id="cms.contents.jobs.no-jobs" />
        </Text>
      )}
    </Box>
  )
}

export default CmsContentsJobs
