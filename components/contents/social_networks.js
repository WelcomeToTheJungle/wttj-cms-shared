import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Icons } from '@welcome-ui/icons.font'

import * as S from './social_networks.styled'

const SOCIAL_NETWORK_ICONS = {
  facebook: Icons.Facebook,
  twitter: Icons.Twitter,
  youtube: Icons.Youtube,
  instagram: Icons.Instagram,
  linkedin: Icons.Linkedin,
  pinterest: Icons.Pinterest,
}

const CmsContentsSocialNetworks = props => {
  const { content, contentProps = {} } = props
  const networks = content.getIn(['properties', 'networks'])
  const { onSocialLinkClick, organization } = contentProps

  return (
    <S.Content>
      <S.ContentHeader>
        <S.ContentTitle>
          <FormattedMessage id="cms.contents.social-networks.title" />
        </S.ContentTitle>
      </S.ContentHeader>
      <S.ContentContent>
        <S.List>
          {networks &&
            networks.keySeq().map(network => {
              const Icon = SOCIAL_NETWORK_ICONS[network]
              return (
                <S.Item key={network}>
                  <S.ItemLink
                    href={networks.get(network)}
                    onClick={() =>
                      onSocialLinkClick && onSocialLinkClick({ network, origin: 'profile' })
                    }
                    rel="nofollow"
                    target="_blank"
                  >
                    <Icon title={organization && organization.get('name')} />
                  </S.ItemLink>
                </S.Item>
              )
            })}
        </S.List>
      </S.ContentContent>
    </S.Content>
  )
}

export default CmsContentsSocialNetworks
