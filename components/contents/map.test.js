import React from 'react'
import { fromJS } from 'immutable'

import { createRenderer, shallow } from '../../test/support'

import CmsContentsMap from './map'
import { MapWrapper, MapLink } from './map.styled'

const content = fromJS({
  kind: 'map',
  position: 1,
  properties: {
    headquarter: {
      zip_code: '75002',
      city: 'Paris',
      address: '11 bis, rue Bachaumont',
      longitude: -1.4934942,
      latitude: 47.2574292,
    },
    google_maps_api_key: 'GMAP_API_KEY',
  },
})

test('CmsContentsMap [snapshot] should render proper DOM', () => {
  const component = createRenderer(<CmsContentsMap content={content} />)

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsMap [shallow] should render MapWrapper', () => {
  const wrapper = shallow(<CmsContentsMap content={content} />)

  const coordinates = `${content.getIn(['properties', 'headquarter', 'latitude'])},${content.getIn([
    'properties',
    'headquarter',
    'longitude',
  ])}`

  expect(wrapper).to.contain(
    <MapWrapper
      cover={`https://maps.googleapis.com/maps/api/staticmap?center=${coordinates}&size=300x300&sensor=true&scale=2&key=${content.getIn(
        ['properties', 'google_maps_api_key']
      )}&zoom=15&markers=color:0x00c69e%7C${coordinates}`}
    />
  )
})

test('CmsContentsMap [shallow] should render MapLink', () => {
  const wrapper = shallow(<CmsContentsMap content={content} />)

  const mapLink = wrapper.find(MapLink)
  expect(mapLink.prop('href')).to.equal(
    'http://maps.google.com/?q=11 bis, rue Bachaumont, 75002, Paris'
  )
  expect(mapLink.prop('target')).to.equal('_blank')
})

test('CmsContentsMap [shallow] should render address as text', () => {
  const wrapper = shallow(<CmsContentsMap content={content} />)

  expect(wrapper).to.contain(
    content.getIn(['properties', 'headquarter', 'address']),
    content.getIn(['properties', 'headquarter', 'zip_code']),
    content.getIn(['properties', 'headquarter', 'city'])
  )
})
