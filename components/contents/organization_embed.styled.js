import styled, { css, th } from '@xstyled/styled-components'

import { color, fontSize, radius, fontWeight, transition } from '../../utils/theme-helpers'

export const FieldGroup = styled.div`
  position: relative;
  margin-bottom: lg;

  &:last-child {
    margin-bottom: 0;
  }
`

export const FieldLabel = styled.label`
  display: block;
  padding-bottom: xs;
  color: ${color('texts', 'light')};
  font-weight: ${fontWeight('bold')};
  line-height: 1.4;
  user-select: none;
`

export const fieldStyles = css`
  display: block;
  padding: md;
  width: 100%;
  font-size: ${fontSize('md')};
  border: 1px solid ${color('gray', '150')};
  border-radius: ${radius('sm')};
  color: ${color('texts', 'dark')};
  outline: none;
  appearance: none;
  transition: ${transition('md')};

  &::placeholder {
    color: ${color('gray', '200')};
    transition: ${transition('md')};
  }

  &:hover,
  &:focus {
    border-color: ${color('gray', '200')};

    &::placeholder {
      color: ${color('gray', '300')};
    }
  }
`

const errorStyles = css`
  border-color: ${color('red', '500')};
`

export const InputField = styled.input`
  ${fieldStyles};
  ${props => (props.errors ? errorStyles : null)};
`

export const TextareaField = styled.textarea`
  ${fieldStyles};
`

export const FormResultP = styled.p`
  margin-bottom: lg;
  color: ${color('red', '500')};

  &:last-child {
    margin-bottom: 0;
  }
`

export const FormError = styled.div`
  position: relative;
  margin-bottom: lg;
  border-radius: ${radius('md')};
  padding: lg;
  line-height: 1.5;
  text-align: left;
  background-color: ${color('red', '100')};
  color: ${color('red', '500')};
`

export const FieldWrapper = styled.div`
  position: relative;
`

export const unitValueStyles = css`
  flex: 0 0 auto;
  display: block;
  padding: md;
  font-size: ${fontSize('md')};
  border: 1px solid ${color('gray', '150')};
  background-color: ${color('gray', '100')};
  border-left: none;
  border-radius: 0 ${radius('sm')} ${radius('sm')} 0;
`

export const UnitSelectField = styled.select`
  appearance: none;
  ${unitValueStyles}
  padding-right: calc(${th('space.xxl')} + ${th('space.xxs')});
`

export const UnitValue = styled.span`
  ${unitValueStyles};
`

export const FieldWrapperWithUnit = styled(FieldWrapper)`
  display: flex;
  align-items: flex-end;
`

export const UnitSelectFieldWrapper = styled.div`
  position: relative;
  flex-shrink: 0;
`

export const UnitSelectFieldIcon = styled.div`
  position: absolute;
  top: 50%;
  right: md;
  transform: translateY(-50%);
  pointer-events: none;
`

export const Form = styled.form`
  margin-bottom: lg;

  &:last-child {
    margin-bottom: 0;
  }
`
