import React from 'react'
import { Box } from '@welcome-ui/box'
import { Text } from '@welcome-ui/text'

const CmsContentsNumber = props => {
  const { content } = props

  return (
    <Box backgroundColor={content.getIn(['properties', 'background']) || 'sub.1'} padding="xl">
      <Text variant="h4" mb="sm" lines={1}>
        {content.getIn(['properties', 'value'])}
      </Text>
      <Text variant="body3" lines={2}>
        {content.getIn(['properties', 'label'])}
      </Text>
    </Box>
  )
}

export default CmsContentsNumber
