import styled from '@xstyled/styled-components'
import { color, radius } from '../../utils/theme-helpers'
import { withTooltipStyles } from '../tooltips/tooltip.styled'

export const Stat = styled.div`
  padding: xs 0;
`

export const Headline = styled.div`
  display: flex;
  justify-content: space-between;
`

export const Title = styled.p`
  color: ${color('gray', 300)};
  ${withTooltipStyles};
`

export const Percent = styled.p`
  color: dark.900;
`

export const Measure = styled.div`
  position: relative;
  height: 1px;
  width: 100%;
  background: ${color('gray', 150)};
  margin-top: xs;

  ::before {
    content: ' ';
    position: absolute;
    left: 0;
    top: -1px;
    height: 3px;
    width: ${props => `${props.percent}%`};
    border-radius: ${radius('sm')};
    background: ${color('primary')};
  }
`
