import React, { PureComponent, Component, Fragment } from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import { Button } from '@welcome-ui/button'
import { Field } from '@welcome-ui/field'
import { Label } from '@welcome-ui/label'
import { InputText } from '@welcome-ui/input-text'
import { Textarea } from '@welcome-ui/textarea'
import { Select } from '@welcome-ui/select'
import { Icons } from '@welcome-ui/icons.font'

import { Content, ContentHeader, ContentTitle, ContentContent } from './social_networks.styled'
import { Grid, Column } from '../layouts/grid.styled'
import { FieldWrapperWithUnit, FormError, FormResultP, Form } from './organization_embed.styled'
import { ModalContentSection, ModalContentSectionTitle } from '../modals/modals.styled'

import { Flag } from '../icons/flag'

const FORM_NAME = 'organization-embed'

const DEFAULT_STATE = {
  widthValue: 100,
  widthUnit: '%',
  heightValue: 700,
  heightUnit: 'px',
  columns: 3,
  lines: 2,
}

const VALIDATIONS = {
  width: {
    '%': {
      min: 30,
      max: 100,
    },
    px: {
      min: 250,
    },
  },
  height: {
    px: {
      min: 300,
    },
  },
  lines: {
    min: 1,
    max: 5,
  },
  columns: {
    min: 1,
    max: 5,
  },
}

const getLabel = field => <FormattedMessage id={`cms.contents.${FORM_NAME}.form.labels.${field}`} />

class Iframe extends PureComponent {
  render() {
    const { url, width, height } = this.props
    return <iframe title="WTTJ" src={url} width={width} height={height} frameBorder="0" />
  }
}

class ModalContent extends Component {
  constructor(props) {
    const {
      content,
      intl: { formatMessage },
    } = props
    super(props)

    const languages =
      content &&
      content
        .getIn(['properties', 'languages'])
        .toJS()
        .map(locale => ({
          value: locale,
          label: formatMessage({ id: `cms.languages.${locale}` }),
        }))

    const language =
      content &&
      this.getLanguage(
        content.getIn(['properties', 'website_organization', 'default_language']),
        languages
      )

    const initialState = {
      ...DEFAULT_STATE,
      language: language.value,
    }

    this.state = {
      ...initialState,
      preview: initialState,
      languages,
      errors: {},
    }
  }

  getLanguage = (key, languages) => languages.find(language => language.value === key)

  iframeUrl() {
    const {
      content,
      contentProps: { host },
    } = this.props
    const {
      preview: { columns, lines, language },
    } = this.state

    const path = [
      host,
      language,
      'companies',
      content.getIn(['properties', 'website_organization', 'slug']),
      'embed',
    ].join('/')

    return `${path}?c=${columns}&l=${lines}`
  }

  width() {
    const {
      preview: { widthValue, widthUnit },
    } = this.state
    return `${widthValue}${widthUnit}`
  }

  height() {
    const {
      preview: { heightValue, heightUnit },
    } = this.state
    return `${heightValue}${heightUnit}`
  }

  onChangeValue(kind, e) {
    const value = ['widthUnit', 'language'].includes(kind) ? e : e.target.value
    this.setState({ [kind]: value }, this.validate)
  }

  validate() {
    let errors = {}
    const { widthValue, widthUnit, heightValue, heightUnit, lines, columns } = this.state
    const {
      intl: { formatMessage },
    } = this.props
    if (widthUnit === '%' && widthValue > VALIDATIONS.width[widthUnit].max) {
      errors.widthValue = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.width.larger' },
        {
          value: VALIDATIONS.width[widthUnit].max,
          unit: widthUnit,
        }
      )
    } else if (widthValue < VALIDATIONS.width[widthUnit].min) {
      errors.widthValue = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.width.smaller' },
        {
          value: VALIDATIONS.width[widthUnit].min,
          unit: widthUnit,
        }
      )
    }
    if (heightValue < VALIDATIONS.height[heightUnit].min) {
      errors.heightValue = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.height.smaller' },
        {
          value: VALIDATIONS.height[heightUnit].min,
          unit: heightUnit,
        }
      )
    }
    if (lines > VALIDATIONS.lines.max) {
      errors.lines = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.lines.larger' },
        {
          value: VALIDATIONS.lines.max,
        }
      )
    } else if (lines < VALIDATIONS.lines.min) {
      errors.lines = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.lines.smaller' },
        {
          value: VALIDATIONS.lines.min,
        }
      )
    }
    if (columns > VALIDATIONS.columns.max) {
      errors.columns = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.columns.larger' },
        {
          value: VALIDATIONS.columns.max,
        }
      )
    } else if (columns < VALIDATIONS.columns.min) {
      errors.columns = formatMessage(
        { id: 'cms.contents.organization-embed.form.errors.columns.smaller' },
        {
          value: VALIDATIONS.columns.min,
        }
      )
    }
    this.setState({ errors })
    return Object.keys(errors).length === 0
  }

  applyForm = e => {
    e && e.preventDefault()
    if (!this.validate()) {
      return
    }

    const { widthValue, widthUnit, heightValue, heightUnit, lines, columns, language } = this.state
    this.setState({
      preview: {
        widthValue,
        widthUnit,
        heightValue,
        heightUnit,
        columns,
        lines,
        language,
      },
    })
  }

  embedCode() {
    return `<iframe src="${this.iframeUrl()}" width="${this.width()}" height="${this.height()}" frameborder="0"></iframe>`
  }

  fieldValidationProps(field) {
    const { widthUnit, heightUnit } = this.state
    switch (field) {
      case 'widthValue':
        return VALIDATIONS.width[widthUnit]
      case 'heightValue':
        return VALIDATIONS.height[heightUnit]
      default:
        return VALIDATIONS[field]
    }
  }

  renderLanguageItem = item => (
    <Fragment>
      <Flag local={item.value} /> {item.label}
    </Fragment>
  )

  render() {
    const { errors } = this.state
    const formErrors = Object.keys(errors)

    return (
      <div>
        <ModalContentSection>
          <Grid columns={2} gutter="lg">
            <Column>
              <Form>
                <ModalContentSectionTitle>
                  <FormattedMessage id="cms.contents.organization-embed.form.title" />
                </ModalContentSectionTitle>
                <Grid columns={2} gutter="xs">
                  <Column>
                    <Fragment>
                      <Label error={this.state.errors.widthValue !== undefined} mb="sm">
                        {getLabel('widthValue')}
                      </Label>
                      <FieldWrapperWithUnit>
                        <InputText
                          type="number"
                          id="widthValue"
                          value={this.state.widthValue}
                          onChange={this.onChangeValue.bind(this, 'widthValue')}
                          ref={e => (this.widthValue = e)}
                          label={getLabel('widthValue')}
                          error={this.state.errors.widthValue !== undefined}
                          {...this.fieldValidationProps('widthValue')}
                          min={this.state.widthUnit === '%' ? 30 : 250}
                          max={this.state.widthUnit === '%' ? 100 : null}
                          flex="1 1 0"
                          mr="xs"
                        />
                        <Select
                          id="widthUnit"
                          options={[
                            {
                              value: '%',
                              label: '%',
                            },
                            {
                              value: 'px',
                              label: 'px',
                            },
                          ]}
                          value={{ value: this.state.widthUnit, label: this.state.widthUnit }}
                          onChange={this.onChangeValue.bind(this, 'widthUnit')}
                          ref={e => (this.widthUnit = e)}
                          required
                        />
                      </FieldWrapperWithUnit>
                    </Fragment>
                  </Column>
                  <Column>
                    <Field
                      component={InputText}
                      type="number"
                      id="heightValue"
                      value={this.state.heightValue}
                      onChange={this.onChangeValue.bind(this, 'heightValue')}
                      ref={e => (this.lines = e)}
                      label={getLabel('heightValue')}
                      error={this.state.errors.heightValue !== undefined}
                      {...this.fieldValidationProps('heightValue')}
                      icon={` ${DEFAULT_STATE.heightUnit} `}
                      iconPlacement="right"
                      min={300}
                      mb="lg"
                    />
                  </Column>
                  <Column>
                    <Field
                      component={InputText}
                      type="number"
                      id="lines"
                      value={this.state.lines}
                      onChange={this.onChangeValue.bind(this, 'lines')}
                      ref={e => (this.lines = e)}
                      label={getLabel('lines')}
                      error={this.state.errors.lines !== undefined}
                      {...this.fieldValidationProps('lines')}
                      min={1}
                      max={5}
                      mb="lg"
                    />
                  </Column>
                  <Column>
                    <Field
                      component={InputText}
                      type="number"
                      id="columns"
                      value={this.state.columns}
                      onChange={this.onChangeValue.bind(this, 'columns')}
                      ref={e => (this.columns = e)}
                      label={getLabel('columns')}
                      error={this.state.errors.columns !== undefined}
                      {...this.fieldValidationProps('columns')}
                      min={1}
                      max={5}
                      mb="lg"
                    />
                  </Column>
                  <Column>
                    <Field
                      component={Select}
                      options={this.state.languages}
                      type="number"
                      id="language"
                      required
                      renderItem={this.renderLanguageItem}
                      value={this.state.language}
                      onChange={this.onChangeValue.bind(this, 'language')}
                      ref={e => (this.language = e)}
                      label={getLabel('language')}
                      error={this.state.errors.language !== undefined}
                      {...this.fieldValidationProps('language')}
                      mb="lg"
                    />
                  </Column>
                </Grid>
                <Button onClick={this.applyForm} w="100%" mt="xl" size="lg">
                  <FormattedMessage id={`cms.contents.organization-embed.form.submit`} />
                </Button>
                {formErrors.length > 0 && (
                  <FormError>
                    {formErrors.map(key => (
                      <FormResultP key={key}>{errors[key]}</FormResultP>
                    ))}
                  </FormError>
                )}
              </Form>
            </Column>
            <Column>
              <ModalContentSectionTitle>
                <FormattedMessage id="cms.contents.organization-embed.embed.title" />
              </ModalContentSectionTitle>
              <Field
                component={Textarea}
                id="code"
                readOnly
                value={this.embedCode()}
                label={getLabel('subtitle')}
                cols={50}
                rows={4}
              />
            </Column>
          </Grid>
        </ModalContentSection>
        <ModalContentSection>
          <ModalContentSectionTitle>
            <FormattedMessage id="cms.contents.organization-embed.preview.title" />
          </ModalContentSectionTitle>
          <Iframe url={this.iframeUrl()} width={this.width()} height={this.height()} />
        </ModalContentSection>
      </div>
    )
  }
}

ModalContent = injectIntl(ModalContent)

class CmsContentsOrganizationEmbed extends Component {
  open = e => {
    e && e.preventDefault()
    const {
      contentProps: { onOpenModal },
    } = this.props
    onOpenModal && onOpenModal(this.modalParams())
  }

  modalParams() {
    const { content } = this.props
    return {
      size: 'lg',
      title: (
        <FormattedMessage
          id="cms.contents.organization-embed.modal.title"
          values={{ organizationName: content.getIn(['properties', 'organization', 'name']) }}
        />
      ),
      content: <ModalContent {...this.props} />,
    }
  }

  render() {
    return (
      <Content>
        <ContentHeader>
          <ContentTitle>
            <FormattedMessage id="cms.contents.organization-embed.title" />
          </ContentTitle>
        </ContentHeader>
        <ContentContent>
          <Button variant="secondary" onClick={this.open}>
            <FormattedMessage id="cms.contents.organization-embed.button" />
          </Button>
        </ContentContent>
      </Content>
    )
  }
}

export default CmsContentsOrganizationEmbed
