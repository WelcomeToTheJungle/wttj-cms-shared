import React, { Component } from 'react'
import ReactMarkdown from 'react-markdown'
import { Icons } from '@welcome-ui/icons.font'

import {
  Content,
  ContentHeader,
  ContentContent,
  ContentTitle,
  ContentIcon,
} from '../content.styled'
import * as S from './facebook.styled'

class CmsContentsFacebook extends Component {
  messages() {
    const { content } = this.props
    return content.getIn(['properties', 'messages'])
  }

  userName() {
    const { content } = this.props
    return `@${content.getIn(['properties', 'username'])}`
  }

  url() {
    const { content } = this.props
    return content.getIn(['properties', 'url'])
  }

  error() {
    const { content } = this.props
    return content.getIn(['properties', 'display_error']) && content.getIn(['properties', 'error'])
  }

  display() {
    const error = this.error()
    const messages = this.messages()
    if (error) {
      return error
    }
    if (!messages || !messages.size) {
      return null
    }
    if (messages.size > 0 && messages.get(0)) {
      return <ReactMarkdown source={messages.get(0).normalize()} />
    }
  }

  onFacebookLink = e => {
    const {
      contentProps: { onSocialLinkClick },
    } = this.props
    e.preventDefault()
    onSocialLinkClick && onSocialLinkClick({ network: 'facebook', origin: 'social_network_block' })
    let link = e.target && e.target.tagName === 'A' ? e.target : null
    if (link) {
      global.open(link.href, 'blank')
      return
    }
    global.open(this.url(), 'blank')
  }

  render() {
    return (
      <Content>
        <S.FacebookLink onClick={this.onFacebookLink}>
          <ContentIcon>
            <Icons.Facebook w={30} h={30} />
          </ContentIcon>
          <ContentHeader>
            <ContentTitle>{this.userName()}</ContentTitle>
          </ContentHeader>
          <ContentContent>
            <S.FacebookList>
              <S.FacebookListItem>{this.display()}</S.FacebookListItem>
            </S.FacebookList>
          </ContentContent>
        </S.FacebookLink>
      </Content>
    )
  }
}

export default CmsContentsFacebook
