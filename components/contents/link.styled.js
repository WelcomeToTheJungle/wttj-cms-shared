import styled from '@xstyled/styled-components'
import { ContentLink } from '../content.styled'
import { color, rgba, fontSize, fontWeight } from '../../utils/theme-helpers'

export const ContentBlockLink = styled(ContentLink)`
  background-color: ${color('gray', 600)};
  padding: xxl;
`

export const Header = styled.header`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  padding: lg;
  background: linear-gradient(to top, ${rgba('dark', '900', 0.8)}, ${rgba('dark', '900', 0)});
  z-index: 10;
`

export const Title = styled.h3`
  font-size: ${fontSize('xl')};
  color: ${color('white')};
  font-weight: ${fontWeight('bold')};
  text-transform: uppercase;
`

export const Surtitle = styled.p`
  margin-bottom: xxs;
  font-size: ${fontSize('md')};
  color: ${rgba('light', '900', '0.8')};
`
