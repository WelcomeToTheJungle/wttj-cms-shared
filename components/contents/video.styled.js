import styled, { th } from '@xstyled/styled-components'
import { hoverScaleStyles } from '../utils/utils.styled'
import { rgba, fontSize } from '../../utils/theme-helpers'

export const Image = styled.img`
  display: block;
  width: 100%;
`

export const Header = styled.header`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  display: flex;
  align-items: center;
  padding: xl;
  background: linear-gradient(to top, ${rgba('dark', '900', 0.7)}, ${rgba('dark', '900', 0)});
  z-index: 10;
`

export const Button = styled.div`
  flex: 0 0 auto;
  width: 35px;
  height: 35px;
  background-color: primary.500;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: medium;
  margin-right: sm;
`

export const Infos = styled.div`
  flex: 1 1 auto;
`

export const Title = styled.h3`
  ${th('texts.h5')};
  color: light.900;
`

export const Subtitle = styled.p`
  font-size: ${fontSize('sm')};
  color: light.900;
`

export const VideoLink = styled.a`
  position: relative;
  display: block;
  overflow: hidden;
  border: 1px solid;
  border-color: light.800;
  cursor: pointer;

  &::after {
    content: '';
    position: absolute;
    top: 30%;
    left: 0;
    bottom: 0;
    width: 100%;
    background: linear-gradient(to top, ${rgba('dark', '900', 0.4)}, ${rgba('dark', '900', 0)});
    opacity: 0;
    transition: medium;
  }

  &:hover {
    border-color: light.700;
    &::after {
      opacity: 1;
    }
    ${Button} {
      transform: scale(1.1);
    }
  }
`
