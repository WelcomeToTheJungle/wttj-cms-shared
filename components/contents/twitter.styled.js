import styled from '@xstyled/styled-components'
import { ContentContent, ContentHeader, ContentTitle, ContentIcon } from '../content.styled'
import { color } from '../../utils/theme-helpers'
import { overflowEllipsis } from '../utils/utils.styled'
import { inlineLinkStyles } from '../buttons/button.styled'

export const TwitterLink = styled.div`
  display: block;
  position: relative;
  padding: 3xl xxs;
  background-color: ${color('socials', 'twitter')};
  color: light.900;
  cursor: pointer;

  ${ContentHeader} {
    padding-top: 0;
    padding-bottom: 0;
    border: none;
    padding-right: 40;
  }

  ${ContentTitle} {
    ${overflowEllipsis};
  }

  ${ContentIcon} {
    opacity: 0.2;
    transition: medium;
  }

  ${ContentContent} {
    padding-bottom: 0;
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: light.900;
    opacity: 0;
    transition: medium;
  }
`

export const TwitterList = styled.ul``

export const TwitterListItem = styled.li`
  a {
    ${inlineLinkStyles};
    color: light.900;
    z-index: 1;
  }
`
