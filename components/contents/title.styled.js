import React from 'react'
import styled from '@xstyled/styled-components'
import { Text } from '@welcome-ui/text'
import { Box } from '@welcome-ui/box'

export const Title = styled(Text).attrs({ variant: 'h3' })`
  color: dark.900;
`

export const ContainerBox = props => (
  <Box
    display={{ xs: 'block', md: 'flex' }}
    flexDirection={{ xs: null, md: 'row' }}
    textAlign={{ xs: 'center', md: 'left' }}
    {...props}
  />
)

export const TitleBox = props => <Box flex={{ xs: null, md: 1 }} {...props} />

export const ButtonBox = props => <Box m="auto 0 auto auto" my={{ xs: 15, md: 0 }} {...props} />
