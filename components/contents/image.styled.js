import styled from '@xstyled/styled-components'
import { ContentLink } from '../content.styled'
import { color, rgba, transition, fontSize, fontWeight } from '../../utils/theme-helpers'

export const Image = styled.img`
  display: block;
  width: 100%;
`

export const ImageLinkIcon = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  transform: scale3d(0, 0, 1);
  transition: ${transition('sm')};
  width: 50px;
  height: 50px;
  margin: auto;
  background-color: primary.500;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 2;
  color: dark.900;
`

export const Header = styled.header`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  padding: lg;
  background: linear-gradient(to top, ${rgba('dark', '900', 0.7)}, ${rgba('dark', '900', 0)});
  z-index: 10;
`

export const Title = styled.h3`
  font-size: ${fontSize('md')};
  color: ${color('white')};
  font-weight: ${fontWeight('bold')};
`

export const Subtitle = styled.p`
  margin-top: xxs;
  font-size: ${fontSize('sm')};
  color: ${color('white')};
`

export const ImageLink = styled(ContentLink)`
  overflow: hidden;
  transition: ${transition('md')};

  &::after {
    position: absolute;
    top: 30%;
    left: 0;
    bottom: 0;
    width: 100%;
    background: linear-gradient(to top, ${rgba('dark', '900', 0.4)}, ${rgba('dark', '900', 0)});
    opacity: 0;
    transition: ${transition('md')};
    content: ' ';
  }

  &:hover {
    &::after {
      opacity: 1;
    }

    ${ImageLinkIcon} {
      transform: scale3d(1, 1, 1);
    }
  }
`
