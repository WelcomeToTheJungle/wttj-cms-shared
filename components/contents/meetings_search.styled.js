import styled, { th } from '@xstyled/styled-components'
import { Link } from 'react-router-dom'
import { hoverCardStyles } from '../utils/utils.styled'
import {
  boxShadow,
  color,
  fontSize,
  fontWeight,
  transition,
  radius,
} from '../../utils/theme-helpers'

export const MeetingThumb = styled(Link)`
  position: relative;
  display: block;
  margin-bottom: xxl;
  padding: xxl;
  padding-left: calc(${th('space.xxl')} * 2 + ${th('space.6xl')});
  overflow: hidden;
  border-bottom: 1px solid ${color('gray', '150')};
  background-color: ${color('white')};
  border-radius: ${radius('md')};
  box-shadow: ${boxShadow('md')};
  ${hoverCardStyles};

  &:last-child {
    margin-bottom: none;
    border-bottom: none;
  }
`

export const MeetingThumbTitle = styled.h3`
  font-size: ${fontSize('lg')};
  color: ${color('texts', 'dark')};
  margin-bottom: xs;
  transition: ${transition('md')};
`

export const MeetingThumbDate = styled.p`
  position: absolute;
  top: 50%;
  left: xxl;
  transform: translateY(-50%);
  width: ${th('space.6xl')};
  text-align: center;
`

export const MeetingThumbDateDay = styled.span`
  display: block;
  font-size: ${fontSize('xxl')};
  color: ${color('texts', 'dark')};
  font-weight: ${fontWeight('bold')};
`

export const MeetingThumbDateMonth = styled.span`
  display: block;
  text-transform: uppercase;
  color: ${color('texts', 'light')};
  font-size: ${fontSize('xs')};
`
