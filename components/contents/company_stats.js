import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { Icons } from '@welcome-ui/icons.font'

import { Content, ContentContent } from '../content.styled'
import * as S from './company_stats.styled'

class CmsContentsCompanyStats extends Component {
  renderStat(key) {
    const { content } = this.props
    const stat = content.getIn(['properties', key])
    if (stat !== undefined) {
      return (
        <S.StatsListItem>
          <S.StatsListItemLabel>
            <FormattedMessage id={`cms.contents.company-stats.${key}`} />
          </S.StatsListItemLabel>
          <S.StatsListItemValue>
            {stat}
            {(key === 'average_age' || key === 'turnover') && (
              <FormattedMessage id={`cms.contents.company-stats.${key}_unit`} />
            )}
          </S.StatsListItemValue>
        </S.StatsListItem>
      )
    }
  }

  renderParity() {
    const { content } = this.props
    if (
      content.getIn(['properties', 'parity_women']) &&
      content.getIn(['properties', 'parity_men'])
    ) {
      return (
        <S.StatsListItem w="full">
          <S.StatsListItemLabel>
            <FormattedMessage id="cms.contents.company-stats.parity" />
          </S.StatsListItemLabel>
          <S.StatsListItemValue>
            <Icons.Female height={16} title="Female" />
            {content.getIn(['properties', 'parity_women'])} %
          </S.StatsListItemValue>
          <S.StatsListItemValue>
            <Icons.Male height={16} title="Male" />
            {content.getIn(['properties', 'parity_men'])} %
          </S.StatsListItemValue>
        </S.StatsListItem>
      )
    }
  }

  render() {
    return (
      <Content>
        <ContentContent>
          <S.StatsList>
            {this.renderStat('creation_year')}
            {this.renderStat('nb_employees')}
            {this.renderParity()}
            {this.renderStat('average_age')}
            {this.renderStat('turnover')}
            {this.renderStat('revenue')}
          </S.StatsList>
        </ContentContent>
      </Content>
    )
  }
}

export default CmsContentsCompanyStats
