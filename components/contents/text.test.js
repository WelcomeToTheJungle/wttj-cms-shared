import React from 'react'
import { fromJS } from 'immutable'
import ReactMarkdown from 'react-markdown'

import { createRenderer, shallow } from '../../test/support'

import CmsContentsText from './text'

const content = fromJS({
  kind: 'text',
  position: 1,
  properties: {
    title: 'This is a title',
    body: 'This is a body',
  },
})

const contentProps = { onLinkClick: console.debug }

test('CmsContentsText [snapshot] should render proper DOM', () => {
  const component = createRenderer(
    <CmsContentsText content={content} contentProps={contentProps} />
  )

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsText [shallow] should render title and body', () => {
  const wrapper = shallow(<CmsContentsText content={content} contentProps={contentProps} />)

  expect(wrapper).to.contain(content.getIn(['properties', 'title']))
  expect(wrapper).to.contain(<ReactMarkdown source={content.getIn(['properties', 'body'])} />)
})
