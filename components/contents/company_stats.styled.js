import styled, { th } from '@xstyled/styled-components'

export const StatsList = styled.ul`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  margin: -1px;
`

export const StatsListItem = styled.li`
  position: relative;
  flex: 1 1 auto;
  width: ${props => (props.w === 'full' ? '100%' : '50%')};
  z-index: 2;
  padding: lg md;
  background: light.900;
  text-align: center;
  border-bottom: 1px solid;
  border-bottom-color: light.800;

  &::before,
  &::after {
    position: absolute;
    background-color: light.800;
    content: ' ';
  }

  &::before {
    top: 0;
    left: -1px;
    width: 1px;
    height: 100%;
  }

  &::after {
    bottom: -1px;
    left: 0;
    width: 100%;
    height: 1px;
  }
`

export const StatsListItemLabel = styled.h4`
  ${th('texts.subtitle2')};
  text-transform: uppercase;
  color: light.100;
  margin-bottom: xs;
`

export const StatsListItemValue = styled.span`
  display: inline-block;
  ${th('texts.h4')};

  & + span {
    margin-left: 3xl;
  }

  svg {
    display: inline-block;
    vertical-align: middle;
    margin-right: xs;
  }
`
