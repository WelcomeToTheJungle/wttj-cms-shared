import styled, { th } from '@xstyled/styled-components'
import { Text } from '@welcome-ui/text'
import { FormattedText } from '../utils/formatted_text.styled'

export const ContentHeader = styled.header`
  border-bottom: 1px solid;
  border-bottom-color: light.800;
  padding-bottom: lg;
  margin-bottom: lg;
`

export const ContentTitle = styled(Text).attrs({ variant: 'h4' })``

export const ContentContent = styled(FormattedText)`
  p,
  li,
  blockquote {
    ${th('texts.body2')};
  }
`
