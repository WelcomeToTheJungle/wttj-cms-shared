import React from 'react'
import { fromJS } from 'immutable'
import { FormattedMessage } from 'react-intl'

import { createRenderer, shallow } from '../../test/support'

import CmsContentsCompanyStats from './company_stats'
import { StatsListItem, StatsListItemLabel, StatsListItemValue } from './company_stats.styled'

const content = fromJS({
  kind: 'company-stats',
  position: 1,
  properties: {
    turnover: 10,
    revenue: '100k',
    parity_women: 85,
    parity_men: 15,
    nb_employees: 42,
    creation_year: 2015,
    average_age: 30,
  },
})

test('CmsContentsCompanyStats [snapshot] should render proper DOM', () => {
  const component = createRenderer(<CmsContentsCompanyStats content={content} />)

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsCompanyStats [shallow] should render creation_year', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  expect(wrapper).to.contain(
    <StatsListItem>
      <StatsListItemLabel>
        <FormattedMessage id={`cms.contents.company-stats.creation_year`} />
      </StatsListItemLabel>
      <StatsListItemValue>{content.getIn(['properties', 'creation_year'])}</StatsListItemValue>
    </StatsListItem>
  )
})

test('CmsContentsCompanyStats [shallow] should render nb_employees', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  expect(wrapper).to.contain(
    <StatsListItem>
      <StatsListItemLabel>
        <FormattedMessage id={`cms.contents.company-stats.nb_employees`} />
      </StatsListItemLabel>
      <StatsListItemValue>{content.getIn(['properties', 'nb_employees'])}</StatsListItemValue>
    </StatsListItem>
  )
})

test('CmsContentsCompanyStats [shallow] should render parity', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  const femaleIcon = wrapper.find('[title="Female"]')
  expect(femaleIcon).to.have.length(1)

  const maleIcon = wrapper.find('[title="Male"]')
  expect(maleIcon).to.have.length(1)

  const items = wrapper.find(StatsListItem)
  expect(items).to.have.length(6)

  const parities = wrapper.find(StatsListItemValue)
  const parityFemale = parities.get(2)
  expect(parityFemale.props.children[1]).to.equal(content.getIn(['properties', 'parity_women']))

  const parityMale = parities.get(3)
  expect(parityMale.props.children[1]).to.equal(content.getIn(['properties', 'parity_men']))
})

test('CmsContentsCompanyStats [shallow] should render average_age', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  expect(wrapper).to.contain(
    <StatsListItem>
      <StatsListItemLabel>
        <FormattedMessage id={`cms.contents.company-stats.average_age`} />
      </StatsListItemLabel>
      <StatsListItemValue>
        {content.getIn(['properties', 'average_age'])}
        <FormattedMessage id={`cms.contents.company-stats.average_age_unit`} />
      </StatsListItemValue>
    </StatsListItem>
  )
})

test('CmsContentsCompanyStats [shallow] should render turnover', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  expect(wrapper).to.contain(
    <StatsListItem>
      <StatsListItemLabel>
        <FormattedMessage id={`cms.contents.company-stats.turnover`} />
      </StatsListItemLabel>
      <StatsListItemValue>
        {content.getIn(['properties', 'turnover'])}
        <FormattedMessage id={`cms.contents.company-stats.turnover_unit`} />
      </StatsListItemValue>
    </StatsListItem>
  )
})

test('CmsContentsCompanyStats [shallow] should render revenue', () => {
  const wrapper = shallow(<CmsContentsCompanyStats content={content} />)

  expect(wrapper).to.contain(
    <StatsListItem>
      <StatsListItemLabel>
        <FormattedMessage id={`cms.contents.company-stats.revenue`} />
      </StatsListItemLabel>
      <StatsListItemValue>{content.getIn(['properties', 'revenue'])}</StatsListItemValue>
    </StatsListItem>
  )
})
