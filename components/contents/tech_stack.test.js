import React from 'react'
import { fromJS } from 'immutable'

import { createRenderer, mount, shallow } from '../../test/support'

import CmsContentsTechStack from './tech_stack'
import { StackCategoryTitle, StackToolPictureImg, StackToolTitle } from './tech_stack.styled'

const content = fromJS({
  kind: 'tech-stack',
  position: 1,
  properties: {
    tools: [
      {
        tools: [
          {
            percent: 50,
            name: 'React JS',
            image: {
              url: 'https://img.com/react.png',
              thumb: {
                url: 'https://img.com/react-thumb.png',
              },
            },
          },
        ],
        position: 1,
        name: 'Frontend',
      },
    ],
  },
})

test.skip('CmsContentsTechStack [snapshot] should render proper DOM', () => {
  const component = createRenderer(<CmsContentsTechStack content={content} />)

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

// TODO: Figure out how to access theme
test.skip('CmsContentsTechStack [shallow] should render categories', () => {
  const wrapper = shallow(<CmsContentsTechStack content={content} />)

  expect(wrapper).to.contain(<StackCategoryTitle>Frontend</StackCategoryTitle>)
})

// TODO: Figure out how to access theme
test.skip('CmsContentsTechStack [shallow] should render tools with image', () => {
  const wrapper = shallow(<CmsContentsTechStack content={content} />)

  expect(wrapper).to.contain(<StackToolTitle>React JS</StackToolTitle>)
  expect(wrapper).to.contain(
    <StackToolPictureImg alt="React JS" className="thumb" src="https://img.com/react-thumb.png" />
  )
})
