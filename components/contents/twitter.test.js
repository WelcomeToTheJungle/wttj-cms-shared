import React from 'react'
import { fromJS } from 'immutable'

import { createRenderer, shallow, mount } from '../../test/support'

import ReactMarkdown from 'react-markdown'
import CmsContentsTwitter from './twitter'
import { ContentTitle } from '../content.styled'
import { TwitterLink, TwitterListItem } from './twitter.styled'

const content = fromJS({
  kind: 'twitter',
  position: 1,
  properties: {
    screen_name: 'WTTJ',
    tweets: ['Hello World'],
    url: 'http://twitter.com/WTTJ',
  },
})

const contentProps = { onSocialLinkClick: console.debug }

test('CmsContentsTwitter [snapshot] should render proper DOM', () => {
  const component = createRenderer(
    <CmsContentsTwitter content={content} contentProps={contentProps} />
  )

  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('CmsContentsTwitter [shallow] should render screen name and messages', () => {
  const wrapper = shallow(<CmsContentsTwitter content={content} contentProps={contentProps} />)

  expect(wrapper).to.contain(<ContentTitle>@WTTJ</ContentTitle>)
  expect(wrapper).to.contain(
    <TwitterListItem key={0}>
      <ReactMarkdown source={content.getIn(['properties', 'tweets']).get(0)} />
    </TwitterListItem>
  )
  expect(wrapper.find('ReactMarkdown').html()).contains('Hello World')
})

test('CmsContentsTwitter [mount] should decode html characters', () => {
  let c = content.setIn(['properties', 'tweets', 0], '&commat;Hello &lt; World &gt;')
  const wrapper = mount(<CmsContentsTwitter content={c} contentProps={contentProps} />)
  expect(wrapper.find('ReactMarkdown').first().text()).to.equal('@Hello < World >')
})

test('CmsContentsTwitter [full dom] should render link', () => {
  const wrapper = mount(<CmsContentsTwitter content={content} contentProps={contentProps} />)
  const onTwitterLink = jest.spyOn(wrapper.instance(), 'onTwitterLink')

  // https://github.com/airbnb/enzyme/issues/365
  wrapper.update()
  wrapper.instance().forceUpdate()

  const link = wrapper.find(TwitterLink)
  expect(link.type().target).not.to.equal('a')
  expect(link.type().target).not.to.equal('button')
  expect(link).to.have.length(1)
  expect(wrapper.instance().url()).to.equal('http://twitter.com/WTTJ')
  expect(onTwitterLink).toHaveBeenCalledTimes(0)
  expect(global.open).toHaveBeenCalledTimes(0)

  link.first().simulate('click')
  wrapper.update()
  expect(onTwitterLink).toHaveBeenCalledTimes(1)
  expect(global.open).toBeCalledWith('http://twitter.com/WTTJ', 'blank')

  onTwitterLink.mockRestore()
  global.open.mockRestore()
})

test('CmsContentsTwitter [full dom] containing a link should be clickable', () => {
  let c = content.setIn(['properties', 'tweets', 0], 'Hello World [link](http://google.fr/)')
  const wrapper = mount(<CmsContentsTwitter content={c} contentProps={contentProps} />)
  const onTwitterLink = jest.spyOn(wrapper.instance(), 'onTwitterLink')

  // https://github.com/airbnb/enzyme/issues/365
  wrapper.update()
  wrapper.instance().forceUpdate()

  const externalLink = wrapper.find('a')
  expect(externalLink).to.have.length(1)
  expect(externalLink.prop('href')).to.equal('http://google.fr/')
  expect(externalLink.type()).to.equal('a')
  expect(onTwitterLink).toHaveBeenCalledTimes(0)
  expect(global.open).toHaveBeenCalledTimes(0)

  externalLink.first().simulate('click')
  expect(onTwitterLink).toHaveBeenCalledTimes(1)
  expect(global.open).toHaveBeenCalledTimes(1)

  onTwitterLink.mockRestore()
  global.open.mockRestore()
})
