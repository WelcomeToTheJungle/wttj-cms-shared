import styled, { css, th } from '@xstyled/styled-components'
import { ContentLink } from '../content.styled'
import { Metas } from '../metas/metas.styled'
import { color, fontSize, imagesHeight, rgba, radius, transition } from '../../utils/theme-helpers'
import { getBackgroundImage, backgroundCover } from '../utils/utils.styled'

export const Header = styled.header`
  display: flex;
  align-items: center;
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  z-index: 10;
`

export const Logo = styled.img`
  display: block;
  border-radius: ${radius('md')};
  flex: none;
`

export const Infos = styled.div`
  flex: 1 1 auto;
  min-width: 1px;
`

export const Title = styled.h3`
  ${th('texts.h4')};
  color: light.900;
`

const organizationLinkSmStyles = css`
  height: ${imagesHeight('organizations', 'contents', 'link')};

  ${Header} {
    padding: lg;
  }

  ${Title} {
    font-size: ${fontSize('lg')};
  }

  ${Logo} {
    width: 2.5rem;
    height: 2.5rem;
    margin-right: lg;
  }

  ${Metas} {
    margin-top: xxs;
  }
`

const organizationLinkMdStyles = css`
  height: ${imagesHeight('organizations', 'contents', 'link')};

  ${Header} {
    padding: xxl;
  }

  ${Title} {
    font-size: ${fontSize('xxl')};
  }

  ${Logo} {
    width: 3.75rem;
    height: 3.75rem;
    margin-right: xxl;
  }

  ${Metas} {
    margin-top: xs;
  }
`

const organizationLinkLgStyles = css`
  height: 31.25rem;

  ${Header} {
    padding: 3xl;
  }

  ${Title} {
    font-size: ${fontSize('xxl')};
  }

  ${Logo} {
    width: 5rem;
    height: 5rem;
    margin-right: xxl;
  }

  ${Metas} {
    margin-top: md;
  }
`

const getOrganizationLinkStyles = size => {
  switch (size) {
    case 'sm':
      return organizationLinkSmStyles
    case 'md':
      return organizationLinkMdStyles
    case 'lg':
      return organizationLinkLgStyles
    default:
      return organizationLinkLgStyles
  }
}

export const OrganizationLink = styled(ContentLink)`
  ${props => getBackgroundImage(props.cover)};
  ${backgroundCover};
  ${props => getOrganizationLinkStyles(props.size)};

  &::before,
  &::after {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 50%;
    transition: ${transition('md')};
    content: ' ';
  }

  &::before {
    background: linear-gradient(to top, ${rgba('dark', '900', 0.7)}, ${rgba('dark', '900', 0)});
  }

  &::after {
    opacity: 0;
    background: linear-gradient(to top, ${rgba('dark', '900', 0.4)}, ${rgba('dark', '900', 0)});
  }

  &:hover {
    &::after {
      opacity: 1;
    }
  }
`

export const OrganizationLinkWarning = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  color: ${color('white')};
  background-color: ${color('orange', '400')};
  padding: xxs;
  font-size: ${fontSize('s')};
  display: flex;
  align-items: center;

  svg {
    margin-right: 5px;
    path {
      fill: ${color('white')};
    }
  }
`
