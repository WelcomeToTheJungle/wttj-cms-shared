import styled, { th } from '@xstyled/styled-components'
import { scrollbar } from '../utils/utils.styled'

export const StackCategories = styled.div`
  background-color: dark.900;
  padding: 3xl;
`

export const StackCategory = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 0 3xl;
  margin: 0 xs;
  color: light.900;
  padding-right: 0;
`

export const StackCategoryHeader = styled.header`
  padding-bottom: lg;
  margin-right: lg;
  margin-bottom: lg;
  border-bottom: 1px solid;
  border-bottom-color: dark.700;
`

export const StackCategoryTitle = styled.h4`
  ${th('texts.h4')};
`

export const StackCategoryList = styled.ul`
  flex-grow: 1;
  max-height: 18rem;
  overflow: auto;
  padding-right: 3xl;
  ${props => `${scrollbar('4px', th('colors.light.900')(props), th('colors.dark.100')(props))}`};
`

export const SwiperControls = styled.div`
  display: flex;
  justify-content: space-between;
  border-top: 1px solid;
  border-top-color: dark.700;
  padding-top: xl;
`

export const SwiperControl = styled.button`
  display: flex;
  align-items: center;
  padding: 0;
  background: none;
  outline: none;
  border: none;
  cursor: pointer;
  color: light.900;
  transition: medium;

  svg path {
    fill: light.900;
    transition: medium;
  }

  &:hover {
    color: primary.500;

    svg path {
      fill: primary.500;
    }
  }

  &:active {
    transform: translateY(2px);
  }
`

export const SwiperControlLabel = styled.span`
  display: inline-block;
  margin: 0 xss;
  ${th('texts.subtitle1')};
`
