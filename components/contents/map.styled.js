import styled from '@xstyled/styled-components'
import { getBackgroundImage } from '../utils/utils.styled'
import { Content } from '../content.styled'

export const Wrapper = styled(Content)`
  border: 1px solid;
  border-color: light.800;
  transition: medium;

  &:hover {
    border-color: light.700;
  }
`

export const MapLink = styled.a`
  display: block;
  position: relative;
  color: dark.200;
`

export const MapWrapper = styled.div`
  width: 100%;
  height: 300px;
  background-color: light.200;
  ${props => getBackgroundImage(props.cover)};
  background-position: center center;
  background-size: cover;
`

export const MapAddress = styled.div`
  padding: lg;
`

export const MapAddressLine = styled.p``

export const MapAddressCity = styled.p`
  span + span {
    margin-left: xxs;
  }
`
