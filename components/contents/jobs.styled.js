import styled from '@xstyled/styled-components'
import CmsLink from '../link'
import { Text } from '@welcome-ui/text'

export const Title = styled(Text).attrs({
  variant: 'h4',
})`
  transition: medium;
  padding-bottom: sm;
`

export const Job = styled(CmsLink)`
  display: block;
  padding: xl;
  border-bottom: 1px solid;
  border-bottom-color: light.800;
  color: dark.900;

  &:hover,
  &:focus {
    ${Title} {
      opacity: 0.55;
    }
  }
`

export const Footer = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  top: 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: xl;

  &::before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    top: 0;
    background: linear-gradient(transparent, white 50%);
  }
`
