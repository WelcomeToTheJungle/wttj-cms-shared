import React from 'react'

import { render } from '../../test/rtl'
import CustomFormattedDate from './formatted_date'

const mockedDate = new Date('2016-07-21T14:00:00.000Z')
const RealDate = Date

beforeAll(() => {
  global.Date = jest.fn(date => {
    return date ? new RealDate(date) : mockedDate
  })
  global.Date.now = jest.fn(() => mockedDate.getTime())
})

afterAll(() => {
  global.Date = RealDate
  global.Date.now = RealDate.now
})

test('empty value', () => {
  const { container } = render(<CustomFormattedDate />)

  expect(container.querySelector('time')).toBeNull()
})

describe('relative date', () => {
  test('relative in year', () => {
    const { getByText } = render(<CustomFormattedDate isRelative value="2013-06-21" />)
    expect(getByText('3 years ago')).toBeInTheDocument()
  })

  test('relative in month', () => {
    const { getByText } = render(<CustomFormattedDate isRelative value="2016-06-21" />)
    expect(getByText('last month')).toBeInTheDocument()
  })

  test('relative in days', () => {
    const { getByText } = render(
      <CustomFormattedDate isRelative value="2016-07-20T13:00:00.000Z" />
    )
    expect(getByText('yesterday')).toBeInTheDocument()
  })

  test('relative in hours', () => {
    const { getByText } = render(
      <CustomFormattedDate isRelative value="2016-07-21T13:00:00.000Z" />
    )
    expect(getByText('1 hour ago')).toBeInTheDocument()
  })

  test('relative in minutes', () => {
    const { getByText } = render(
      <CustomFormattedDate isRelative value="2016-07-21T13:59:00.000Z" />
    )
    expect(getByText('1 minute ago')).toBeInTheDocument()
  })

  test('relative in seconds', () => {
    const { getByText } = render(
      <CustomFormattedDate isRelative value="2016-07-21T13:59:59.000Z" />
    )
    expect(getByText('1 second ago')).toBeInTheDocument()
  })

  test('now', () => {
    const { getByText } = render(
      <CustomFormattedDate isRelative value="2016-07-21T14:00:00.000Z" />
    )
    expect(getByText('now')).toBeInTheDocument()
  })

  test('updateIntervalInSeconds', async () => {
    const { getByText, findByText } = render(
      <CustomFormattedDate updateIntervalInSeconds={1} value="2016-07-21T14:00:00.000Z" />
    )
    expect(getByText('now')).toBeInTheDocument()
    expect(
      await findByText(
        '1 second ago',
        {},
        {
          // Default is 1s but we want to wait 1s to ensure that the text changes
          timeout: 2e3,
        }
      )
    ).toBeInTheDocument()
  })
})

test('short format', () => {
  const { getByText } = render(<CustomFormattedDate format="short" value={new Date()} />)
  expect(getByText('7/21')).toBeInTheDocument()
})

test('medium format', () => {
  const { getByText } = render(<CustomFormattedDate format="medium" value={new Date()} />)
  expect(getByText('July 21')).toBeInTheDocument()
})

test('default', () => {
  const { getByText } = render(<CustomFormattedDate value={new Date()} />)
  expect(getByText('July 21, 2016')).toBeInTheDocument()
})
