import React from 'react'
import PropTypes from 'prop-types'
import { FormattedTime } from 'react-intl'

export const CustomFormattedTime = ({ value, ...rest }) => {
  return (
    <time dateTime={value}>
      <FormattedTime value={value} {...rest} />
    </time>
  )
}

CustomFormattedTime.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
}
export default CustomFormattedTime
