import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { FormattedDate, FormattedRelativeTime, FormattedMessage } from 'react-intl'
import { selectDateUnit } from '../../utils/selectDateUnit'

export const CustomFormattedDate = ({
  value,
  format = 'long',
  updateIntervalInSeconds,
  isRelative,
}) => {
  // Should only be the case on articles that aren't published but that are previewed through the backoffice
  if (!value) return null

  let date = value
  if (typeof date === 'string') {
    // Date can already have a timezone
    date = isNaN(new Date(`${date}Z`)) ? date : `${date}Z`
  }
  let formattedRelativeTimeProps = selectDateUnit(new Date(date))
  // Only pass updateIntervalInSeconds if hours or less (https://formatjs.io/docs/react-intl/components/#formattedrelativetime)
  const isToday = ['second', 'minute', 'hour'].includes(formattedRelativeTimeProps.unit)

  if (isRelative || (updateIntervalInSeconds && isToday)) {
    return (
      <Fragment>
        {format !== 'short' && (
          <Fragment>
            <FormattedMessage id="cms.contents.date.published" />{' '}
          </Fragment>
        )}
        <time dateTime={value}>
          <FormattedRelativeTime
            {...formattedRelativeTimeProps}
            updateIntervalInSeconds={isToday && updateIntervalInSeconds}
            numeric="auto"
          />
        </time>
      </Fragment>
    )
  }

  if (format === 'short') {
    return (
      <time dateTime={value}>
        <FormattedDate value={value} month="numeric" day="numeric" />
      </time>
    )
  }

  if (format === 'medium') {
    return (
      <time dateTime={value}>
        <FormattedDate value={value} month="long" day="numeric" />
      </time>
    )
  }

  return (
    <time dateTime={value}>
      <FormattedDate value={value} year="numeric" month="long" day="numeric" />
    </time>
  )
}

CustomFormattedDate.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  format: PropTypes.oneOf(['short', 'medium', 'long']),
  updateIntervalInSeconds: PropTypes.number,
  isRelative: PropTypes.bool,
}
export default CustomFormattedDate
