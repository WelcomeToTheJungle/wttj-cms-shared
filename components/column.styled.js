import styled, { css } from '@xstyled/styled-components'
import { media } from './utils/utils.styled'

const mainStyles = css`
  max-width: calc(100% - 300px);
  ${media.mobile`
    max-width: 100%;
  `};
`

const getReferenceStyles = reference => {
  switch (reference) {
    case 'main':
      return mainStyles
    default:
      return null
  }
}

export const Column = styled.div`
  flex: ${({ w }) => (w !== 'auto' ? `0 0 ${w}` : '1 1 auto')};
  ${props => getReferenceStyles(props.reference)};
`

export default Column
