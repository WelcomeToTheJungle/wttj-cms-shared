import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import PropTypes from 'prop-types'

import { Section as StyledSection } from './section.styled'

const Section = props => {
  const { section, children } = props
  const styles = {
    maxWidth: section.getIn(['properties', 'max-width']),
  }

  return <StyledSection style={styles}>{children}</StyledSection>
}

Section.propTypes = {
  section: ImmutablePropTypes.map,
  children: PropTypes.node,
}

export default Section
