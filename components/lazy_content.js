import React, { useEffect, useState } from 'react'

export default function LazyContent({ placeHolder: PlaceHolder, children = null }) {
  const observerAvailable = typeof IntersectionObserver !== 'undefined'
  const [display, setDisplay] = useState(
    !observerAvailable && process.env.BUILD_TARGET !== 'server'
  )
  const [ref, setRef] = useState()

  useEffect(() => {
    let observer

    if (ref && !display) {
      if (observerAvailable && IntersectionObserver) {
        observer = new IntersectionObserver(
          entries => {
            entries.forEach(entry => {
              if (entry.intersectionRatio > 0 || entry.isIntersecting) {
                setDisplay(true)
                observer.unobserve(ref)
              }
            })
          },
          { threshold: 0.01, rootMargin: '10%' }
        )
        observer.observe(ref)
      } else {
        // Old browsers fallback
        setDisplay(true)
      }
    }
    return () => {
      if (observer && observer.unobserve) {
        observer.unobserve(ref)
      }
    }
  }, [ref])

  if (!display && PlaceHolder.prototype && !PlaceHolder.prototype.isReactComponent) {
    return <PlaceHolder forwardRef={setRef} />
  }
  if (!display) {
    return <PlaceHolder ref={setRef} />
  }
  return children
}
