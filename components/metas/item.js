import React from 'react'
import { bool, object, oneOfType, string } from 'prop-types'
import { Tooltip } from '@welcome-ui/tooltip'
import { Icons } from '@welcome-ui/icons.font'

import { Meta, MetaIcon, MetaValue } from './metas.styled'
import { MetaValueLoading } from './metas_loading.styled'
import { LoadingTextLine } from '../loading.styled'

const META_ICONS = {
  cities: <Icons.Location />,
  office: <Icons.Location />,
  address: <Icons.Location />,
  company_size: <Icons.Department />,
  contract_type: <Icons.Contract />,
  published_at: <Icons.Clock />,
  inserted_at: <Icons.Clock />,
  date: <Icons.Clock />,
  remote: <Icons.Remote />,
  website: <Icons.Earth />,
  salary: <Icons.Salary />,
  education: <Icons.EducationLevel />,
  video: <Icons.Video />,
  duration: <Icons.Clock />,
  folder: <Icons.Folder />,
  featured: <Icons.Star />,
  write: <Icons.Write />,
  calendar: <Icons.Date />,
  'remote-friendly': <Icons.Remote />,
  'covid-hiring': <Icons.Origine />,
  default: <Icons.Tag size="sm" />,
}

const MetaItem = props => {
  const {
    label,
    capitalize,
    value,
    className,
    title,
    loading,
    noEllipsis,
    ellipsisOnlyOnMobile,
    withTooltip,
    metaIconVariant,
  } = props

  const content = (
    <Meta className={className} title={title} noEllipsis={noEllipsis}>
      {label && (
        <MetaIcon variant={metaIconVariant} role="img">
          {META_ICONS[label] || META_ICONS.default}
        </MetaIcon>
      )}
      {!loading && value && (
        <MetaValue
          capitalize={capitalize}
          noEllipsis={noEllipsis}
          ellipsisOnlyOnMobile={ellipsisOnlyOnMobile}
        >
          {value}
        </MetaValue>
      )}
      {loading && (
        <MetaValueLoading>
          <LoadingTextLine w="full" height="sm" />
        </MetaValueLoading>
      )}
    </Meta>
  )

  return withTooltip ? (
    <Tooltip content={value} fixed placement="top-start">
      {content}
    </Tooltip>
  ) : (
    content
  )
}

MetaItem.propTypes = {
  label: string,
  value: oneOfType([string, object]),
  className: string,
  title: oneOfType([string, object]),
  noEllipsis: bool,
  loading: bool,
}

export default MetaItem
