import styled, { css, th, up } from '@xstyled/styled-components'
import { media, overflowEllipsis } from '../utils/utils.styled'
import { fontSize } from '../../utils/theme-helpers'

export const Meta = styled.li`
  ${th('texts.meta1')}
  max-width: 100%;
  align-items: center;
  font-size: inherit;

  ${props =>
    props.noEllipsis
      ? css`
          display: inline;
        `
      : css`
          display: flex;
          flex: 0 0 auto;
        `}

  i {
    opacity: 1;
  }
`

const metasBackgroundLightStyles = css`
  color: dark.200;

  ${Meta}::after {
    background-color: dark.200;
  }
`

const metasBackgroundDarkStyles = css`
  color: light.900;

  ${Meta}::after {
    background-color: light.900;
  }
`

const metasAlignCenterStyles = css`
  justify-content: center;

  ${Meta} {
    margin: 0 xs xxs;
  }
`

const metasAlignLeftStyles = css`
  justify-content: flex-start;

  ${Meta} {
    margin: 0 md xxs 0;
  }
`

const getFont = size => {
  switch (size) {
    case 'sm':
      return th('texts.meta2')
    case 'lg':
      return fontSize('lg')
    case 'body2':
      return th('texts.body2')
    default:
      return th('texts.meta1')
  }
}

const metasWithSeparatorStyles = css`
  ${Meta} {
    &::after {
      content: '';
      width: 3px;
      height: 3px;
      border-radius: 50%;
      margin-left: md;
    }

    &:last-child::after {
      display: none;
    }
  }
`

export const MetaLabel = styled.span`
  display: block;
`

const noEllipsisStyles = css`
  max-width: none !important;
`

const capitalizeStyles = css`
  text-transform: capitalize;
`

export const MetaValue = styled.span`
  ${({ noEllipsis, ellipsisOnlyOnMobile }) =>
    !ellipsisOnlyOnMobile && (noEllipsis ? noEllipsisStyles : overflowEllipsis)};
  ${({ capitalize }) => capitalize && capitalizeStyles};
  ${({ ellipsisOnlyOnMobile }) =>
    ellipsisOnlyOnMobile &&
    css`
      ${noEllipsisStyles};
      ${media.mobile`
        ${overflowEllipsis};
      `}
    `};
  color: inherit;
  font-size: inherit;
`

const roundedMetaStyles = css`
  border-radius: 50%;
  background-color: nude.100;
  padding: lg;
`

export const MetaIcon = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin-right: sm;
  flex-shrink: 0;
  ${({ variant }) => variant === 'rounded' && up('md', roundedMetaStyles)}
`

const metasSingleLine = css`
  ${MetaValue} {
    max-width: ${props => props.theme.metasValueMaxWidth};
    ${overflowEllipsis};

    ${media.mobile`
      max-width: 100%;
    `};
  }
`

const metasColumnStyles = css`
  align-items: flex-start;
  flex-direction: column;

  ${Meta} {
    align-items: flex-start;
    margin-left: 0;
    margin-right: 0;
  }

  ${MetaIcon} {
    width: 12px;
    flex: none;
    padding-top: 2px;
  }
`

const metasRowStyles = css`
  align-items: center;
  flex-direction: row;
`

export const Metas = styled.ul(
  ({
    multilines,
    size,
    direction,
    $withSeparator,
    align,
    background,
    $mobileAlign,
    $mobileDirection,
  }) => css`
    display: flex;
    flex-wrap: wrap;
    margin-bottom: -xxs;
    ${getFont(size)};

    ${!multilines && metasSingleLine};
    ${direction === 'column' ? metasColumnStyles : metasRowStyles};
    ${align === 'left' ? metasAlignLeftStyles : metasAlignCenterStyles};
    ${background === 'dark' ? metasBackgroundDarkStyles : metasBackgroundLightStyles};
    ${$withSeparator && metasWithSeparatorStyles};

    ${media.mobile`
      ${$mobileAlign === 'left' ? metasAlignLeftStyles : metasAlignCenterStyles}
      ${$mobileDirection === 'column' ? metasColumnStyles : metasRowStyles};
      width: 100%;
    `};
  `
)

export const MetaLink = styled.a`
  color: light.900;

  ${media.print`
    color: light.800;
  `};

  &:hover {
    text-decoration: underline;
  }
`

export const MetaAuthorImg = styled.img`
  display: block;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  overflow: hidden;
  background-color: light.500;
  margin-right: xs;
`
