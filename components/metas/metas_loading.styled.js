import styled, { th } from '@xstyled/styled-components'
import { MetaValue } from './metas.styled'

export const MetaValueLoading = styled(MetaValue)`
  width: ${th('space.7xl')};
`
