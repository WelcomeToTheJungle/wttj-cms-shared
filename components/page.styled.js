import styled from '@xstyled/styled-components'

export const StyledPage = styled.div`
  margin: ${props => (props.margin === 'none' ? '0' : '0 -lg')};
  padding: lg 0;
`
