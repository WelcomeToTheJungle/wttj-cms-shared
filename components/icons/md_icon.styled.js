import styled, { css, keyframes } from '@xstyled/styled-components'

import { color } from '../../utils/theme-helpers'

const spin = keyframes`
  from {
    transform:rotate(0deg);
  }
  to {
    transform:rotate(360deg);
  }
`

const spinRule = css`
  ${spin} 1s ease infinite;
`

const getColor = props => {
  if (!props.color) {
    return 'inherit'
  }

  const colors = Array.isArray(props.color) ? props.color : props.color.split('.')
  return color(...colors)(props)
}

export const spinStyles = css`
  animation: ${spinRule};
`

export const StyledIcon = styled.i`
  color: ${getColor};

  font-size: ${props => (props.large ? '1.5em' : '1em')};
  vertical-align: middle;

  ${props => (props.spin ? spinStyles : null)};

  &:not(:only-child) {
    margin-right: 5px;
  }
`

export const IconWrapper = styled.div`
  background: ${color('gray', 100)};
  color: ${color('texts', 'light')};
  border: 1px solid ${color('borders')};
  border-radius: 50%;
  width: 60px;
  height: 60px;
  text-align: center;
  margin-right: md;

  i {
    line-height: 60px;
  }
`
