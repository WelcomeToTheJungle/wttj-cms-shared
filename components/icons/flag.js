import React from 'react'
import { FlagCsIcon } from '@welcome-ui/icons.flag_cs'
import { FlagEnIcon } from '@welcome-ui/icons.flag_en'
import { FlagEsIcon } from '@welcome-ui/icons.flag_es'
import { FlagFrIcon } from '@welcome-ui/icons.flag_fr'
import { FlagSkIcon } from '@welcome-ui/icons.flag_sk'

export const FLAGS = {
  cs: FlagCsIcon,
  en: FlagEnIcon,
  sk: FlagSkIcon,
  es: FlagEsIcon,
  fr: FlagFrIcon,
}

export const Flag = ({ locale, ...props }) => {
  const Flag = FLAGS[locale]
  if (!Flag) {
    return null
  }

  return <Flag {...props} />
}
