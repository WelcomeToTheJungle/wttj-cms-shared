import React, { PureComponent } from 'react'
import { array, bool, oneOfType, string } from 'prop-types'

import { StyledIcon } from './md_icon.styled'

export default class MaterialDesignIcon extends PureComponent {
  static propTypes = {
    symbol: string.isRequired,
    /** String can be normal string like `'blue'` or theme color accessor like `'blue.400'` */
    color: oneOfType([array, string]),
    large: bool,
  }

  render() {
    const { symbol, ...rest } = this.props
    return <StyledIcon className={`zmdi zmdi-${symbol}`} {...rest} />
  }
}
