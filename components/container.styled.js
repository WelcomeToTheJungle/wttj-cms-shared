import styled, { css, keyframes } from '@xstyled/styled-components'
import { color, radius } from '../utils/theme-helpers'
import { media } from './utils/utils.styled'

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`

const fadeInRule = css`
  ${fadeIn} 1.5s;
`

const ContainerWrapper = styled.div`
  position: relative;
  display: block;

  .hide {
    opacity: 0;
  }

  .show {
    animation: ${fadeInRule};
  }

  ${props =>
    props.editable &&
    css`
      border: 1px dashed ${color('gray', 150)};
      border-radius: ${radius('md')};
      min-height: xxl;
      margin-top: xxl;
    `};

  // Set width to 100% if single column
  // Use high-specificity to override normal block width
  .packery-container {
    .block {
      width: ${({ columns }) => (columns === '1' ? '100%' : null)};
    }
  }

  ${media.mobile`
    flex: 1 1 100%;
  `};

  > div {
    flex: 1 1 auto;
  }
`

export default ContainerWrapper
