import styled, { th } from '@xstyled/styled-components'
import { color } from '../../utils/theme-helpers'
import { PageNavBreadcrumbLink, NavCompanyLogo, PageNavLink } from './nav.styled'
import { loadingStyles } from '../loading.styled'

export const NavCompanyLogoLoading = styled(NavCompanyLogo)`
  background-color: ${color('gray', '150')};
  ${loadingStyles};
  position: absolute;
  z-index: 101;
  .sticky & {
    transform: translateY(-100%);
  }
`

const PageNavBreadcrumbText = PageNavBreadcrumbLink.withComponent('div')

export const PageNavBreadcrumbLinkLoading = styled(PageNavBreadcrumbText)`
  width: calc(${th('space.6xl')} * 2);
  cursor: default;
`

const PageNavText = PageNavLink.withComponent('div')

export const PageNavLinkLoading = styled(PageNavText)`
  width: ${th('space.6xl')};
`
