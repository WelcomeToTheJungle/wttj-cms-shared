import styled, { css, th } from '@xstyled/styled-components'
import { bool, oneOf, string } from 'prop-types'
import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import { CenteredContainer } from '../layouts/layouts.styled'
import { media, overflowEllipsis, getBackgroundImage, scrollbar } from '../utils/utils.styled'
import {
  color,
  fontSize,
  fontWeight,
  transition,
  pageNavHeight,
  rgba,
  imagesHeight,
} from '../../utils/theme-helpers'

const pageNavTabsModeStyles = css`
  text-transform: uppercase;
  font-size: ${fontSize('sm')};
`

const getPageNavTabsModeStyles = mode => {
  switch (mode) {
    case 'uppercase':
      return pageNavTabsModeStyles
    default:
      return null
  }
}

export const Content = styled.div(
  ({ backgroundColor }) => css`
    z-index: 99;
    background-color: inherit;
    height: ${pageNavHeight()};
    transition: medium;
    background-color: ${backgroundColor || 'nude.200'};
    backface-visibility: hidden;

    ${CenteredContainer} {
      position: relative;
      display: flex;
      align-items: center;
      height: 100%;
    }
  `
)

export const PageNavListWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: ${({ centered }) => (centered ? 'center' : 'space-between')};
  flex-grow: 1;
  height: 100%;
  margin: 0;
  padding: 0;
  ${props => `${scrollbar('4px', color('borders')(props), color('white')(props))}`};

  ${media.mobile`
    text-align: center;
    max-width: 100vw;
    flex: 1 1 auto;
    margin: 0 -lg;
  `};
`

export const PageNavList = styled.ul`
  position: relative;
  z-index: 3;
  display: inline-flex;
  justify-content: flex-start;
  height: 100%;

  &:hover {
    z-index: 200;
  }

  ${media.mobile`
    overflow: auto;
    -webkit-overflow-scrolling: touch;
  `};
`

export const PageNavItemSubnav = styled.nav`
  position: absolute;
  top: 100%;
  left: 0;
  width: 200;
  background-color: light.900;
  border: 1px solid;
  border-color: light.800;
  font-size: body3;
  opacity: 0;
  visibility: hidden;
  transform: translateY(10px);
  transition: medium;

  ${media.mobile`
    display: none;
  `};
`

export const PageNavLinkIcon = styled.span`
  margin-left: xs;
  line-height: 0;

  svg {
    transition: medium;
  }

  ${media.mobile`
    display: none;
  `};
`

export const PageNavItem = styled.li`
  position: relative;
  margin-right: md;
  padding: 0 md;
  min-width: auto;

  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    padding-right: 0;
  }

  ${media.mobile`
    margin: 0;
    padding: 0 xs;

    &:first-child {
      padding-left: lg;
    }

    &:last-child {
      padding-right: lg;
    }
  `};

  &:hover {
    ${PageNavItemSubnav} {
      opacity: 1;
      visibility: visible;
      transform: translateX(0);
    }

    ${PageNavLinkIcon} {
      svg {
        transform: rotate(180deg);
      }
    }
  }
`

export const PageNavTabs = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  justify-content: center;
  height: 100%;
  font-size: ${fontSize('md')};
  ${props => props.mode && getPageNavTabsModeStyles(props.mode)};
`

PageNavTabs.propTypes = {
  mode: oneOf(['uppercase']),
}

const pageNavTabActiveStyles = css`
  color: dark.900;

  &::after {
    background-color: primary.500;
    opacity: 1;
    transform: translateX(0);
  }
`

const pageNavLinkStyles = css`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  padding: 0;
  border: none;
  background: none;
  ${th('texts.body2')};
  font-weight: medium;
  color: nude.800;
  outline: none;
  cursor: pointer;
  transition: medium;
  ${overflowEllipsis};

  &::after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 3px;
    background-color: light.100;
    transition: medium;
    opacity: 0;
    transform: translateX(-10px);
  }

  &:hover {
    &::after {
      opacity: 1;
      transform: translateX(0);
    }
  }
`

export const PageNavLink = styled(NavLink)`
  ${pageNavLinkStyles};
  .tab-link-active &,
  &.active {
    ${pageNavTabActiveStyles};
  }
`

export const Tab = PageNavLink.withComponent('div')

export const PageLink = styled(Link)`
  ${pageNavLinkStyles} &.active {
    ${pageNavTabActiveStyles};
  }
`

export const PageNavLinkInfo = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 20px;
  height: 20px;
  margin-left: xs;
  border-radius: 50%;
  background-color: primary.500;
  ${th('texts.meta2')};
  font-family: headings;
  font-weight: medium;
  color: dark.900;
`

export const PageNavTab = styled.button`
  ${pageNavLinkStyles};
  margin: 0 lg;

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    margin-right: 0;
  }

  &.active {
    ${pageNavTabActiveStyles};
  }
`

export const PageNavBreadcrumb = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  height: 100%;
  max-width: 100%;
  overflow: hidden;

  & + ${PageNavTabs} {
    margin-left: auto;

    ${media.mobile`
      margin-left: 0;
      flex: 1 1 auto;
    `};
  }

  ${media.mobile`
    display: none;
  `};
`

export const PageNavBreadcrumbLink = styled(Link)`
  position: relative;
  flex: 0 1 auto;
  display: flex;
  align-items: center;
  height: 100%;
  margin-right: lg;
  padding-right: lg;
  text-transform: uppercase;
  cursor: pointer;
  font-size: ${fontSize('sm')};
  color: ${color('texts', 'light')};
  font-weight: ${fontWeight('bold')};
  transition: ${transition('md')};

  ${media.mobile`
    margin-right: md;
    padding-right: md;
  `};

  span {
    display: inline-block;
    white-space: nowrap;
  }

  &:hover {
    color: ${rgba('dark', '900', 0.75)};
  }

  &::after {
    display: block;
    position: absolute;
    right: 0;
    top: 50%;
    width: 6px;
    height: 6px;
    margin-left: lg;
    border-top: 1px solid ${color('texts', 'light')};
    border-right: 1px solid ${color('texts', 'light')};
    transform: rotate(45deg) translateY(calc(-50% - 1px));
    content: ' ';
  }

  &:last-child {
    flex-shrink: 0.5;
    color: ${color('white')};

    span {
      ${overflowEllipsis};
    }

    &::after {
      display: none;
    }
  }

  &:first-child {
    ${media.mobile`
      display: none;
    `};
  }
`

PageNavBreadcrumb.propTypes = {
  background: string,
}

export const PageShare = styled.div`
  display: flex;
  align-items: center;
  margin-left: auto;

  ${media.mobile`
    display: none;
  `};
`

export const PageShareLabel = styled.p`
  margin-left: auto;
  font-size: ${fontSize('sm')};
  text-transform: uppercase;
  font-weight: ${fontWeight('bold')};
  color: ${color('gray', '350')};
  margin-right: xxl;

  ${media.tablet`
    display: none;
  `};
`

export const NavCompanyWrapper = styled.div`
  position: relative;
  width: calc(${imagesHeight('organizations', 'show', 'logo')} + ${th('space.xxl')});
  height: 100%;
  overflow: hidden;
  padding-right: xxl;

  ${media.mobile`
    display: none;
  `};
`

export const NavCompanyLogo = styled.div`
  position: absolute;
  left: ${th('space.xxl')};
  bottom: calc(${th('space.xxl')} * -1);
  width: ${imagesHeight('organizations', 'show', 'logo')};
  height: ${imagesHeight('organizations', 'show', 'logo')};
  border: 1px solid;
  border-color: light.800;
  ${props => getBackgroundImage(props.image)};
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-color: light.900;
  transition: medium;
  z-index: 1;

  ${media.mobile`
    display: none;
  `};
`

NavCompanyLogo.propTypes = {
  image: string,
}

export const NavCompanyDetails = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  transition: medium;
`

export const NavCompanyDetailsLogoFrame = styled.div`
  flex: 0 0 auto;
  align-self: stretch;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: md;
  padding: xxs;
  border-right: 1px solid;
  border-left: 1px solid;
  border-color: light.800;
`

export const NavCompanyDetailsLogo = styled.div`
  width: 3rem;
  height: 3rem;
  ${props => getBackgroundImage(props.image)};
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`

export const NavCompanyDetailsName = styled.h2`
  flex: 1 1 auto;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 3;
  ${th('texts.body3')};
  color: dark.900;
  overflow: hidden;
`

/** @component */
export const Nav = styled.nav(
  ({ sticky, isHeaderSticky }) => css`
    height: ${th('pageNavHeight')};
    min-height: auto;
    position: relative;
    flex: 0 0 auto;
    background-color: light.900;

    ${NavCompanyDetails} {
      backface-visibility: hidden;
      transform: ${sticky ? 'translateY(0)' : 'translateY(100%)'};
    }

    ${NavCompanyLogo} {
      backface-visibility: hidden;
      transform: ${sticky ? 'translateY(-100%)' : 'translateY(0)'};
    }

    ${Content} {
      ${sticky &&
      css`
        position: fixed;
        top: ${th('header.desktop.sticky.height')};
        left: 0;
        width: 100%;

        ${media.mobile`
          top: ${th('header.mobile.height')};
        `};
      `};
    }

    ${PageNavItem}, ${PageNavBreadcrumbLink} {
      color: dark.900;
    }

    ${media.mobile`
      &::before, &::after {
        content: '';
        position: absolute;
        top: 0;
        z-index: 100;
        width: ${th('space.lg')};
        height: 100%;
      }

      &::before {
        left: 0;
        background: linear-gradient(to right, ${th('colors.nude.200')}, ${rgba('light', '900', 0)});
      }

      &::after {
        right: 0;
        background: linear-gradient(to left, ${th('colors.nude.200')}, ${rgba('light', '900', 0)});
      }
    `};
  `
)

Nav.propTypes = {
  sticky: bool,
}

export const PageNavItemSubnavList = styled.ul`
  ${PageNavItem} {
    margin: 0;
    padding: 0;
  }

  ${PageNavLink} {
    display: block;
    padding: xs md;
    text-align: left;
    font-weight: normal;
    color: light.100;

    &::after {
      display: none;
    }

    &:hover {
      background-color: nude.200;
      color: dark.900;
    }

    &.active {
      font-weight: medium;
      color: dark.900;
    }
  }
`

export const PageSubnav = styled.nav`
  position: relative;
  display: flex;
  margin: 0 lg lg;
  padding-bottom: 2px;

  &::after {
    position: absolute;
    bottom: 1px;
    left: 0;
    width: 100%;
    height: 1px;
    z-index: 1;
    content: ' ';
    background-color: ${color('borders')};
  }

  ${media.mobile`
    justify-content: center;
    overflow: auto;
  `};
`

export const PageSubnavList = styled.ul`
  display: flex;
`

export const PageSubnavListItemIcon = styled.span`
  margin-left: md;
  color: light.100;
`

export const PageSubnavListItem = styled.li`
  position: relative;
  z-index: 2;
  display: flex;
  align-items: center;
  padding: 0 sm;
  flex-shrink: 0;

  &::after {
    content: '';
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    height: 20px;
    width: 1px;
    background-color: light.800;
  }

  &:first-child,
  &:last-child {
    &::after {
      content: none;
    }
  }

  &:last-child {
    margin-right: 0;
  }
`

export const PageSubnavLink = styled(NavLink)`
  position: relative;
  display: block;
  padding: md 0;
  ${th('texts.body2')};
  font-weight: medium;
  color: light.100;
  transition: medium;

  &::after {
    content: '';
    position: absolute;
    left: 0;
    bottom: -2px;
    width: 100%;
    height: 3px;
    background-color: primary.500;
    transition: medium;
    opacity: 0;
    transform: translateX(-10px);
  }

  &:hover,
  &.active {
    color: dark.900;

    &::after {
      background-color: primary.500;
      opacity: 1;
      transform: translateX(0);
    }
  }
`

export const PageNavAction = styled.div`
  display: flex;
  flex-shrink: 0;
  align-items: center;
  height: 100%;
  background-color: nude.200;
  z-index: 3;

  ${media.tablet`
    padding: 0 md;
    border-left: 1px solid;
    border-left-color: light.800;
  `};
`
