import React, { PureComponent } from 'react'

import Icon from '../icons/md_icon'

const statuses = {
  draft: {
    symbol: 'file-text',
    color: 'gray.200',
  },
  pending: {
    symbol: 'time',
    color: 'orange.200',
  },
  published: {
    symbol: 'check-circle',
    color: 'primary',
  },
}

class PageStatusIcon extends PureComponent {
  render() {
    const { status } = this.props
    const pageStatus = statuses[status]
    const symbol = pageStatus ? pageStatus.symbol : 'circle'
    const color = pageStatus ? pageStatus.color : 'gray.200'

    return <Icon symbol={symbol} color={color} />
  }
}

export default PageStatusIcon
