import React, { Component } from 'react'
import { bool, number, string, node } from 'prop-types'
import { list, listOf, mapContains } from 'react-immutable-proptypes'
import kebabCase from 'lodash/kebabCase'
import { Icons } from '@welcome-ui/icons.font'

import {
  PageNavAction,
  PageNavListWrapper,
  PageNavList,
  PageNavItem,
  PageNavLink,
  PageNavLinkIcon,
  PageNavLinkInfo,
  PageNavItemSubnav,
  PageNavItemSubnavList,
} from './pages/nav.styled'
import PageStatusIcon from './pages/page_status_icon.js'

class PageItem extends Component {
  getItemsCount() {
    const { page } = this.props
    if (Number.isInteger(page.get('items_count'))) {
      return page.get('items_count') > 99 ? '99+' : page.get('items_count')
    }
    return null
  }

  isVisible() {
    const { page } = this.props
    if (page.get('reference') === 'meetings' && page.get('items_count') === 0) {
      return false
    }
    return true
  }

  render() {
    const { page, pages, level, showStatus } = this.props

    if (!this.isVisible()) {
      return null
    }

    const itemsCount = this.getItemsCount()
    let children = page.get('children')
    children = children && children.size > 0 && children.sortBy(p => p.get('position'))
    const className = page.get('isMatch') ? 'active' : ''
    const testId = `organization-nav-${level > 0 && 'sub'}link-${kebabCase(page.get('name'))}`

    return (
      <PageNavItem>
        <PageNavLink
          data-testid={testId}
          to={page.get('link')}
          className={className}
          exact={page.get('exactMatch')}
        >
          {showStatus && <PageStatusIcon status={page.get('status')} />}
          <span>{page.get('name')}</span>
          {itemsCount !== null && <PageNavLinkInfo>{itemsCount}</PageNavLinkInfo>}
          {children && (
            <PageNavLinkIcon>
              <Icons.Down />
            </PageNavLinkIcon>
          )}
        </PageNavLink>
        {children && (
          <PageNavItemSubnav>
            <PageNavItemSubnavList>
              {children.map(child => (
                <PageItem
                  key={child.get('id')}
                  page={child}
                  pages={pages}
                  level={level + 1}
                  showStatus={showStatus}
                />
              ))}
            </PageNavItemSubnavList>
          </PageNavItemSubnav>
        )}
      </PageNavItem>
    )
  }
}

class CmsNavigation extends Component {
  rootPages() {
    const { pages } = this.props
    return pages.filter(p => !p.get('parent_id')).sortBy(p => p.get('position'))
  }

  render() {
    const { actions, pages, showStatus } = this.props

    if (!pages) {
      return null
    }

    return (
      <PageNavListWrapper>
        <PageNavList data-testid="organization-nav-items">
          {this.rootPages().map(page => (
            <PageItem
              key={page.get('id')}
              page={page}
              pages={pages}
              level={0}
              showStatus={showStatus}
              actions={actions}
            />
          ))}
        </PageNavList>
        {actions && <PageNavAction>{actions}</PageNavAction>}
      </PageNavListWrapper>
    )
  }
}

CmsNavigation.propTypes = {
  pages: listOf(
    mapContains({
      id: number.isRequired,
      name: string.isRequired,
      link: string.isRequired,
      slug: string,
      position: number,
      items_count: number,
      children: list,
    })
  ),
  showStatus: bool,
  actions: node,
}

export default CmsNavigation
