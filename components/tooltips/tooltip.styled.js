import styled, { css, th } from '@xstyled/styled-components'
import { color, fontSize, radius, transition } from 'wttj-cms-shared/utils/theme-helpers'

const tooltipAlignCenterStyles = css`
  left: 50%;
  transform: translateX(-50%);

  &::before {
    left: 50%;
    margin-left: -3px;
  }
`

const tooltipAlignLeftStyles = css`
  left: 0;

  &::before {
    left: 15px;
  }
`

const tooltipAlignLeftBottomStyles = css`
  left: 0;
  bottom: auto;
  top: calc(100% + ${th('space.xs')});

  &::before {
    left: 15px;
    top: -3px;
    bottom: auto;
  }
`

const tooltipAlignRightStyles = css`
  right: -10px;

  &::before {
    right: 15px;
  }
`

const getTooltipPosition = position => {
  switch (position) {
    case 'left':
      return tooltipAlignLeftStyles
    case 'left-bottom':
      return tooltipAlignLeftBottomStyles
    case 'right':
      return tooltipAlignRightStyles
    case 'center':
      return tooltipAlignCenterStyles
    default:
      return tooltipAlignCenterStyles
  }
}

export const Tooltip = styled.span`
  position: absolute;
  bottom: calc(100% + ${th('space.xs')});
  background-color: ${color('gray', '400')};
  border-radius: ${radius('md')};
  padding: xxs xs;
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('sm')};
  color: ${color('white')};
  z-index: 10;
  opacity: 0;
  transition: ${transition('sm')};
  visibility: hidden;
  white-space: nowrap;

  &::before {
    position: absolute;
    bottom: -3px;
    width: 6px;
    height: 6px;
    transform: rotate(45deg);
    background-color: inherit;
    content: ' ';
  }

  ${props => getTooltipPosition(props.position)};
`

export const withTooltipStyles = css`
  position: relative;

  &:hover {
    ${Tooltip} {
      opacity: 1;
      visibility: visible;
    }
  }
`
