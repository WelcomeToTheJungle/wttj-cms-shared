import styled, { css, th } from '@xstyled/styled-components'
import { Text } from '@welcome-ui/text'
import { color, fontSize } from '../utils/theme-helpers'

const cardTheme = css`
  padding: lg;
  border: 1px solid;
  border-color: light.800;
  background: light.900;
`

const defaultTheme = css``

export const themes = {
  card: cardTheme,
  default: defaultTheme,
}

export const Content = styled.article`
  position: relative;

  ${({ theme }) => themes[theme] || themes.default}
`

export const ContentWrapper = styled.div``

const contentHeaderBorder = css`
  border-bottom: 1px solid;
  border-bottom-color: light.800;
`

const contentHeaderBorderShort = css`
  &::after {
    content: ' ';
    height: 1px;
    background: ${color('gray', 150)};
    position: absolute;
    bottom: 0;
    left: ${th('space.lg')};
    width: ${th('space.5xl')};
  }
`

export const ContentHeader = styled.header`
  position: relative;
  padding: xl;

  ${props => (props.short ? contentHeaderBorderShort : contentHeaderBorder)};
`

export const ContentTitle = styled(Text).attrs({ variant: 'h4' })``

export const ContentContent = styled.div`
  padding: lg;
  ${th('texts.body2')};

  .company-stats & {
    padding: 0;
  }
`

export const ContentContentP = styled.p`
  ${th('texts.body2')};
  color: dark.200;
`

export const ContentFooter = styled.footer`
  padding: lg;
  border-top: 1px solid ${color('gray', '150')};
`

export const ContentFooterP = styled.p`
  font-size: ${fontSize('sm')};
  color: ${color('texts', 'xlight')};
`

export const ContentIcon = styled.div`
  position: absolute;
  top: 1rem;
  right: 1rem;
`

export const ContentLink = styled.a`
  position: relative;
  display: block;
`
