import { expect } from 'chai'
import difference from 'lodash/difference'
import { flattenMessages } from '../locales/utils'

export const compareTranslationFiles = (localeA, localeB) => {
  describe('Translation files', () => {
    const diff = difference(
      Object.keys(flattenMessages(localeA)),
      Object.keys(flattenMessages(localeB))
    )
    const message = JSON.stringify(diff, null, 2)

    it('have same keys', () => {
      // eslint-disable-next-line no-unused-expressions
      expect(diff, message).to.be.empty
    })
  })
}
