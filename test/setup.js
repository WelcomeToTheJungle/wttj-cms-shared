import Adapter from 'enzyme-adapter-react-16'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import { configure } from 'enzyme'
import 'jest-styled-components'
import '@testing-library/jest-dom'

configure({ adapter: new Adapter() })
chai.use(chaiEnzyme())

// https://github.com/testing-library/dom-testing-library/issues/477#issuecomment-598731998
import MutationObserver from '@sheerun/mutationobserver-shim'
window.MutationObserver = MutationObserver

// https://medium.com/@RubenOostinga/combining-chai-and-jest-matchers-d12d1ffd0303
const originalExpect = global.expect
const originalNot = Object.getOwnPropertyDescriptor(chai.Assertion.prototype, 'not').get
Object.defineProperty(chai.Assertion.prototype, 'not', {
  get() {
    Object.assign(this, this.assignedNot)
    return originalNot.apply(this)
  },
  set(newNot) {
    this.assignedNot = newNot
    return newNot
  },
})

global.open = jest.fn()

global.expect = actual => {
  const originalMatchers = originalExpect(actual)
  const chaiMatchers = chai.expect(actual)
  const combinedMatchers = Object.assign(chaiMatchers, originalMatchers)
  return combinedMatchers
}
