import React from 'react'
import { render as testRender } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { IntlProvider } from 'react-intl'

import { flattenMessages } from '../locales/utils'
import translations from '../locales/fr'

const AllTheProviders = ({ children, options = {} }) => {
  const messages = flattenMessages(options.translations || { cms: translations })

  return (
    <IntlProvider locale="en" messages={messages} textComponent="span">
      <MemoryRouter context={{}}>{children}</MemoryRouter>
    </IntlProvider>
  )
}

export const render = (ui, options) => {
  return testRender(ui, {
    wrapper: () => <AllTheProviders options={options}>{ui}</AllTheProviders>,
    ...options,
  })
}

// Re-export everything
export * from '@testing-library/react'
