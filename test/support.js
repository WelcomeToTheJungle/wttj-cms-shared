import React from 'react'
import { IntlProvider } from 'react-intl'
import { StaticRouter } from 'react-router-dom'
import { ThemeProvider } from '@xstyled/styled-components'
import renderer from 'react-test-renderer'
import { shallow as enzymeShallow, mount as enzymeMount } from 'enzyme'

import theme from '../constants/theme.styled'
import en from '../locales/en'
import { flattenMessages } from '../locales/utils'

const locale = 'en'
const messages = flattenMessages({ cms: en })

export const shallow = node => {
  return enzymeShallow(node, { wrappingComponent: Wrapper })
}

export const mount = node => {
  return enzymeMount(node, { wrappingComponent: Wrapper })
}

export const createRenderer = children => {
  return renderer.create(<Wrapper>{children}</Wrapper>)
}

const Wrapper = ({ children }) => (
  <IntlProvider locale={locale} messages={messages}>
    <ThemeProvider theme={theme}>
      <StaticRouter context={{}}>{children}</StaticRouter>
    </ThemeProvider>
  </IntlProvider>
)
