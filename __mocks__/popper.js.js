const PopperJS = jest.requireActual('@popperjs/core')

export default class {
  static placements = PopperJS.placements

  constructor() {
    return {
      destroy: () => {},
      scheduleUpdate: () => {},
    }
  }
}
