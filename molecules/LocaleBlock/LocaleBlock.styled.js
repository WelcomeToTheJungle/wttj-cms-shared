import styled, { css } from '@xstyled/styled-components'

import Button from '../../components/buttons/button.styled'
import { color, padding, radius, rgba, textStyles, transition } from '../../utils/theme-helpers'
import { Tab } from '../../components/pages/nav.styled'
import { media } from '../../components/utils/utils.styled'

const getHoverModeStyles = ({ hoverMode }) => {
  if (hoverMode === 'jungle-select-item') {
    return css`
      background-color: ${rgba('primary', '500', 0.05)};
      border-radius: ${radius('md')};
    `
  }
  return null
}

export const LocaleActions = styled.div`
  position: absolute;
  right: 0;
  display: flex;
  transition: ${transition('md')};

  ${Button} {
    margin-left: lg;
  }
`

export const LocaleDefault = styled.span`
  ${textStyles('secondary')};
  color: ${color('gray', 600)};
`

export const LocaleTitle = styled.p`
  ${textStyles('button')};

  ${LocaleDefault} {
    margin-left: xs;
  }

  svg {
    margin-right: md;
    ${media.mobile`
      margin-right: xxs;
    `};
  }
`

export const LocaleBlock = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding: ${props => (props.padding ? padding(props.padding)(props) : null)};

  &:hover {
    ${getHoverModeStyles};
  }

  ${Tab} & {
    opacity: 0.6;
    transition: ${transition('md')};
  }

  .tab-link-active &,
  ${Tab}:hover & {
    opacity: 1;
  }
`

export default LocaleBlock
