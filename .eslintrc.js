module.exports = {
  extends: ['plugin:prettier/recommended', 'plugin:react/recommended'],
  plugins: ['react', 'jsx-a11y'],
  settings: {
    react: {
      version: '16.8.6',
    },
  },
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 6,
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'prettier/prettier': 'error',
    'jsx-a11y/href-no-hash': 'off',
    'react/prop-types': 'off',
    'react/display-name': 'off',
    'react/jsx-no-target-blank': 'off',
    'react/no-unsafe': 'error',
    'react/no-deprecated': 'warn',
  },
}
