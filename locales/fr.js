export default {
  languages: {
    de: 'Allemand',
    en: 'Anglais',
    ar: 'Arabe',
    es: 'Espagnol',
    zh: 'Chinois',
    ko: 'Coréen',
    hr: 'Croate',
    da: 'Danois',
    fr: 'Français',
    hu: 'Hongrois',
    he: 'Hébreu',
    it: 'Italien',
    ja: 'Japonais',
    ms: 'Malais',
    no: 'Norvégien',
    pl: 'Polonais',
    pt: 'Portugais',
    ro: 'Roumain',
    ru: 'Russe',
    sr: 'Serbe',
    sk: 'Slovaque',
    sl: 'Slovène',
    sv: 'Suédois',
    cs: 'Tchèque',
    th: 'Thaï',
    tr: 'Turc',
    uk: 'Ukrainien',
    fi: 'Finnois',
    id: 'Indonésien',
    nl: 'Néerlandais',
  },
  quotes: {
    beginning: '“',
    ending: '”',
  },
  articles: {
    userName: 'Par {name}',
  },
  contents: {
    text: {
      topics: {
        description: 'Présentation',
        good_to_know: 'Bon à savoir',
        looking_for: "Ce qu'ils recherchent",
        working_for: 'Positionnement',
      },
    },
    'company-stats': {
      creation_year: 'Année de création',
      nb_employees: 'Collaborateurs',
      sectors: 'Secteurs',
      location: 'Localisation',
      parity: 'Parité',
      average_age: 'Âge moyen',
      average_age_unit: ' ans',
      turnover: 'Turnover',
      turnover_unit: ' %',
      revenue: "Chiffre d'affaires",
    },
    jobs: {
      title: 'Derniers jobs',
      'all-jobs': 'Toutes nos offres',
      'no-jobs': "Aucune offre en ce moment, revenez d'ici quelques jours !",
    },
    'jobs-search': {
      'no-results': "Il n'y a pas d'offre correspondant à votre recherche…",
      'no-jobs':
        "{organizationName} ne propose malheureusement pas d'offre en ce moment ! Revenez dans quelques jours !",
      spontaneous: {
        title: 'Vous ne trouvez pas votre bonheur ?',
        subtitle:
          "{organizationName} est toujours à la recherche de personnes talentueuses, n'hésitez pas à postuler !",
        link: 'Postuler',
      },
    },
    'meetings-search': {
      'no-results': "Il n'y a pas d'événements correspondant à votre recherche…",
      'no-meetings': "{organizationName} ne propose malheureusement pas d'événements en ce moment…",
      meanwhile: "En attendant, n'hésitez pas à parcourir les événements d'autres entreprises :",
      'browse-all-meetings': 'Parcourir tous les événements',
    },
    articles: {
      title: 'Apprenez-en plus sur {organizationName}',
      userName: 'Par {name}',
    },
    'social-networks': {
      title: 'Ils sont sociables',
    },
    'tech-stack': {
      'swiper-controls': {
        previous: 'Précédent',
        next: 'Suivant',
      },
    },
    video: {
      'image-missing': 'Image en cours de récupération',
    },
    'organization-embed': {
      title: 'Intégrer ce profil sur votre site',
      button: 'Intégrer le widget',
      modal: {
        title: 'Tous les contenus {organizationName} sur votre site',
      },
      embed: {
        title: 'Récupérez votre widget',
      },
      preview: {
        title: 'Prévisualisation',
      },
      form: {
        title: 'Configurez votre widget',
        labels: {
          subtitle: "Copiez/Collez le code suivant à l'emplacement souhaité",
          widthValue: 'Largeur du widget',
          heightValue: 'Hauteur du widget',
          lines: 'Nombre de lignes',
          columns: 'Nombre de colonnes',
          language: 'Langue',
        },
        errors: {
          width: {
            larger: 'La largeur doit être inférieure à {value}{unit}',
            smaller: 'La largeur doit être supérieure à {value}{unit}',
          },
          height: {
            larger: 'La hauteur doit être inférieure à {value}{unit}',
            smaller: 'La hauteur doit être supérieure à {value}{unit}',
          },
          lines: {
            larger: 'Le nombre de lignes doit être inférieur à {value}',
            smaller: 'Le nombre de lignes doit être supérieur à {value}',
          },
          columns: {
            larger: 'Le nombre de colonnes doit être inférieur à {value}',
            smaller: 'Le nombre de colonnes doit être supérieur à {value}',
          },
        },
        submit: 'Valider',
      },
    },
    date: {
      published: 'Publié',
    },
    values: {
      title: 'Leurs valeurs',
    },
  },
  search: {
    common: {
      'range-slider': {
        label: {
          year: '{count} {count, plural, one {an} other {ans}}',
        },
      },
      searchBox: {
        placeholder: 'Votre recherche...',
        submitTitle: 'Quelle est votre recherche ?',
        resetTitle: 'Mettre à jour ma recherche',
      },
      location: {
        placeholder: 'Où ?',
        nearby: 'Position actuelle',
        remote: 'En télétravail',
        radius: {
          long: 'à moins de {count} km',
          short: '< {count} km',
        },
        resetTitle: 'Effacer',
      },
      reset: 'Réinitialiser',
    },
    articles: {
      searchBox: {
        placeholder: 'Un sujet en tête ?',
      },
      stats: '{count} {count, plural, one {article} other {articles}} à découvrir',
      widgets: {
        categories: {
          general: {
            title: 'Général',
          },
          tech: {
            title: 'Web & Tech',
          },
          fashion: {
            title: 'Mode & Luxe',
          },
          consulting: {
            title: 'Consulting & Finance',
          },
          food: {
            title: 'Food & Beverage',
          },
        },
      },
    },
    organizations: {
      searchBox: {
        placeholder: 'La tribu de vos rêves ?',
      },
      stats: '{count} {count, plural, one {entreprise} other {entreprises}} à découvrir',
      mobile: {
        stats: '{count} {count, plural, one {entreprise} other {entreprises}}',
      },
      widgets: {
        sectors: {
          title: 'Secteur',
        },
        labels: {
          title: 'Caractéristiques',
        },
        size: {
          title: 'Taille',
        },
        offices: {
          title: 'Localisation',
          district: {
            title: 'Par département',
          },
          country: {
            title: 'Par pays',
          },
        },
        languages: {
          title: 'Langues',
        },
      },
    },
    interviews: {
      searchBox: {
        placeholder: 'Le métier de vos rêves ?',
      },
      stats: '{count} {count, plural, one {métier} other {métiers}} à découvrir',
      widgets: {
        category: {
          name: {
            title: 'Catégories',
          },
        },
      },
    },
    jobs: {
      followedCompanies: {
        stats: '{count} {count, plural, one {job} other {jobs}} depuis mes entreprises suivies',
        mobile: {
          stats: '{count} {count, plural, one {job} other {jobs}}',
        },
      },
      searchBox: {
        placeholder: 'Le job de vos rêves ?',
      },
      stats: '{count} {count, plural, one {job} other {jobs}} pour votre recherche',
      mobile: {
        stats: '{count} {count, plural, one {job} other {jobs}}',
      },
      widgets: {
        sectors: {
          title: 'Secteur',
        },
        contract_type_names: {
          title: 'Contrat',
        },
        office: {
          title: 'Localisation',
          country: {
            title: 'Par pays',
          },
          district: {
            title: 'Par département',
          },
        },
        profession: {
          title: 'Professions',
        },
        other: {
          title: 'Autres',
        },
        organization: {
          name: {
            title: 'Entreprises',
          },
          size: {
            title: 'Taille',
          },
        },
        remote: {
          title: 'Télétravail',
        },
        experience_level_minimum: {
          title: "Niveau d'expérience",
        },
        language: {
          title: 'Langues',
        },
      },
      seen: "Déjà vu"
    },
    stacks: {
      searchBox: {
        placeholder: 'La stack de vos rêves ?',
      },
      stats: '{count} {count, plural, one {stack} other {stacks}} à découvrir',
      widgets: {
        tools: {
          backend: {
            title: 'Backend',
          },
          frontend: {
            title: 'Frontend',
          },
          devops: {
            title: 'DevOps',
          },
          data: {
            title: 'Data Analysis',
          },
          mobile: {
            title: 'Mobile',
          },
          ci: {
            title: 'Continuous Integration',
          },
          pm: {
            title: 'Product Management',
          },
          ide: {
            title: 'IDE',
          },
          monitoring: {
            title: 'Monitoring',
          },
          misc: {
            title: 'Miscellaneous',
          },
        },
        'other-tools': {
          title: 'Autres',
        },
      },
    },
    meetings: {
      searchBox: {
        placeholder: "L'événement de vos rêves ?",
      },
      stats: '{count} {count, plural, one {événement} other {événements}} pour votre recherche',
      widgets: {
        category: {
          name: {
            title: 'Catégorie',
          },
        },
        address: {
          title: 'Localisation',
          district: {
            title: 'Par département',
          },
          country: {
            title: 'Par pays',
          },
        },
        organization: {
          name: {
            title: 'Entreprises',
          },
        },
        language: {
          title: 'Langues',
        },
      },
    },
    applications: {
      searchBox: {
        placeholder: 'Entrez votre recherche',
      },
      stats: '{count} {count, plural, one {candidature} other {candidatures}}',
      widgets: {
        status: {
          title: 'Statut',
        },
      },
      'no-results': "Aucun résultat. Essayez avec d'autres mots clés !",
      empty: {
        title: "Oh, il semblerait que vous n'ayez jamais postulé !",
        'find-organization': 'Trouver une tribu',
        'find-job': 'Trouver un job',
        'if-mistake': "S'il s'agit d'une erreur, ",
        'contact-us': 'contactez-nous !',
      },
    },
  },
  jobs: {
    'contract-types': {
      full_time: 'CDI',
      part_time: 'Temps partiel',
      temporary: 'CDD / Temporaire',
      freelance: 'Freelance',
      internship: 'Stage',
      apprenticeship: 'Alternance',
      vie: 'VIE',
      other: 'Autres',
      volunteer: 'Bénévolat / Service civique',
      idv: 'VDI',
      graduate_program: 'Graduate program',
    },
    remote: {
      fulltime: 'Télétravail total possible',
      partial: 'Télétravail partiel possible',
      punctual: 'Télétravail ponctuel autorisé',
      no: 'Non spécifié',
    },
  },
  videoCookiePlaceholder: {
    title: 'Ce contenu est bloqué',
    subtitle: 'Le cookie {source} est obligatoire pour voir ce contenu',
    button: 'Accepter le cookie',
  },
}
