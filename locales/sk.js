export default {
  languages: {
    de: 'Nemčina',
    en: 'Angličtina',
    ar: 'Arabčina',
    es: 'Španielčina',
    zh: 'Čínština',
    ko: 'Kórejčina',
    hr: 'Chorvátčina',
    da: 'Dánčina',
    fr: 'Francúzština',
    hu: 'Maďarčina',
    he: 'Hebrejčina',
    it: 'Taliančina',
    ja: 'Japončina',
    ms: 'Malajzijčina',
    no: 'Nórčina',
    pl: 'Poľština',
    pt: 'Portugalčina',
    ro: 'Rumunčina',
    ru: 'Ruština',
    sr: 'Srbčina',
    sk: 'Slovenčina',
    sl: 'Slovinčina',
    sv: 'Švédčina',
    cs: 'Čeština',
    th: 'Thajčina',
    tr: 'Turečtina',
    uk: 'Ukrajinčina',
    fi: 'Fínčina',
    id: 'Indonézština',
    nl: 'Holandčina',
  },
  quotes: {
    beginning: '„',
    ending: '“',
  },
  articles: {
    userName: 'Podľa {name}',
  },
  contents: {
    text: {
      topics: {
        description: 'Popis',
        good_to_know: 'Čo by ste mali vedieť',
        looking_for: 'Čo hľadajú',
        working_for: 'Pozícia',
      },
    },
    'company-stats': {
      creation_year: 'Rok založenia',
      nb_employees: 'Zamestnanci',
      parity: 'Zastúpenie pohlaví',
      sectors: 'Secteurs',
      location: 'Localisation',
      average_age: 'Priemerný vek',
      average_age_unit: ' rokov',
      turnover: 'Fluktuácia zamestnancov',
      turnover_unit: '%',
      revenue: 'Obrat',
    },
    jobs: {
      title: 'Najnovšie pracovné ponuky',
      'all-jobs': 'Zobraziť všetky pracovné ponuky',
      'no-jobs': 'V tejto chvíli tu nie sú žiadne ponuky, skúste to znova o pár dní!',
    },
    'jobs-search': {
      'no-results': 'Vášmu vyhľadávaniu nezodpovedajú žiadne pracovné ponuky…',
      'no-jobs':
        'Ľutujeme, v tejto chvíli spoločnosť {organizationName} neponúka žiadne pracovné miesta, skúste to znova o pár dní!',
      spontaneous: {
        title: 'Nenašli ste ponuku, ktorá by vás oslovila?',
        subtitle:
          '{organizationName} neustále hľadá talenty. Neváhajte a prejavte svoj záujem o prácu!',
        link: 'Odoslať všeobecnú žiadosť',
      },
    },
    'meetings-search': {
      'no-results': 'Vášmu vyhľadávaniu nezodpovedajú žiadne podujatia…',
      'no-meetings': 'Ľutujeme, spoločnosť {organizationName} teraz neorganizuje žiadne podujatia…',
      meanwhile: 'Zatiaľ si môžete prezrieť podujatia v iných spoločnostiach:',
      'browse-all-meetings': 'Prehľadávať všetky podujatia',
    },
    articles: {
      title: 'Zistite viac o spoločnosti {organizationName}',
      userName: 'Podľa {name}',
    },
    'social-networks': {
      title: 'Sledujte ich!',
    },
    'tech-stack': {
      'swiper-controls': {
        previous: 'Predchádzajúce',
        next: 'Nasledujúce',
      },
    },
    video: {
      'image-missing': 'Chýba obrázok',
    },
    'organization-embed': {
      title: 'Vložiť tento profil na vašu webovú stránku',
      button: 'Vložiť widget',
      modal: {
        title: 'Všetok obsah spoločnosti {organizationName} na vašej stránke',
      },
      embed: {
        title: 'Získajte widget',
      },
      preview: {
        title: 'Ukážka',
      },
      form: {
        title: 'Nakonfigurujte widget',
        labels: {
          subtitle: 'Skopírujte a vložte tento kód na vašu stránku',
          widthValue: 'Šírka widgetu',
          heightValue: 'Výška widgetu',
          lines: 'Počet riadkov',
          columns: 'Počet stĺpcov',
          language: 'Jazyk',
        },
        errors: {
          width: {
            larger: 'Šírka musí byť menšia ako {value} {unit}',
            smaller: 'Šírka musí byť väčšia ako {value} {unit}',
          },
          height: {
            larger: 'Výška musí byť menšia ako {value} {unit}',
            smaller: 'Výška musí byť väčšia ako {value} {unit}',
          },
          lines: {
            larger: 'Počet riadkov musí byť menší ako {value}',
            smaller: 'Počet riadkov musí byť väčší ako {value}',
          },
          columns: {
            larger: 'Počet stĺpcov musí byť menší ako {value}',
            smaller: 'Počet stĺpcov musí byť väčší ako {value}',
          },
        },
        submit: 'Odoslať',
      },
    },
    date: {
      published: 'Zverejnené',
    },
    values: {
      title: 'Hodnoty spoločnosti',
    },
  },
  search: {
    common: {
      'range-slider': {
        label: {
          year: '{count} {count, plural, one {rok} few {roky} many {rokov} other {rokov}}',
        },
      },
      searchBox: {
        placeholder: 'Vaše vyhľadávanie...',
        submitTitle: 'Čo hľadáte?',
        resetTitle: 'Aktualizovať vyhľadávanie',
      },
      location: {
        placeholder: 'Kde?',
        nearby: 'Aktuálna poloha',
        remote: 'Práca na diaľku',
        radius: {
          long: 'menej ako {count} km',
          short: '< {count} km',
        },
        resetTitle: 'Resetovať',
      },
      reset: 'Resetovať',
    },
    articles: {
      searchBox: {
        placeholder: 'Čo by vás zaujímalo?',
      },
      stats:
        '{count} {count, plural, one {článok} few {články} many {článkov} other {článkov}} na prečítanie',
      widgets: {
        categories: {
          general: {
            title: 'Prehľad',
          },
          tech: {
            title: 'Web a technológie',
          },
          fashion: {
            title: 'Móda a luxus',
          },
          consulting: {
            title: 'Poradenstvo a financie',
          },
          food: {
            title: 'Jedlá a nápoje',
          },
        },
      },
    },
    organizations: {
      searchBox: {
        placeholder: 'Váš vysnívaný pracovný tím?',
      },
      stats:
        '{count} {count, plural, one {spoločnosť} few {spoločnosti} many {spoločností} other {spoločností}} na preskúmanie',
      mobile: {
        stats:
          '{count} {count, plural, one {spoločnosť} few {spoločnosti} many {spoločností} other {spoločností}}',
      },
      widgets: {
        sectors: {
          title: 'Odvetvie',
        },
        labels: {
          title: 'Highlights',
        },
        size: {
          title: 'Veľkosť',
        },
        offices: {
          title: 'Lokalita',
          district: {
            title: 'Podľa oblasti',
          },
          country: {
            title: 'Podľa krajiny',
          },
        },
        languages: {
          title: 'Jazyky',
        },
      },
    },
    interviews: {
      searchBox: {
        placeholder: 'Vaša práca snov?',
      },
      stats:
        '{count} {count, plural, one {pracovná príležitosť} few {pracovné príležitosti} many {pracovných príležitostí} other {pracovných príležitostí}} na preskúmanie',
      widgets: {
        category: {
          name: {
            title: 'Kategórie',
          },
        },
      },
    },
    jobs: {
      followedCompanies: {
        stats:
          '{count} {count, plural, one {pracovná ponuka} few {pracovné ponuky} other {pracovných ponúk}} sledovaných spoločností',
        mobile: {
          stats:
            '{count} {count, plural, one {pracovná ponuka} few {pracovné ponuky} other {pracovných ponúk}}',
        },
      },
      searchBox: {
        placeholder: 'Vaša práca snov?',
      },
      stats:
        'Vyhľadávanie našlo {count} {count, plural, one {pracovnú ponuku} few {pracovné ponuky} many {pracovných ponúk} other {pracovných ponúk}}',
      mobile: {
        stats: '{count} {count, plural, one {ponuku} few {ponuky} many {ponúk} other {ponúk}}',
      },
      widgets: {
        sectors: {
          title: 'Odvetvie',
        },
        contract_type_names: {
          title: 'Druh pracovného pomeru',
        },
        office: {
          title: 'Lokalita',
          country: {
            title: 'Podľa krajiny',
          },
          district: {
            title: 'Podľa oblasti',
          },
        },
        profession: {
          title: 'Oblasť',
        },
        other: {
          title: 'Ďalšie',
        },
        organization: {
          name: {
            title: 'Spoločnosti',
          },
          size: {
            title: 'Veľkosť',
          },
        },
        remote: {
          title: 'Práca na diaľku',
        },
        experience_level_minimum: {
          title: 'Úroveň skúseností',
        },
        language: {
          title: 'Jazyky',
        },
      },
      seen: "Zobrazené"
    },
    stacks: {
      searchBox: {
        placeholder: 'Váš stack snov?',
      },
      stats:
        '{count} {count, plural, one {stack} few {stacky} many {stackov} other {stackov}} na preskúmanie',
      widgets: {
        tools: {
          backend: {
            title: 'Back-end',
          },
          frontend: {
            title: 'Front-end',
          },
          devops: {
            title: 'DevOps',
          },
          data: {
            title: 'Analýza údajov',
          },
          mobile: {
            title: 'Mobilné aplikácie',
          },
          ci: {
            title: 'Continuous Integration',
          },
          pm: {
            title: 'Produktový manažment',
          },
          ide: {
            title: 'IDE',
          },
          monitoring: {
            title: 'Monitoring',
          },
          misc: {
            title: 'Rôzne',
          },
        },
        'other-tools': {
          title: 'Iné',
        },
      },
    },
    meetings: {
      searchBox: {
        placeholder: 'Vaše podujatie snov?',
      },
      stats:
        'Vyhľadávanie našlo {count} {count, plural, one {podujatie} few {podujatia} many {podujatí} other {podujatí}}',
      widgets: {
        category: {
          name: {
            title: 'Kategória',
          },
        },
        address: {
          title: 'Lokalita',
          district: {
            title: 'Podľa oblasti',
          },
          country: {
            title: 'Podľa krajiny',
          },
        },
        organization: {
          name: {
            title: 'Spoločnosti',
          },
        },
        language: {
          title: 'Jazyky',
        },
      },
    },
    applications: {
      searchBox: {
        placeholder: 'Zadajte, čo hľadáte',
      },
      stats:
        '{count} {count, plural, one {reakcia na ponuku} few {reakcie na ponuku} many {reakcií na ponuku} other {reakcií na ponuku}}',
      widgets: {
        status: {
          title: 'Stav',
        },
      },
      'no-results': 'Žiadne výsledky. Skúste to znova s inými kľúčovými slovami!',
      empty: {
        title: 'Zdá sa, že ste na ponuku ešte nereagovali!',
        'find-organization': 'Nájsť spoločnosť',
        'find-job': 'Nájsť prácu',
        'if-mistake': 'Ak sa vyskytla chyba, ',
        'contact-us': 'kontaktujte nás!',
      },
    },
  },
  jobs: {
    'contract-types': {
      full_time: 'Zmluva na dobu neurčitú',
      part_time: 'Čiastočný úväzok',
      temporary: 'Zmluva na dobu určitú',
      freelance: 'Externá spolupráca',
      internship: 'Stáž',
      apprenticeship: 'Odborná prax',
      vie: 'Medzinárodný firemný dobrovoľnícky program',
      volunteer: 'Dobrovoľníctvo',
      idv: 'IDV',
      graduate_program: 'Graduate program',
      other: 'Iné',
    },
    remote: {
      fulltime: 'Možnosť pracovať iba na diaľku',
      partial: 'Možnosť pracovať čiastočne na diaľku',
      punctual: 'Možnosť príležitostne pracovať na diaľku',
      no: 'Neuvedené',
    },
  },
  videoCookiePlaceholder: {
    title: 'Tento obsah je blokovaný',
    subtitle: '{source} cookie je povinný pre zobrazenie tohoto obsahu',
    button: 'Prijať cookie',
  },
}
