export default {
  languages: {
    de: 'Alemán',
    en: 'Inglés',
    ar: 'Árábe',
    es: 'Español',
    zh: 'Chino',
    ko: 'Coreano',
    hr: 'Croata',
    da: 'Danés',
    fr: 'Francés',
    hu: 'Húngaro',
    he: 'Hebreo',
    it: 'Italiano',
    ja: 'Japonés',
    ms: 'Malayo',
    no: 'Noruego',
    pl: 'Polaco',
    pt: 'Portugués',
    ro: 'Rumano',
    ru: 'Ruso',
    sr: 'Serbio',
    sk: 'Eslovaco',
    sl: 'Esloveno',
    sv: 'Sueco',
    cs: 'Checo',
    th: 'Tailandés (thai)',
    tr: 'Turco',
    uk: 'Ukranio',
    fi: 'Finés',
    id: 'Indonesio',
    nl: 'Holandés',
  },
  quotes: {
    beginning: '“',
    ending: '”',
  },
  articles: {
    userName: 'Por {name}',
  },
  contents: {
    text: {
      topics: {
        description: 'Presentación',
        good_to_know: 'Te gustará saber',
        looking_for: 'Lo que buscan',
        working_for: 'Posicionamiento',
      },
    },
    'company-stats': {
      creation_year: 'Año de creación',
      nb_employees: 'Empleados',
      sectors: 'Secteurs',
      location: 'Localisation',
      parity: 'Paridad',
      average_age: 'Media de edad',
      average_age_unit: ' años',
      turnover: 'Rotación de personal',
      turnover_unit: ' %',
      revenue: 'Ingresos',
    },
    jobs: {
      title: 'Últimos empleos',
      'all-jobs': 'Todas nuestras ofertas',
      'no-jobs': 'No hay ofertas en este momento, ¡vuelve dentro de unos días!',
    },
    'jobs-search': {
      'no-results': 'No hay ofertas que coincidan con tu búsqueda…',
      'no-jobs':
        '{organizationName} no tiene ofertas en este momento. ¡Vuelve dentro de unos días!',
      spontaneous: {
        title: '¿No has encontrado el puesto que deseas?',
        subtitle:
          '{organizationName} siempre está buscando a personas con talento, ¡no dudes en enviar tu candidatura!',
        link: 'Envía tu candidatura',
      },
    },
    'meetings-search': {
      'no-results': 'No hay eventos que coincidan con tu búsqueda…',
      'no-meetings': 'Lamentablemente, {organizationName} no ofrece ningún evento en este momento…',
      meanwhile: 'Mientras tanto, no dudes en echar un vistazo a los eventos de otras empresas:',
      'browse-all-meetings': 'Ver todos los eventos',
    },
    articles: {
      title: 'Más información sobre {organizationName}',
      userName: 'Por {name}',
    },
    'social-networks': {
      title: '¡Síguelos!',
    },
    'tech-stack': {
      'swiper-controls': {
        previous: 'Anterior',
        next: 'Siguiente',
      },
    },
    video: {
      'image-missing': 'Recuperando imagen',
    },
    'organization-embed': {
      title: 'Añade este perfil a tu página web',
      button: 'Añadir el widget',
      modal: {
        title: 'Todo el contenido de {organizationName} en tu página web',
      },
      embed: {
        title: 'Obtén tu widget',
      },
      preview: {
        title: 'Vista previa',
      },
      form: {
        title: 'Configura tu widget',
        labels: {
          subtitle: 'Copia el siguiente código y pégalo en tu página web',
          widthValue: 'Ancho del widget',
          heightValue: 'Altura del widget',
          lines: 'Número de líneas',
          columns: 'Número de columnas',
          language: 'Idioma',
        },
        errors: {
          width: {
            larger: 'El ancho debe ser inferior a {value}{unit}',
            smaller: 'El ancho debe ser superior a {value}{unit}',
          },
          height: {
            larger: 'La altura debe ser inferior a {value}{unit}',
            smaller: 'La altura debe ser superior a {value}{unit}',
          },
          lines: {
            larger: 'El número de líneas debe ser inferior a {value}',
            smaller: 'El número de líneas debe ser superior a {value}',
          },
          columns: {
            larger: 'El número de columnas debe ser inferior a {value}',
            smaller: 'El número de columnas debe ser superior a {value}',
          },
        },
        submit: 'Guardar',
      },
    },
    date: {
      published: 'Publicado',
    },
    values: {
      title: 'Sus valores',
    },
  },
  search: {
    common: {
      'range-slider': {
        label: {
          year: '{count} {count, plural, one {año} other {años}}',
        },
      },
      searchBox: {
        placeholder: 'Tu búsqueda...',
        submitTitle: '¿Qué estás buscando?',
        resetTitle: 'Actualizar mi búsqueda',
      },
      location: {
        placeholder: '¿Dónde?',
        nearby: 'Posición actual',
        remote: 'Teletrabajo',
        radius: {
          long: 'menos de {count} km',
          short: '< {count} km',
        },
        resetTitle: 'Reiniciar',
      },
      reset: 'Reiniciar',
    },
    articles: {
      searchBox: {
        placeholder: 'Introduce un tema',
      },
      stats: '{count} {count, plural, one {artículo} other {artículos}} por descubrir',
      widgets: {
        categories: {
          general: {
            title: 'General',
          },
          tech: {
            title: 'Web y Tech',
          },
          fashion: {
            title: 'Moda y Lujo',
          },
          consulting: {
            title: 'Asesoría y Finanzas',
          },
          food: {
            title: 'Alimentación y Bebidas',
          },
        },
      },
    },
    organizations: {
      searchBox: {
        placeholder: '¿La tribu de tus sueños?',
      },
      stats: 'Descubre {count} {count, plural, one {empresa} other {empresas}}',
      mobile: {
        stats: '{count} {count, plural, one {empresa} other {empresas}}',
      },
      widgets: {
        sectors: {
          title: 'Sector',
        },
        labels: {
          title: 'Highlights',
        },
        size: {
          title: 'Tamaño',
        },
        offices: {
          title: 'Ubicación',
          district: {
            title: 'Por provincia',
          },
          country: {
            title: 'Por país',
          },
        },
        languages: {
          title: 'Idiomas',
        },
      },
    },
    interviews: {
      searchBox: {
        placeholder: '¿La profesión de tus sueños?',
      },
      stats: 'Descubre {count} {count, plural, one {profesión} other {profesiones}}',
      widgets: {
        category: {
          name: {
            title: 'Categorías',
          },
        },
      },
    },
    jobs: {
      followedCompanies: {
        stats: '{count} {count, plural, one {oferta} other {ofertas}} desde mis empresas favoritas',
        mobile: {
          stats: '{count} {count, plural, one {oferta} other {ofertas}}',
        },
      },
      searchBox: {
        placeholder: '¿El trabajo de tus sueños?',
      },
      stats: 'Hemos encontrado {count} {count, plural, one {empleo} other {empleos}}',
      mobile: {
        stats: '{count} {count, plural, one {empleo} other {empleos}}',
      },
      widgets: {
        sectors: {
          title: 'Sector',
        },
        contract_type_names: {
          title: 'Contrato',
        },
        office: {
          title: 'Ubicación',
          country: {
            title: 'Por país',
          },
          district: {
            title: 'Por provincia',
          },
        },
        profession: {
          title: 'Profesiones',
        },
        other: {
          title: 'Otros',
        },
        organization: {
          name: {
            title: 'Empresas',
          },
          size: {
            title: 'Tamaño',
          },
        },
        remote: {
          title: 'Teletrabajo',
        },
        experience_level_minimum: {
          title: 'Nivel de experiencia',
        },
        language: {
          title: 'Idiomas',
        },
      },
      seen: "Vista"
    },
    stacks: {
      searchBox: {
        placeholder: '¿El stack de tus sueños?',
      },
      stats: 'Descubre {count} {count, plural, one {stack} other {stacks}}',
      widgets: {
        tools: {
          backend: {
            title: 'Backend',
          },
          frontend: {
            title: 'Frontend',
          },
          devops: {
            title: 'DevOps',
          },
          data: {
            title: 'Análisis de datos',
          },
          mobile: {
            title: 'Apps móviles',
          },
          ci: {
            title: 'Integración continua',
          },
          pm: {
            title: 'Gestión de producto',
          },
          ide: {
            title: 'IDE',
          },
          monitoring: {
            title: 'Monitoring',
          },
          misc: {
            title: 'Varios',
          },
        },
        'other-tools': {
          title: 'Otros',
        },
      },
    },
    meetings: {
      searchBox: {
        placeholder: '¿El evento de tus sueños?',
      },
      stats: 'Hemos encontrado {count} {count, plural, one {evento} other {eventos}}',
      widgets: {
        category: {
          name: {
            title: 'Categoría',
          },
        },
        address: {
          title: 'Ubicación',
          district: {
            title: 'Por provincia',
          },
          country: {
            title: 'Por país',
          },
        },
        organization: {
          name: {
            title: 'Empresas',
          },
        },
        language: {
          title: 'Idiomas',
        },
      },
    },
    applications: {
      searchBox: {
        placeholder: 'Introduce tu búsqueda',
      },
      stats: '{count} {count, plural, one {candidatura} other {candidaturas}}',
      widgets: {
        status: {
          title: 'Estado',
        },
      },
      'no-results':
        'No se han encontrado resultados. ¡Inténtalo de nuevo con otras palabras clave!',
      empty: {
        title: '¡Oh, todavía no has enviado ninguna candidatura!',
        'find-organization': 'Encuentra una tribu',
        'find-job': 'Encuentra un trabajo',
        'if-mistake': 'Si se trata de un error, ',
        'contact-us': '¡ponte en contacto con nosotros!',
      },
    },
  },
  jobs: {
    'contract-types': {
      full_time: 'Indefinido',
      part_time: 'Tiempo parcial',
      temporary: 'Temporal',
      freelance: 'Freelance',
      internship: 'Prácticas',
      apprenticeship: 'Profesionalización',
      vie: 'Voluntariado Internacional en Empresa',
      other: 'Otros',
      volunteer: 'Voluntariado',
      idv: 'IDV',
      graduate_program: 'Graduate program',
    },
    remote: {
      fulltime: 'Teletrabajo a tiempo completo',
      partial: 'Teletrabajo a tiempo partial',
      punctual: 'Permitido de manera ocasional',
      no: 'No especificado',
    },
  },
  videoCookiePlaceholder: {
    title: 'Este contenido está bloqueado',
    subtitle: 'Las cookies de {source} son necesarias para mostrarte este contenido',
    button: 'Aceptar las cookies',
  },
}
