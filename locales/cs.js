export default {
  languages: {
    de: 'Němčina',
    en: 'Angličtina',
    ar: 'Arabština',
    es: 'Španělština',
    zh: 'Čínština',
    ko: 'Korejština',
    hr: 'Chorvatština',
    da: 'Dánština',
    fr: 'Francouzština',
    hu: 'Maďarština',
    he: 'Hebrejština',
    it: 'Italština',
    ja: 'Japonština',
    ms: 'Malajština',
    no: 'Norština',
    pl: 'Polština',
    pt: 'Portugalština',
    ro: 'Rumunština',
    ru: 'Ruština',
    sr: 'Srbština',
    sk: 'Slovenština',
    sl: 'Slovinština',
    sv: 'Švédština',
    cs: 'Čeština',
    th: 'Thajština',
    tr: 'Turečtina',
    uk: 'Ukrajinština',
    fi: 'Finština',
    id: 'Indonéština',
    nl: 'Holandština',
  },
  quotes: {
    beginning: '„',
    ending: '“',
  },
  articles: {
    userName: 'Podle {name}',
  },
  contents: {
    text: {
      topics: {
        description: 'Popis',
        good_to_know: 'Hodí se vědět',
        looking_for: 'Co hledají',
        working_for: 'Pozice',
      },
    },
    'company-stats': {
      creation_year: 'Rok založení',
      nb_employees: 'Zaměstnanci',
      sectors: 'Odvětví',
      location: 'Lokalita',
      parity: 'Zastoupení pohlaví',
      average_age: 'Průměrný věk',
      average_age_unit: ' let',
      turnover: 'Fluktuace zaměstnanců',
      turnover_unit: '%',
      revenue: 'Obrat',
    },
    jobs: {
      title: 'Nejnovější nabídky práce',
      'all-jobs': 'Všechny nabídky',
      'no-jobs': 'V tuto chvíli tu nejsou žádné nabídky, zkuste to znovu za pár dní!',
    },
    'jobs-search': {
      'no-results': 'Vašemu vyhledávání neodpovídají žádné nabídky…',
      'no-jobs':
        'Společnost {organizationName} bohužel v tuto chvíli nemá žádné volné pracovní nabídky! Prosím, zkuste to znovu za pár dní!',
      spontaneous: {
        title: 'Nenašli jste nabídku, která by vás nadchla?',
        subtitle:
          'Společnost {organizationName} neustále hledá talenty. Neváhejte a dejte najevo svůj zájem o práci!',
        link: 'Reagovat na nabídku',
      },
    },
    'meetings-search': {
      'no-results': 'Vašemu vyhledávání neodpovídají žádné události…',
      'no-meetings':
        'Společnost {organizationName} bohužel v tuto chvíli neplánuje žádné události…',
      meanwhile: 'Zatím si můžete prohlédnout události ostatních společností:',
      'browse-all-meetings': 'Prohlédnout si všechny události',
    },
    articles: {
      title: 'Zjistěte více o společnosti {organizationName}',
      userName: 'Podle {name}',
    },
    'social-networks': {
      title: 'Sledovat!',
    },
    'tech-stack': {
      'swiper-controls': {
        previous: 'Předchozí',
        next: 'Další',
      },
    },
    video: {
      'image-missing': 'Chybí obrázek',
    },
    'organization-embed': {
      title: 'Vložit profil na své webové stránky',
      button: 'Vložit widget',
      modal: {
        title: 'Všechen obsah společnosti {organizationName} na vašich stránkách',
      },
      embed: {
        title: 'Získejte widget',
      },
      preview: {
        title: 'Náhled',
      },
      form: {
        title: 'Nakonfigurujte widget',
        labels: {
          subtitle: 'Zkopírujte a vložte následující kód na vaše stránky',
          widthValue: 'Šířka widgetu',
          heightValue: 'Výška widgetu',
          lines: 'Počet řádků',
          columns: 'Počet sloupců',
          language: 'Jazyk',
        },
        errors: {
          width: {
            larger: 'Šířka musí být menší než {value}{unit}',
            smaller: 'Šířka musí být větší než {value}{unit}',
          },
          height: {
            larger: 'Výška musí být menší než {value}{unit}',
            smaller: 'Výška musí být větší než {value}{unit}',
          },
          lines: {
            larger: 'Počet řádků musí být menší než {value}',
            smaller: 'Počet řádků musí být větší než {value}',
          },
          columns: {
            larger: 'Počet sloupců musí být menší než {value}',
            smaller: 'Počet sloupců musí být větší než {value}',
          },
        },
        submit: 'Potvrdit',
      },
    },
    date: {
      published: 'Zveřejněno',
    },
    values: {
      title: 'Hodnoty společnosti',
    },
  },
  search: {
    common: {
      'range-slider': {
        label: {
          year: '{count} {count, plural, one {rok} few {roky} other {let}}',
        },
      },
      searchBox: {
        placeholder: 'Vaše vyhledávání...',
        submitTitle: 'Co hledáte?',
        resetTitle: 'Aktualizovat vyhledávání',
      },
      location: {
        placeholder: 'Kde?',
        nearby: 'Aktuální poloha',
        remote: 'Práce z domova',
        radius: {
          long: 'méně než {count} km',
          short: '< {count} km',
        },
        resetTitle: 'Resetovat',
      },
      reset: 'Resetovat',
    },
    articles: {
      searchBox: {
        placeholder: 'Jaké téma by vás zajímalo?',
      },
      stats: '{count} {count, plural, one {článek} few {články} other {článků}} k přečtení',
      widgets: {
        categories: {
          general: {
            title: 'Přehled',
          },
          tech: {
            title: 'Web & Tech',
          },
          fashion: {
            title: 'Fashion & Luxury',
          },
          consulting: {
            title: 'Consulting & Finance',
          },
          food: {
            title: 'Food & Beverage',
          },
        },
      },
    },
    organizations: {
      searchBox: {
        placeholder: 'Tým nebo značka vašeho srdce?',
      },
      stats:
        '{count} {count, plural, one {společnost} few {společnosti} other {společností}} k prozkoumání',
      mobile: {
        stats: '{count} {count, plural, one {společnost} few {společnosti} other {společností}}',
      },
      widgets: {
        sectors: {
          title: 'Odvětví',
        },
        labels: {
          title: 'Highlights',
        },
        size: {
          title: 'Velikost',
        },
        offices: {
          title: 'Místo',
          district: {
            title: 'Podle regionu',
          },
          country: {
            title: 'Podle země',
          },
        },
        languages: {
          title: 'Jazyky',
        },
      },
    },
    interviews: {
      searchBox: {
        placeholder: 'Práce snů?',
      },
      stats: '{count} povolání k prozkoumání',
      widgets: {
        category: {
          name: {
            title: 'Kategorie',
          },
        },
      },
    },
    jobs: {
      followedCompanies: {
        stats:
          '{count} {count, plural, one {pracovní nabídka} few {pracovní nabídky} other {pracovních nabídek}} sledovaných společností',
        mobile: {
          stats:
            '{count} {count, plural, one {pracovní nabídka} few {pracovní nabídky} other {pracovních nabídek}}',
        },
      },
      searchBox: {
        placeholder: 'Práce snů?',
      },
      stats:
        'Našli jsme {count} {count, plural, one {nabídku práce} few {nabídky práce} other {nabídek práce}}',
      mobile: {
        stats: '{count} {count, plural, one {nabídku} few {nabídky} other {nabídek}}',
      },
      widgets: {
        sectors: {
          title: 'Odvětví',
        },
        contract_type_names: {
          title: 'Úvazek',
        },
        office: {
          title: 'Místo',
          country: {
            title: 'Podle země',
          },
          district: {
            title: 'Podle regionu',
          },
        },
        profession: {
          title: 'Povolání',
        },
        other: {
          title: 'Další',
        },
        organization: {
          name: {
            title: 'Společnosti',
          },
          size: {
            title: 'Velikost',
          },
        },
        remote: {
          title: 'Možnost práce z domova',
        },
        experience_level_minimum: {
          title: 'Úroveň zkušeností',
        },
        language: {
          title: 'Jazyky',
        },
      },
      seen: "Zobrazeno"
    },
    stacks: {
      searchBox: {
        placeholder: 'Stack snů?',
      },
      stats: '{count} {count, plural, one {stack} few {stacky} other {stacků}} k prozkoumání',
      widgets: {
        tools: {
          backend: {
            title: 'Backend',
          },
          frontend: {
            title: 'Frontend',
          },
          devops: {
            title: 'DevOps',
          },
          data: {
            title: 'Analýza dat',
          },
          mobile: {
            title: 'Mobilní aplikace',
          },
          ci: {
            title: 'Průběžná integrace',
          },
          pm: {
            title: 'Produktový management',
          },
          ide: {
            title: 'IDE',
          },
          monitoring: {
            title: 'Monitoring',
          },
          misc: {
            title: 'Různé',
          },
        },
        'other-tools': {
          title: 'Jiné',
        },
      },
    },
    meetings: {
      searchBox: {
        placeholder: 'Událost snů?',
      },
      stats: 'Našli jsme {count} {count, plural, one {událost} few {události} other {událostí}}',
      widgets: {
        category: {
          name: {
            title: 'Kategorie',
          },
        },
        address: {
          title: 'Místo',
          district: {
            title: 'Podle regionu',
          },
          country: {
            title: 'Podle země',
          },
        },
        organization: {
          name: {
            title: 'Společnosti',
          },
        },
        language: {
          title: 'Jazyky',
        },
      },
    },
    applications: {
      searchBox: {
        placeholder: 'Zadejte, co hledáte',
      },
      stats:
        '{count} {count, plural, one {reakce na nabídku} few {reakce na nabídku} other {reakcí na nabídku}}',
      widgets: {
        status: {
          title: 'Stav',
        },
      },
      'no-results': 'Nic jsme nenašli. Zkuste to znovu s jinými klíčovými slovy!',
      empty: {
        title: 'Vypadá to, že jste na nabídku ještě nereagovali!',
        'find-organization': 'Najít svůj „kmen“',
        'find-job': 'Najít práci',
        'if-mistake': 'Pokud se jedná o chybu, ',
        'contact-us': 'kontaktujte nás!',
      },
    },
  },
  jobs: {
    'contract-types': {
      full_time: 'Plný úvazek',
      part_time: 'Částečný úvazek',
      temporary: 'Smlouva na dobu určitou',
      freelance: 'Freelance',
      internship: 'Stáž',
      apprenticeship: 'Work-Study',
      vie: 'Mezinárodní firemní dobrovolnický program',
      volunteer: 'Dobrovolnictví',
      idv: 'IDV',
      graduate_program: 'Graduate program',
      other: 'Jiné',
    },
    remote: {
      fulltime: 'Možnost pracovat plně z domova',
      partial: 'Možnost pracovat částečně z domova',
      punctual: 'Možnost pracovat příležitostně z domova',
      no: 'Neuvedeno',
    },
  },
  videoCookiePlaceholder: {
    title: 'Tento obsah je zablokovaný',
    subtitle: 'K zobrazení tohoto obsahu jsou nutné cookies {source}',
    button: 'Přijmout cookies',
  },
}
