export default {
  languages: {
    de: 'German',
    en: 'English',
    ar: 'Arabic',
    es: 'Spanish',
    zh: 'Chinese',
    ko: 'Korean',
    hr: 'Croatian',
    da: 'Danish',
    fr: 'French',
    hu: 'Hungarian',
    he: 'Hebrew',
    it: 'Italian',
    ja: 'Japanese',
    ms: 'Malay',
    no: 'Norwegian',
    pl: 'Polish',
    pt: 'Portuguese',
    ro: 'Romanian',
    ru: 'Russian',
    sr: 'Serbian',
    sk: 'Slovak',
    sl: 'Slovenian',
    sv: 'Swedish',
    cs: 'Czech',
    th: 'Thai',
    tr: 'Turkish',
    uk: 'Ukrainian',
    fi: 'Finnish',
    id: 'Indonesian',
    nl: 'Dutch',
  },
  quotes: {
    beginning: '“',
    ending: '”',
  },
  articles: {
    userName: 'By {name}',
  },
  contents: {
    text: {
      topics: {
        description: 'Description',
        good_to_know: 'Good to know',
        looking_for: "What they're looking for",
        working_for: 'Position',
      },
    },
    'company-stats': {
      creation_year: 'Year of Founding',
      nb_employees: 'Employees',
      sectors: 'Sectors',
      location: 'Location',
      parity: 'Gender Breakdown',
      average_age: 'Average Age',
      average_age_unit: ' years old',
      turnover: 'Turnover',
      turnover_unit: '%',
      revenue: 'Revenue',
    },
    jobs: {
      title: 'Latest job listings',
      'all-jobs': 'See all job listings',
      'no-jobs': 'No openings for now, please check back in a few days!',
    },
    'jobs-search': {
      'no-results': 'No jobs match your search…',
      'no-jobs':
        "Unfortunately, {organizationName} doesn't have any openings at the moment! Please check back in a few days!",
      spontaneous: {
        title: "Don't see a position that strikes your fancy?",
        subtitle:
          '{organizationName} is always looking for great talent. Go ahead and send an application!',
        link: 'Apply',
      },
    },
    'meetings-search': {
      'no-results': 'No events matched your search…',
      'no-meetings': "Unfortunately, {organizationName} isn't holding any events right now…",
      meanwhile: 'In the meantime, feel free to browse events at other companies:',
      'browse-all-meetings': 'Browse all events',
    },
    articles: {
      title: 'Learn more about {organizationName}',
      userName: 'By {name}',
    },
    'social-networks': {
      title: 'Follow them!',
    },
    'tech-stack': {
      'swiper-controls': {
        previous: 'Previous',
        next: 'Next',
      },
    },
    video: {
      'image-missing': 'Missing image',
    },
    'organization-embed': {
      title: 'Embed this profile on your website',
      button: 'Embed Widget',
      modal: {
        title: "All of {organizationName}'s content on your site",
      },
      embed: {
        title: 'Get your widget',
      },
      preview: {
        title: 'Preview',
      },
      form: {
        title: 'Configure your widget',
        labels: {
          subtitle: 'Copy and paste the following code onto your site',
          widthValue: 'Width of widget',
          heightValue: 'Height of widget',
          lines: 'Number of rows',
          columns: 'Number of columns',
          language: 'Language',
        },
        errors: {
          width: {
            larger: 'The width must be smaller than {value}{unit}',
            smaller: 'The width must be larger than {value}{unit}',
          },
          height: {
            larger: 'The height must be smaller than {value}{unit}',
            smaller: 'The height must be larger than {value}{unit}',
          },
          lines: {
            larger: 'The number of rows must be fewer than {value}',
            smaller: 'The number of rows must be greater than {value}',
          },
          columns: {
            larger: 'The number of columns must be fewer than {value}',
            smaller: 'The number of columns must be greater than {value}',
          },
        },
        submit: 'Submit',
      },
    },
    date: {
      published: 'Posted',
    },
    values: {
      title: 'Their values',
    },
  },
  search: {
    common: {
      'range-slider': {
        label: {
          year: '{count} {count, plural, one {year} other {years}}',
        },
      },
      searchBox: {
        placeholder: 'Your search...',
        submitTitle: 'What are you searching for?',
        resetTitle: 'Update my search',
      },
      location: {
        placeholder: 'Where?',
        nearby: 'Current location',
        remote: 'Telework',
        radius: {
          long: 'less than {count} km',
          short: '< {count} km',
        },
        resetTitle: 'Reset',
      },
      reset: 'Reset',
    },
    articles: {
      searchBox: {
        placeholder: 'Have a topic in mind?',
      },
      stats: '{count} {count, plural, one {article} other {articles}} to read',
      widgets: {
        categories: {
          general: {
            title: 'Overview',
          },
          tech: {
            title: 'Web & Tech',
          },
          fashion: {
            title: 'Fashion & Luxury',
          },
          consulting: {
            title: 'Consulting & Finance',
          },
          food: {
            title: 'Food & Beverage',
          },
        },
      },
    },
    organizations: {
      searchBox: {
        placeholder: 'Your dream tribe?',
      },
      stats: '{count} {count, plural, one {company} other {companies}} to check out',
      mobile: {
        stats: '{count} {count, plural, one {company} other {companies}}',
      },
      widgets: {
        sectors: {
          title: 'Sector',
        },
        labels: {
          title: 'Highlights',
        },
        size: {
          title: 'Size',
        },
        offices: {
          title: 'Location',
          district: {
            title: 'By region',
          },
          country: {
            title: 'By country',
          },
        },
        languages: {
          title: 'Languages',
        },
      },
    },
    interviews: {
      searchBox: {
        placeholder: 'Your dream job?',
      },
      stats: '{count} {count, plural, one {career} other {careers}} to check out',
      widgets: {
        category: {
          name: {
            title: 'Categories',
          },
        },
      },
    },
    jobs: {
      followedCompanies: {
        stats: '{count} {count, plural, one {job} other {jobs}} from followed companies',
        mobile: {
          stats: '{count} {count, plural, one {job} other {jobs}}',
        },
      },
      searchBox: {
        placeholder: 'Your dream job?',
      },
      stats: '{count} {count, plural, one {job} other {jobs}} for your search',
      mobile: {
        stats: '{count} {count, plural, one {job} other {jobs}}',
      },
      widgets: {
        sectors: {
          title: 'Sector',
        },
        contract_type_names: {
          title: 'Contract',
        },
        office: {
          title: 'Location',
          country: {
            title: 'By country',
          },
          district: {
            title: 'By region',
          },
        },
        profession: {
          title: 'Professions',
        },
        other: {
          title: 'Other',
        },
        organization: {
          name: {
            title: 'Companies',
          },
          size: {
            title: 'Size',
          },
        },
        remote: {
          title: 'Remote work',
        },
        experience_level_minimum: {
          title: 'Level of experience',
        },
        language: {
          title: 'Languages',
        },
      },
      seen: "Seen"
    },
    stacks: {
      searchBox: {
        placeholder: 'Your dream stack?',
      },
      stats: '{count} {count, plural, one {stack} other {stacks}} to check out',
      widgets: {
        tools: {
          backend: {
            title: 'Back-end',
          },
          frontend: {
            title: 'Front-end',
          },
          devops: {
            title: 'DevOps',
          },
          data: {
            title: 'Data Analysis',
          },
          mobile: {
            title: 'Mobile',
          },
          ci: {
            title: 'Continuous Integration',
          },
          pm: {
            title: 'Product Management',
          },
          ide: {
            title: 'IDE',
          },
          monitoring: {
            title: 'Monitoring',
          },
          misc: {
            title: 'Miscellaneous',
          },
        },
        'other-tools': {
          title: 'Other',
        },
      },
    },
    meetings: {
      searchBox: {
        placeholder: 'Your dream event?',
      },
      stats: '{count} {count, plural, one {event} other {events}} for your search',
      widgets: {
        category: {
          name: {
            title: 'Category',
          },
        },
        address: {
          title: 'Location',
          district: {
            title: 'By region',
          },
          country: {
            title: 'By country',
          },
        },
        organization: {
          name: {
            title: 'Companies',
          },
        },
        language: {
          title: 'Languages',
        },
      },
    },
    applications: {
      searchBox: {
        placeholder: 'Enter your search',
      },
      stats: '{count} {count, plural, one {application} other {applications}}',
      widgets: {
        status: {
          title: 'Status',
        },
      },
      'no-results': 'No results. Try again with other keywords!',
      empty: {
        title: "Oh, it looks like you haven't applied yet!",
        'find-organization': 'Find a tribe',
        'find-job': 'Find a job',
        'if-mistake': "If there's been a mistake, ",
        'contact-us': 'contact us!',
      },
    },
  },
  jobs: {
    'contract-types': {
      full_time: 'Permanent contract',
      part_time: 'Part-time',
      temporary: 'Temporary',
      freelance: 'Freelance',
      internship: 'Internship',
      apprenticeship: 'Work-Study',
      vie: 'International Corporate Volunteer Program',
      other: 'Other',
      volunteer: 'Volunteer Work',
      idv: 'IDV',
      graduate_program: 'Graduate program',
    },
    remote: {
      fulltime: 'Possible full remote',
      partial: 'Partial remote authorized',
      punctual: 'Occasional remote authorized',
      no: 'Not specified',
    },
  },
  videoCookiePlaceholder: {
    title: 'This content is blocked',
    subtitle: '{source} cookies are required to show you this content',
    button: 'Accept cookies',
  },
}
