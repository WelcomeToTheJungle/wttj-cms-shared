import fr from './fr'
import en from './en'
import { compareTranslationFiles } from '../test/shared'

describe('Translations', () => {
  compareTranslationFiles(fr, en)
})
